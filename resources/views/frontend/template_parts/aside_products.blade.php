					<aside class="sidebar product-filter">
						<!-- Categories -->
						<section class="widget section-categories">
							<h3 class="widget-title">Categorías</h3>
							<div class="widget-content">
								<ul class="list-categories list-widget">
									<li class="active">
										<a class="{{ Request::is('products') ? 'active' : '' }}" href="{{ route("products") }}">
											Todas
										</a>
									</li>
									@foreach ($categories as $cat)
										@if($cat->status=="Activa")
											<li class="list-widget__item">
											<a class="list-widget__link {{ Request::is('product/category/' . $cat->slug) ? 'active' : '' }}" 
												href="{{ route("products_category", $cat->slug) }}">
												{{ $cat->name }}
											</a>
										</li>
										@endif
									@endforeach
								</ul>
							</div>
						</section>
						<!-- /Categories -->

						<!-- Brand -->
						<section class="widget section-brand">
							<h3 class="widget-title">by Brand</h3>
							<div class="widget-content">
								<ul class="list-checked">
									<li><a href="#">Alchemy Equipment</a></li>
									<li><a href="#">Arc'teryx</a></li>
									<li class="current"><a href="#">Black Diamond</a></li>
									<li><a href="#">Canada Goose</a></li>
									<li class="current"><a href="#">Craft</a></li>
									<li><a href="#">Mountain Hardwear</a></li>
									<li><a href="#">The North Face</a></li>
									<li><a href="#">Arc'teryx</a></li>
									<li><a href="#">The North Face</a></li>
								</ul>
							</div>
						</section>
						<!-- /Brand -->
							
						<!-- Price -->
						<section class="widget section-price widget_filter">
							<h3 class="widget-title">Price range</h3>
							<div class="widget-content">
								<div class="ps-slider" data-default-min="0" data-default-max="500" data-max="1000" data-step="100" data-unit="$"></div>
					          	<p class="ps-slider__meta">Price:<span class="ps-slider__value ps-slider__min"></span>-<span class="ps-slider__value ps-slider__max"></span></p>
							</div>
						</section>
						<!--/Price -->

						<!-- Size -->
						<section class="widget section-size">
							<h3 class="widget-title">refine by Size</h3>
							<div class="widget-content">
								<ul class="list-checked list-two">
									<li><a href="#">XS</a></li>
									<li class="current"><a href="#">XL</a></li>
									<li class="current"><a href="#">S</a></li>
									<li><a href="#">2XL</a></li>
									<li><a href="#">M</a></li>
									<li><a href="#">3XL</a></li>
									<li><a href="#">L</a></li>
									<li><a href="#">2XL</a></li>
								</ul>
							</div>
						</section>
						<!-- /Size -->

						<!-- Color -->
						<section class="widget section-color">
							<h3 class="widget-title">by Color</h3>
							<div class="widget-content">
								<ul class="list-checked list-two">
									<li><a href="#">Black</a></li>
									<li class="current"><a href="#">Pink</a></li>
									<li class="current"><a href="#">Blue</a></li>
									<li><a href="#">Purple</a></li>
									<li><a href="#">Brown</a></li>
									<li><a href="#">Red</a></li>
									<li><a href="#">Green</a></li>
									<li><a href="#">Yellow</a></li>
									<li><a href="#">Grey</a></li>
									<li><a href="#">White</a></li>
								</ul>
							</div>
						</section>
						<!--/Color -->
					</aside>