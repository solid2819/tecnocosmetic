 <div class="modal fade modal-3d-slit" id="modal-view-{{ $banner->id }}" aria-hidden="true"
                  aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Detalles de {{$banner->title}}</h4>
           <ul class="nav nav-tabs nav-tabs-line" data-plugin="nav-tabs" role="tablist">
              <li class="active" role="presentation"><a data-toggle="tab" href="#exampleLine1" aria-controls="exampleLine1" role="tab">Detalles</a></li>
              <li role="presentation"><a data-toggle="tab" href="#exampleLine2" aria-controls="exampleLine2" role="tab">Desripción</a></li>
              <li role="presentation"><a data-toggle="tab" href="#exampleLine3" aria-controls="exampleLine3" role="tab">CSS</a></li>
              <li role="presentation"><a data-toggle="tab" href="#exampleLine4" aria-controls="exampleLine4" role="tab">JavaScript</a></li>
            </ul>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-4">
                @if($banner->image)
                  <img src="{{ asset("images/frontend/banners/$banner->image") }}" width="250px">
                  @else
                    <img src="{{URL::to('/')}}/global/photos/placeholder.png" width="250px">
                @endif
            </div>
            <div class="col-sm-12 col-md-8 col-lg-8">
              <div class="tab-content">
                  <div class="tab-pane active" id="exampleLine1" role="tabpanel">
                    Dettale de banner
                    {{--  <p>Precio: {{ $banner->price }}</p>  --}}   
                  </div>
                  <div class="tab-pane" id="exampleLine2" role="tabpanel">
                    {!! $banner->title !!}    
                  </div>
                  <div class="tab-pane" id="exampleLine3" role="tabpanel">
                  Cernimus nutu. Maioribus solet. Iustitiam conciliant reliquisti instituendarum
                                  
                  </div>
                  <div class="tab-pane" id="exampleLine4" role="tabpanel">
                    Nec iste vellem, accusamus inesse exhorrescere tertium dominationis licebit perpetiuntur,                
                  </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline btn-warning btn-xs" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>