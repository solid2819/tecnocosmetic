<div class="content-dropdown left">
								<div class="account-inner ">
								    <div class="login-form-head">
								      	<span class="login-form-title">Entrar</span>
								      	<span class="pull-right">
								     	 	<a class="register-link" href="<?php echo e(route("register")); ?>" title="Register">
								     	 	 Crear una cuenta
								     	   </a>
								     	 </span>
								    </div>
								    <form class="form-horizontal" role="form" method="POST" action="<?php echo e(url('/login')); ?>">
                        				<?php echo e(csrf_field()); ?>         
		 
										<div>
											<input id="email" type="text" class="" placeholder="Igresar E-mail" name="email" value="<?php echo e(old('email')); ?>" required>

											<?php if($errors->has('email')): ?>
												<span class="help-block">
													<strong><?php echo e($errors->first('email')); ?></strong>
												</span>
											<?php endif; ?>					
										</div>
			
										 <div>			
												<input id="password" type="password" required class="form-control" name="password" placeholder="Contraseña">

												<?php if($errors->has('password')): ?>
													<span class="help-block">
														<strong><?php echo e($errors->first('password')); ?></strong>
													</span>
												<?php endif; ?>
										</div>
										<button type="submit" class="btn btn-primary ladda-button btn-block" data-plugin="ladda" data-style="expand-left">
											  <?php echo e(trans('Entrar')); ?>

										<span class="ladda-spinner"></span><div class="ladda-progress" style="width: 0px;"></div>
										</button>
										
									</form>
								    <div class="login-form-bottom">
								      		<a href="<?php echo e(url('/password/reset')); ?>" class="lostpass-link" title="Lost your password?"><small>Recuperar contraseña</small></a>
								    </div>          
								</div>
							</div>