 <div class="form-group">
   <div class="input-search">
   	<form role="search" method="get" action="{{ route("banners.index") }}">
	     <button type="submit" class="input-search-btn"><i class="icon wb-search" aria-hidden="true"></i></button>
	     <input type="text" class="form-control" name="searchText" placeholder="Buscar productos por nombre o código..." value="{{$searchText}}">
	   </div>
	</form>
 </div>