@extends("layouts.frontend.theme")
@section("title")
	Nuestra empresa
@endsection

@section("content")
	<!--==== Start breadcrumb -->
   	<div class="breadcrumb">
		<div class="container">
			<h1>About us</h1>
			<ol class="item-breadcrumb">
	            <li><a href="index.html">Home</a></li>
	            <li>About us</li>     
            </ol>
		</div>
   	</div>
   <!-- End breadcrumb ====

   	<div class="page-about">
		<!-- section-image -->
		<div class="section-image pb-50 clearfix">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="section-bg bg-overlay text-white" style="background-image:url(assets/images/about/about_1.jpg)">
							<h3 class="section-image__title">
							"Holistically foster superior
							methodologies without
							market-driven best practices."
							</h3>
						</div>
					</div>
					<div class="col-md-4 hidden-xs hidden-sm">
						<img src="assets/images/about/about_2.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
		<!--/section-image -->

		<!-- section-experience -->
		<div class="section-experience pt-50 pb-50 clearfix">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-12 col-xs-12">
						<div class="section-number section-bg section-bg-fix bg-overlay text-center" style="background-image:url(assets/images/about/about_3.jpg)">
							<h4 class="experience-title text-uppercase">years of experience</h4>
							<div class="experience-number">25</div>
						</div>
					</div>
					<div class="col-md-8 col-sm-12 col-xs-12">
						<div class="experience-content">
							<h3 class="experience-title">We have created beautifull products for you</h3>
							<div class="experience-description">
								<p>
									A streamlined cloud solution. User generated content in real-time will have
									multiple touchpoints has evolved from generation X is on the runway heading
									towards a streamlined cloud solution.
								</p>
								<p>

									User generated content in real-time will have multiple touchpoints has evolved from generation X is on the runway heading towards a streamlined cloud solution.
									A streamlined cloud solution.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /section-experience -->

		<!-- section-kind -->
		<div class="section-kind features skin1 clearfix pt-50 pb-50">
			<div class="container">
				<div class="heading text-uppercase text-center pb-100">
					<div class="heading__sub">
						The best of a kind
					</div>
					<div class="heading__title">
						Explore Our Company
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="features_list">
							<div class="features-images">
								<img src="assets/images/icon_feature1.png" alt="">
							</div>
							<div class="features-description">
								<h4>Dedicated Team</h4>
								<p>Objectively innovate empowered manufactured products whereas parallel platforms.
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="features_list">
							<div class="features-images">
								<img src="assets/images/icon_feature2.png" alt="">
							</div>
							<div class="features-description">
								<h4>Best Engineers</h4>
								<p>
								Distinctively re-engineer revolutionary meta-services and premium architectures.
							</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="features_list">
							<div class="features-images">
								<img src="assets/images/icon_feature3.png" alt="">
							</div>
							<div class="features-description">
								<h4>24/7 Supports</h4>
								<p>
								Holisticly predominate extensible testing procedures for reliable supply chains.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /section-kind -->

		<!-- section-counter -->
		<div class="section-counter section-bg clearfix pt-50" style="background-image:url(assets/images/about/about_bg_counter.jpg)">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="counter_number text-center">
							<span class="counter">2346</span>
							<h5>Employees from around on the world</h5>
						</div>
					</div>
					<div class="col-md-4">
						<div class="counter_number text-center">
							<span class="counter">345</span><span>+</span>
							<h5>We Care About Our Investors</h5>
						</div>
					</div>
					<div class="col-md-4">
						<div class="counter_number text-center">
							<span class="counter">26</span><span>+</span>
							<h5>People Are Our Biggest Capital</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /section-counter -->

		<!-- section-exprore -->
		<div class="section-exprore clearfix pt-50 pb-40">
			<div class="container">
				<div class="section-bg section-bg-fix bg-overlay text-white text-center" style="background-image:url(assets/images/about/about_4.jpg)">
					<h3>You'll be happy to see our <br/> awesome features.</h3>
					<p>Expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money.</p>
					<a href="product_grid.html" class="color-primary font-bold pt-20">Exprore our products!</a>
				</div>
			</div>
		</div>
		<!-- /section-exprore -->
	</div>
	
@endsection