@if(count($errors)>0)
	<div class="alert dark alert-icon alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
		<i class="icon wb-close" aria-hidden="true"></i> ¡Error!
		<ul>
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
		</ul>
	</div>
@endif

@if(session("info"))
	<div class="alert dark alert-icon alert-success alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	    </button>
	    <p class="icon-text"><i class="fa fa-info-circle" aria-hidden="true"></i> Información</p>
		{{--  <i class="icon wb-check" aria-hidden="true"></i> --}}{!! session("info") !!}
	</div>
@endif

@if(session("info-error"))
	<div class="alert dark alert-icon alert-danger alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	    </button>
	    <p class="icon-text"><i class="fa fa-info-circle" aria-hidden="true"></i> Error</p>
		{{--  <i class="icon wb-check" aria-hidden="true"></i> --}}{!! session("info-error") !!}
	</div>
@endif
