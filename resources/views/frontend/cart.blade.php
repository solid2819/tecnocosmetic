@extends("layouts.frontend.theme")
@section("title")
	Carrito
@endsection
@section("content")
	@include("layouts.messages")
	<!--	==== Start breadcrumb -->
   	<div class="breadcrumb">
   		<!-- container -->
		<div class="container">
			<h1>Cart</h1>
			<ol class="item-breadcrumb">
	            <li><a href="{{ url("/") }}">Home</a></li>
	            <li><a href="{{ url("/products") }}">Products</a></li>
	            <li>Cart</li>     
            </ol>
		</div>
		<!-- /container -->
   	</div>

   	<div class="page-cart">
		<!-- container -->
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<form class="cart-form table_responsive" action="#" method="post">
						@if(count($userCart))
							<table class="shop_table table cart cart-form__contents">
								<thead>
									<tr>
										<th class="product-thumbnail">Producto</th>
										<th class="product-name">Nombre</th>
										<th class="product-name">Tamaño</th>
										<th class="product-price">Precio</th>
										<th class="product-quantity">Cantidad</th>
										<th class="product-subtotal">Total</th>
										<th class="product-remove">&nbsp;</th>
									</tr>
								</thead>
								<tbody>
									<?php  $monto_total = 0; ?>
									@foreach($userCart as $cart)
										<tr class="cart-form__cart-item cart_item">

											<td class="product-thumbnail">
												@if($cart->image)
													<img class="product-featured-image" src="{{ asset("images/dashboard/products/" .$cart->image) }}" alt="">
												@endif
											</td>
											<td class="product-name" data-title="Product">
												<a href="product_single.html">{{ $cart->name }}</a>	
											</td>
											<td class="product-name" data-title="Tamaño">
												<a href="product_single.html">{{ $cart->size }}</a>	
											</td>
											<td class="product-price" data-title="Price">
												<span class="price-amount amount"><span class="price-currencySymbol">$</span>{{ $cart->price }}</span>			
											</td>
											<td class="product-quantity" data-title="Quantity">
												<!--<div class="quantity">
													<input type="number" class="input-text qty text" step="1" min="0" value="{{ $cart->quantity }}" title="Qty" size="4">
													
														<a href="{{ url("cart/update-quantity-product/". $cart->id. "/+1") }}">
															<i class="fa fa-refresh"></i>
														</a>
													
												</div>-->
												
												<div class="quantity">
													<a  href="{{ url("cart/update-quantity-product/". $cart->id. "/+1") }}">
															<i class="fa fa-sort-asc"></i>
														</a>
													<input  class="input-text qty text" step="1" min="0" value="{{ $cart->quantity }}" title="Qty" size="2">

													@if($cart->quantity>1)
														<a  href="{{ url("cart/update-quantity-product/". $cart->id. "/-1") }}">
															<i class="fa fa-sort-desc"></i>
														</a>
													@endif
												</div>
											</td>
											<td class="product-subtotal" data-title="Sub-total">
												<span class="price-amount amount"><span class="price-currencySymbol">$</span>{{ $cart->price * $cart->quantity}}</span>					
											</td>

											<td class="product-remove">
												<a href="{{ route("deleteCar", $cart->id) }}" class="remove" aria-label="Quitar Item" data-product_id="9114" data-product_sku=""><i class="fa fa-trash-o"></i></a>		
											</td>
										</tr>
									<?php
										//Calcula el monto total de los productos en el carrito
									 	$monto_total = $monto_total + ($cart->price * $cart->quantity) 
									 ?>
									@endforeach
									<tr>
										<td colspan="6" class="actions">
											<div class="clearfix">
											<div class="coupon pull-left"> 
												<input type="text" name="coupon_code" class="input-text " id="coupon_code" value="" placeholder="Coupon code"> 
												<input type="submit" class="btn btn-default" name="apply_coupon" value="Apply Coupon">
											</div>
												<div class="pull-right"> 
													<input type="hidden" id="_wpnonce" name="_wpnonce" value="#"><input type="hidden" name="_wp_http_referer" value="#">
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						@else
							No hay productos en el carrito 
							<a href="{{ route("products") }}">
								<input type="button" class="btn btn-primary btn-xs" value="Ir a la tienda">
							</a>
						@endif
					</form>
				</div>
				<div class="col-md-4">
					<div class="grand-totall">
                        <div class="title-wrap">
                            <h4 class="cart-bottom-title section-bg-gary-cart">Cart Total</h4>
                        </div>
                        <h5>Total en producto:  <?php echo $monto_total; ?> $ <span></span></h5>
                        <div class="total-shipping">
                            <h5>Total shipping</h5>
                            <ul>
                                {{-- <li><input type="checkbox"> Standard: <span>$20.00</span></li>
                                <li><input type="checkbox"> Express: <span>$30.00</span></li> --}}
                                No Aplica
                            </ul>
                        </div>
                        <h4 class="grand-totall-title">
                        	Total: <?php echo $monto_total." $"; ?> <span></span>
                        </h4>
                        <a href="checkout.html" class="btn btn-primary checkout-button">Proceder al Pago</a>
                    </div>
				</div>
			</div>
		</div>
		<!-- /container -->
	</div>
	
@endsection