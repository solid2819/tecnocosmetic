<?php $__env->startSection("title"); ?>
	Inicio
<?php $__env->stopSection(); ?>
<?php $__env->startSection("content"); ?>
	<!-- slider home -->
	<div class="section-main-slider">
	    <div class="main-slider my-slider slider-pro" id="my-slider-1" data-slider-width="100%" data-slider-height="680">
	        <div class="sp-slides  text-white">
	            <!-- Slide 1 -->
	           <?php $__currentLoopData = $banner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
	           	<div class="sp-slide">
				    <div class="container">

				        <h2 class="sp-layer main-slider__title" data-horizontal="0" data-vertical="120" data-show-transition="up" data-hide-transition="left" data-show-duration="800" data-show-delay="800" data-hide-delay="800">
								<?php echo e($slide->title); ?><br>
								
						</h2>

				        <div class="sp-layer main-slider__text" data-horizontal="0" data-vertical="310"  data-show-transition="up" data-hide-transition="left" data-show-duration="800" data-show-delay="1200" data-hide-delay="1200">
				            <?php echo $slide->description; ?>

				            <div class="main-slider__btn-group">
				                <a class="mt-20 btn-primary btn" href="<?php echo e(url("/products/")); ?>">Comprar ahora<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
				                
				            </div>
				        </div>
				        <span class="sp-layer main-slider__img" data-horizontal="900" data-vertical="60" data-width="30%" data-show-transition="up" data-hide-transition="left" data-show-duration="800" data-show-delay="400" data-hide-delay="400">
								<img class="img-responsive" src="<?php echo e(asset("images/frontend/banners/$slide->image")); ?>" height="630" width="545" alt="">
						</span>
				    </div>
				</div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	            <!-- end sp-slide -->
	        </div>
	        <!-- end sp-slides -->
	    </div>
	    <!-- end main-slider -->
	</div>
	<!-- /end slider home -->

	<!-- Features -->
	<div class="features skin1 pt-100 pb-30">
		<!-- Container -->
		<div class="container-fluid">
			<!-- Row -->
			<div class="row">
				<!-- features -->
				<div class="col-md-4 col-sm-12 col-xs-12 features__left pb-50">
					<div class="heading text-uppercase">
						<div class="heading__sub">
							awesome features
						</div>
						<div class="heading__title">
							You’ll be happy to see our awesome features.
						</div>
					</div>
					<div class="features-description">
						Molestie at elementum eu facilisis sed odio. Scelerisque in dictum non consectetur a erat. Aliquam id diam maecenas ultricies mi eget mauris. Ultrices sagittis orci a scelerisque purus.
					<br/>
						Ultrices sagittis orci a scelerisque purus.
					</div>
					<a href="product_grid.html" class="color-second pt-40">Exprore our products!</a>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 features__cente pb-50">
					<img src="<?php echo e(asset("front/assets/images/image_feature.png")); ?>" alt="">
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 features__right pb-50">
					<div class="features_list">
						<div class="features-images">
							<img src="<?php echo e(asset("front/assets/images/icon_feature1.png")); ?>" alt="">
						</div>
						<div  class="features-description">
							<h4>Smart Connection</h4>
							<p>Paper had only a child’s pinkie bonewo with. His group surprise the wword in 20 reporting that it had extracted</p>
						</div>
					</div>
					<div class="features_list">
						<div class="features-images">
							<img src="<?php echo e(asset("front/assets/images/icon_feature2.png")); ?>" alt="">
						</div>
						<div  class="features-description">
							<h4>Allways on color display</h4>
							<p>Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet.</p>
						</div>
					</div>
					<div class="features_list">
						<div class="features-images">
							<img src="<?php echo e(asset("front/assets/images/icon_feature3.png")); ?>" alt="">
						</div>
						<div  class="features-description">
							<h4>Voice reply notifications</h4>
							<p>Paper had only a child’s pinkie bonewo with. His group surprise the wword in 20 reporting that it had extracted</p>
						</div>
					</div>
				</div>
				<!-- /features -->
			</div>
			<!-- /Row -->
		</div>
		<!-- /Container -->
	</div>
	<!-- /Features -->

	<!-- Video -->
	<div class="video section-bg bg-overlay pt-120 pb-250" style="background-image:url(<?php echo e(asset('front/assets/images/bg_video.jpg')); ?>)">
		<div class="heading text-uppercase text-center text-white">
			<div class="heading__sub">
				See it in action
			</div>
			<div class="heading__title">
				How to made
			</div>
		</div>
		<div class="video_player">
			<a href="https://www.youtube.com/watch?v=dPL1-8ypnEs" data-rel="prettyPhoto[videos]" title="">
			    <div class="video-play-button" data-video-play-button="">
		          	<svg class="video-play-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="39" viewBox="0 0 24 39">
		            <path fill="currentColor" fill-rule="evenodd" d="M24 19.355L0 38.71V0"></path>
		          	</svg>
        		</div>
			</a>
		</div>
	</div>
    <!-- /Video -->

	<!-- /quality -->
	<div class="quality section-bg pt-120 pb-80" style="background-image:url(<?php echo e(asset("front/assets/images/bg_quality.jpg")); ?>)">
		<div class="container-fluid">
	        <div class="row">
	            <div class="col-lg-5 col-md-4">
	            	<img src="<?php echo e(asset("front/assets/images/image_quality.png")); ?>" alt="">
	            </div>

	            <div class="col-lg-7 col-md-8">
	            	<div class="heading text-uppercase">
						<div class="heading__sub">
							Best Quality Watchs
						</div>
						<div class="heading__title">
							Tech SPECIFICATION
						</div>
					</div>
					<div class="row">
		            	<div class="col-md-4">
			                <div class="list-descriptions">
			                    <h5 class="list-descriptions__name">SIZE AND DIMENSIONS</h5>
			                    <div class="list-descriptions__description">2.5” x 2.0” x 0.8” (51.0 x 51.0 x 17.0 mm)</div>
			                </div>
			                <div class="list-descriptions">
			                    <h5 class="list-descriptions__name">DISPLAY SIZE. WXH</h5>
			                    <div class="list-descriptions__description">1.2” (30.4 mm)</div>
			                </div>
			                <div class="list-descriptions">
			                    <h5 class="list-descriptions__name">WEIGHT</h5>
			                    <div class="list-descriptions__description">2.3 oz / 66 g</div>
			                </div>
			                <div class="list-descriptions">
			                    <h5 class="list-descriptions__name">DISPLAY SIZE. WXH</h5>
			                    <div class="list-descriptions__description">1.2” (30.4 mm)</div>
			                </div>
			            </div>
			            <div class="col-md-4">
			                <div class="list-descriptions">
			                    <h5 class="list-descriptions__name">Battery Lifetime</h5>
			                    <div class="list-descriptions__description">
			                    	Upto 50 hours (with average use)
			                    </div>
			                </div>
			                <div class="list-descriptions">
			                    <h5 class="list-descriptions__name">Additional</h5>
			                    <div class="list-descriptions__description">
			                    	High-sensitivity receiver, Vibration alert,
	                            </div>
			                </div>
			                <div class="list-descriptions">
			                    <h5 class="list-descriptions__name">sensors</h5>
			                    <div class="list-descriptions__description">Indoor Tracker, Compass</div>
			                </div>
			                
			                <div class="list-descriptions">
			                    <h5 class="list-descriptions__name">ACTIVITIES</h5>
			                    <div class="list-descriptions__description">
			                    	Indoor Running, Outdoor Running, Cycling 
									Dedicated Bike Mount, Swimming
								</div>
			                </div>
			            </div>
			            <div class="col-md-4">
			                <div class="list-descriptions">
			                    <h5 class="list-descriptions__name">Waterproofing</h5>
			                    <div class="list-descriptions__description">40 m / 131 feet (5 ATM)</div>
			                </div>
			                <div class="list-descriptions">
			                    <h5 class="list-descriptions__name">WIRELESS</h5>
			                    <div class="list-descriptions__description">
			                    	2.4 GHz receiver (proprietary)
								</div>
			                </div>
			                <div class="list-descriptions">
			                    <h5 class="list-descriptions__name">IN THE BOX</h5>
			                    <div class="list-descriptions__description">
			                    	EX SportWatch GPS, Extra Strapes 
									USB charging cable, Quick start guide
								</div>
			                </div>
			                <div class="list-descriptions">
			                    <h5 class="list-descriptions__name">Battery Lifetime</h5>
			                    <div class="list-descriptions__description">
			                    	Upto 50 hours (with average use)
								</div>
			                </div>
			            </div>
			        </div>
	            </div>
	        </div>
	    </div>
	</div>
	<!-- /quality -->

    <!-- view-360 -->
	<div class="view-360 pt-120 pb-80">
		<!-- Container -->
		<div class="container">
			<div class="heading text-uppercase text-center">
				<div class="heading__sub">
					SEE IT IN
				</div>
				<div class="heading__title">
					360 - Degree View
				</div>
			</div>
			<div class="rotate" style="background-image:url(<?php echo e(asset("front/assets/images/bg_rotate.png")); ?>)">
 				<div class="spritespin"></div>
 			</div>
	 	</div>
	</div>
	 <!-- /view-360 -->

  	<!-- Testimonials-->
	<div class="testimonials section-bg pb-100 pt-100" style="background-image:url(<?php echo e(asset("front/assets/images/bg_testimonial.jpg")); ?>)">
		<!-- Container -->
		<div class="container">
			<div class="heading text-uppercase text-center">
				<div class="heading__sub">
					Testimonials
				</div>
				<div class="heading__title">
					Our Clients Say
				</div>
			</div>
			<div class="testimonials_content text-center">
 				<div class="owl-carousel owl-theme" data-pagination="false" data-nav="true" data-items="1" data-large="1" data-medium="1" data-smallmedium="1" data-extrasmall="1" data-verysmall="1" data-autoplay="true">
		            <div class="item">
		            	<img src="<?php echo e(asset("front/assets/images/icon_testimonial.png")); ?>" alt="">
		                <div class="testimonials__content">
		                	Beware diet advice that recommends "eating light," for that is most certainly the way you become
							a black hole. I wonder if caterpillars know they're gonna fly some day or they just start building
							a cocoon and are like 'why am I doing this'. 
						</div>
		                <div class="testimonials__name"> PAMELA MOORE</div>
		                <div class="testimonials__job"> CEO, Industrial Company  </div>
		            </div>
		            <div class="item">
		            	<img src="<?php echo e(asset("front/assets/images/icon_testimonial.png")); ?>" alt="">
		                <div class="testimonials__content">
		                	Beware diet advice that recommends "eating light," for that is most certainly the way you become
							a black hole. I wonder if caterpillars know they're gonna fly some day or they just start building
							a cocoon and are like 'why am I doing this'. 
						</div>
		                <div class="testimonials__name"> PAMELA MOORE</div>
		                <div class="testimonials__job"> CEO, Industrial Company  </div>
		            </div>
		            <div class="item">
		            	<img src="<?php echo e(asset("front/assets/images/icon_testimonial.png")); ?>" alt="">
		                <div class="testimonials__content">
		                	Beware diet advice that recommends "eating light," for that is most certainly the way you become
							a black hole. I wonder if caterpillars know they're gonna fly some day or they just start building
							a cocoon and are like 'why am I doing this'. 
						</div>
		                <div class="testimonials__name"> PAMELA MOORE</div>
		                <div class="testimonials__job"> CEO, Industrial Company  </div>
		            </div>
		        </div>
 			</div>
	 	</div>
	</div>
	<!-- /Testimonials-->

	<!-- product-->
	<div class="product products-grid pt-100 pt-80">
		<!-- Container -->
		<div class="container">
			<div class="heading text-uppercase text-center">
				<div class="heading__sub">
					Our products
				</div>
				<div class="heading__title">
					choose the products
				</div>
			</div>
			<div  class="slick-product">
				<div class="owl-carousel owl-theme list-products" data-pagination="true" data-nav="false" data-items="3" data-large="3" data-medium="3" data-smallmedium="2" data-extrasmall="1" data-verysmall="1" data-autoplay="true">
		 			<div class="item">
		 				<div class="product-block" data-publish-date="">
								<div class="product-image">
									<div class="product-thumbnail">
										<a href="product_single.html" title="">
											<img class="product-featured-image" src="<?php echo e(asset("front/assets/images/product/product_1.jpg")); ?>" alt="">
										</a>
									</div>
								
									<div class="product-actions">
										<a href="#" data-id="" class="btn wishlist product-quick-whistlist" title="Add to whistlist">
										<i class="fa fa-heart-o"></i>
										</a>
										<a href="" data-id="" class="btn product-quick-view btn-quickview" title="Quickview">
										    <i class="fa fa-eye"></i>
										</a>
										<a href="" data-id="" class="btn product-quick-compare btn-compare" title="Compare">
										    <i class="fa fa-retweet"></i>
										</a>
									</div>
								</div><!-- /.product-image -->
								<div class="product-meta">
									<span class="product-rating" data-rating="">
										<span class="star-rating">
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
										</span>
									</span>
									<!-- end rating -->
									<h4 class="product-name">
										<a href="product_single.html" title="">
											Casual Premium edition
										</a>
									</h4>
									<div class="product-price">
										<span class="amout">
											<span class="money" data-currency-usd="$700.00">$700.0</span>
										</span>
										<a href="product_single.html" class="add_to_cart_button">Add to Cart</a>
									</div>
								</div><!-- /.product-meta -->
							</div>
		 			</div>
		 			<div class="item">
		 				<div class="product-block" data-publish-date="">
								<div class="product-image">
									<div class="product-thumbnail">
										<a href="product_single.html" title="">
											<img class="product-featured-image" src="<?php echo e(asset("front/assets/images/product/product_2.jpg")); ?>" alt="">
										</a>
									</div>
									
									<div class="product-actions">
										<a href="#" data-id="" class="btn wishlist product-quick-whistlist" title="Add to whistlist">
										<i class="fa fa-heart-o"></i>
										</a>
										<a href="" data-id="" class="btn product-quick-view btn-quickview" title="Quickview">
										    <i class="fa fa-eye"></i>
										</a>
										<a href="" data-id="" class="btn product-quick-compare btn-compare" title="Compare">
										    <i class="fa fa-retweet"></i>
										</a>
									</div>
								</div><!-- /.product-image -->
								<div class="product-meta">
									<span class="product-rating" data-rating="">
										<span class="star-rating">
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
										</span>
									</span>
									<!-- end rating -->
									<h4 class="product-name">
										<a href="product_single.html" title="">
											Casual Premium edition
										</a>
									</h4>
									<div class="product-price">
										<span class="amout">
											<span class="money" data-currency-usd="$700.00">$700.0</span>
										</span>
										<a href="product_single.html" class="add_to_cart_button">Add to Cart</a>
									</div>
								</div><!-- /.product-meta -->
							</div>
		 			</div>
		 			<div class="item">
		 				<div class="product-block" data-publish-date="">
								<div class="product-image">
									<div class="product-thumbnail">
										<a href="product_single.html" title="">
											<img class="product-featured-image" src="<?php echo e(asset("front/assets/images/product/product_3.jpg")); ?>" alt="">
										</a>
									</div>
									
									<div class="product-actions">
										<a href="#" data-id="" class="btn wishlist product-quick-whistlist" title="Add to whistlist">
										<i class="fa fa-heart-o"></i>
										</a>
										<a href="" data-id="" class="btn product-quick-view btn-quickview" title="Quickview">
										    <i class="fa fa-eye"></i>
										</a>
										<a href="" data-id="" class="btn product-quick-compare btn-compare" title="Compare">
										    <i class="fa fa-retweet"></i>
										</a>
									</div>
								</div><!-- /.product-image -->
								<div class="product-meta">
									<span class="product-rating" data-rating="">
										<span class="star-rating">
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
										</span>
									</span>
									<!-- end rating -->
									<h4 class="product-name">
										<a href="product_single.html" title="">
											Casual Premium edition
										</a>
									</h4>
									<div class="product-price">
										<span class="amout">
											<span class="money" data-currency-usd="$700.00">$700.0</span>
										</span>
										<a href="product_single.html" class="add_to_cart_button">Add to Cart</a>
									</div>
								</div><!-- /.product-meta -->
							</div>
		 			</div>
		 			<div class="item">
		 				<div class="product-block" data-publish-date="">
								<div class="product-image">
									<div class="product-thumbnail">
										<a href="product_single.html" title="">
											<img class="product-featured-image" src="<?php echo e(asset("front/assets/images/product/product_1.jpg")); ?>" alt="">
										</a>
									</div>
									
									<div class="product-actions">
										<a href="#" data-id="" class="btn wishlist product-quick-whistlist" title="Add to whistlist">
										<i class="fa fa-heart-o"></i>
										</a>
										<a href="" data-id="" class="btn product-quick-view btn-quickview" title="Quickview">
										    <i class="fa fa-eye"></i>
										</a>
										<a href="" data-id="" class="btn product-quick-compare btn-compare" title="Compare">
										    <i class="fa fa-retweet"></i>
										</a>
									</div>
								</div><!-- /.product-image -->
								<div class="product-meta">
									<span class="product-rating" data-rating="">
										<span class="star-rating">
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
										</span>
									</span>
									<!-- end rating -->
									<h4 class="product-name">
										<a href="product_single.html" title="">
											Casual Premium edition
										</a>
									</h4>
									<div class="product-price">
										<span class="amout">
											<span class="money" data-currency-usd="$700.00">$700.0</span>
										</span>
										<a href="product_single.html" class="add_to_cart_button">Add to Cart</a>
									</div>
								</div><!-- /.product-meta -->
							</div>
		 			</div>
			 	</div>
	 		</div>
	 	</div>
	</div>
	<!-- /product-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make("layouts.frontend.theme", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>