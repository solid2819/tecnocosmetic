<?php $__env->startSection("title"); ?>
	| Edición de producto
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
        <?php if(count($errors)>0): ?>
            <?php echo $__env->make("layouts.messages", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <?php endif; ?>
       <div class="page-header">
          <h1 class="page-title font_lato">
            Estas editando el producto : <?php echo e($product->name); ?>

          </h1>
          <div class="page-header-actions">
            <ol class="breadcrumb">
              <li><a href="<?php echo e(URL::to('/dashboard')); ?>"><?php echo e(trans('app.home')); ?></a></li>
              <li><a href="<?php echo e(URL::to('/dashboard/products')); ?>"><?php echo e(trans('Products')); ?></a></li>
              <li class="active">Edit</li>
            </ol>
          </div>
        </div> 
	     <div class="col-sm-12 col-md-12 col-lg-12">
          <!-- Panel Floating Lables -->
         <div class="panel">
            <div class="panel-heading">
              <h2 class="panel-title">Editar producto</h2>
               <div class="container">
                 <a href="<?php echo e(route("product-attributes", $product->id)); ?>" class="btn-xs btn btn-outline btn-primary ">
                  <i class="fa fa-edit"></i> Editar Atributos
                 </a>
              </div>
              <div class="container">
                <p class="pull-right">Creado el: <span style="font-weight: bold"><?php echo e($product->created_at->format('d-m-Y')); ?></span></p>
              </div>
              <div class="container">
                <p class="pull-right">Actualizado el: <span style="font-weight: bold"><?php echo e($product->updated_at->format('d-m-Y')); ?></span></p>
              </div>
            </div>
            <div class="panel-body">
              
            <?php echo Form::model($product,["method" => "PUT", "route" => ["products.update", $product->id], "files" => true]); ?>

                    <?php echo Form::token(); ?>

             <?php echo $__env->make("dashboard.products.partials.form-edit", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo Form::close(); ?>


            </div>
          </div>
          <!-- End Panel Floating Lables -->
        </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>