<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Category;
use App\Product;
use DB;

class TrashController extends Controller
{
    public function categories_products_trash(Request $request)
    {
        if($request){
            $query=trim($request->get('searchTextTrash'));//filtro de busqueda de la papelera
            $categories=DB::table('categories')
                ->where('name', 'LIKE', '%' .$query. '%')
                ->where('status', '=' , 'No activa')
                ->orderBy('id', 'desc')
                ->paginate(10);

            return view("dashboard.products.categories.trash", ["categories" => $categories, "searchTextTrash"=>$query]);

        }
    }

    //Listado de producto de la papelera
     public function product_softDeletes()
    {
        $products_trash = Product::onlyTrashed()->paginate(10);
        return view("dashboard.products.trash", compact("products_trash"));
    }

    //Elimina permanentemente un producto de la papelera
    public function product_hard_delete($id)
    {
        $product_hard_delete = Product::withTrashed()->where("id", $id)->forceDelete();

        return redirect()->back()->with("info", "¡Producto eliminado del sistema!");
    }


    //Borrar imagenes en la vista dashboard.products.edit
    public function delete_image_product($id=null)
    {

       //Get Product Image Name
        $productImage = Product::where(["id"=>$id])->first();
        // echo $productImage->image;

        //Variable ruta de la imagen
        $image_path= "images/dashboard/products/";

        //Borrar imagen de la carpeta de almacenamiento de las imagenes de los productos, si tal imagen existe.
        if (file_exists($image_path.$productImage->image)) {
            unlink($image_path.$productImage->image);
        }

        //Delete image from table products
       Product::where(["id"=>$id])->update(["image"=>""]); 

       return redirect()->back()->with("info", "Se ha eliminado la imagen del producto");
    }
}
