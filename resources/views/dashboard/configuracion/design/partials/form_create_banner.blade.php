@section("styles")
   <link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/blueimp-file-upload/jquery.fileupload.css">
  <link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/dropify/dropify.css">
@endsection

<div class="row">

  <div class="col-xs-12  col-sm-12 col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      {{ Form::label("name", "Nombre del Banner") }}
      {{ Form::text("title",null, ["class" => "form-control", "id" => "title"])}} 
    </div>

    <div class="form-group">
      {{ Form::label("slug", "Slug") }}
      {{ Form::text("url",null, ["class" => "form-control", "id" => "slug"])}} 
    </div>
  </div>

   <div class="col-xs-12  col-sm-12 col-md-6 col-lg-6 col-xl-6">
    <input type="file" name="image">

    {{--  --}}
    
  </div>


</div>
<div class="row">
  <div class="col-md-9">
    

   <div class="form-group">
      {{ Form::label("description", "Descripción") }}
      {{ Form::textarea("description",null, ["class" => "form-control", "id" => "description"])}} 
    </div>


    <div class="form-group">
      <div class="checkbox-custom checkbox-primary">
        <input type="hidden" id="status" name="status" value="PUBLISHED" />
       
      </div>
    </div>

  </div>
  
</div>

<div class="row">
    <div class="form-group">
      {{ Form::submit("Crear", ["class" => "btn btn-success btn-sm"]) }}

       <button type="reset" class="btn btn-danger  btn-sm">
        Borrar
      </button>

      <a href="{{ url("dashboard/config/design/banners") }}">
        <button type="button" class="btn btn-warning btn-sm">
          Volver
        </button>
      </a>
      
    </div>
</div>


@section("scripts")
  <script src="{{ asset("vendor/jquerystringToSlug/jquery-stringToSlug.min.js") }} "></script>
  <script src="{{ asset("vendor/ckeditor/ckeditor.js") }} "></script>
  
  
  <script>
    $(document).ready(function () {
      $("#title, #slug").stringToSlug({
        callback: function (text) {
          $("#slug").val(text);
        }
      });
    });

    /*CK EDITOR*/
    CKEDITOR.config.height=400;
    CKEDITOR.config.width="auto";

    CKEDITOR.replace("description");

    $(document).ready(function() {
      $("#form-products").validate();
    });
  </script>
@endsection