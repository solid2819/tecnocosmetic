                <div class="modal fade modal-3d-flip-vertical" id="modal-delete-<?php echo e($banner_t->id); ?>" aria-hidden="true"
                  aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
                 
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header bg-danger">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <h4 class="modal-title" style="color: white!important;">Eliminar del sistema</h4>
                        </div>
                        <div class="modal-body">
                          <p>¿Estas seguro de eliminar el banner <?php echo e($banner_t->title); ?> permanentemente? <br>
                            <small class="text-warning">Advertencia: Si continuas ya no podrás recuperar el banner</small>
                          </p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-outline btn-default margin-0" data-dismiss="modal">Cancelar</button>
                         <a href="<?php echo e(url("dashboard/config/d-sign/banners/delete", $banner_t->id)); ?>">
                            <button type="submit" class="btn btn-outline btn-danger">Continuar</button>
                         </a>
                        </div>
                      </div>
                    </div>
                   
                  </div>

