<?php $__env->startSection("title"); ?>
	Login
<?php $__env->stopSection(); ?>


<?php $__env->startSection("content"); ?>
	<div class="breadcrumb">
   		<!-- container -->
		<div class="container">
			<h1>Login</h1>
			<ol class="item-breadcrumb">
	            <li><a href="index.html">Home</a></li>
	            <li>Login</li>     
            </ol>
		</div>
		<!-- /container -->
   	</div>
	<div class="page-account">
		<!-- container -->
		<div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="account-wrapper">
                    	<ul class="account-tab-list nav">
                        	<li class="active"><a data-toggle="tab" href="#">Ingresar</a></li>
                        </ul>  
                        	<?php $__currentLoopData = $settingdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $view): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>	
                                <div class="account-form-container register-form">
                                    <div class="account-form">
                                       <form class="form-horizontal" role="form"method="POST" action="<?php echo e(url('/login')); ?>">
							                   <?php echo e(csrf_field()); ?>				   
											<div class="form-group<?php echo e($errors->has('username') ? ' has-error' : ''); ?>">                         
														<input id="email" type="text" class="form-control" placeholder="Igresar E-mail" name="email" value="<?php echo e(old('email')); ?>" required>

														<?php if($errors->has('email')): ?>
															<span class="help-block">
																<strong><?php echo e($errors->first('email')); ?></strong>
															</span>
														<?php endif; ?>				  
												</div>
												
												<div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">				   
														<input id="password_id" type="password" required class="form-control" name="password" placeholder="Contraseña">
														<?php if($errors->has('password')): ?>
															<span class="help-block">
																<strong><?php echo e($errors->first('password')); ?></strong>
															</span>
														<?php endif; ?>
												</div>

												 <div class="form-group clearfix">
												  <?php if($view->remember_me == 'ON'): ?>
										            <div class="checkbox-custom checkbox-inline checkbox-primary pull-left">
										              <input type="checkbox" id="remember" name="checkbox">
										              <label for="inputCheckbox"><?php echo e(trans('Recordar')); ?></label>
										            </div>
													<?php endif; ?>
													 
														<a class="pull-right" href="<?php echo e(url('/password/reset')); ?>"><?php echo e(trans('Recuperar contraseña')); ?> </a>
													 
													
												  </div>
								         <button type="submit" class="btn btn-primary ladda-button btn-block" data-plugin="ladda" data-style="expand-left">
											  <?php echo e(trans('Entrar')); ?>

										<span class="ladda-spinner"></span><div class="ladda-progress" style="width: 0px;"></div>
										</button>
										
									</form>
                                    </div>

                                </div>
                    </div>
                </div>
            </div>
		</div>
		<!-- /container -->
	</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopSection(); ?>


<?php echo $__env->make("layouts.frontend.theme", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>