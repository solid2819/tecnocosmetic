<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;


use Illuminate\Support\Facades\Redirect;
use App\Category;
use DB;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }
    
    public function index(Request $request)
    {
        if($request){
            $query=trim($request->get('searchText'));//filtro de busqueda
            $categories=DB::table('categories')
                ->where('name', 'LIKE', '%' .$query. '%')
                ->where('status', '=' , 'Activa')
                ->orderBy('id', 'desc')
                ->paginate(10);

            return view("dashboard.products.categories.index", ["categories" => $categories, "searchText"=>$query]);

        }
    }
    
    public function create()
    {
        $levels = Category::where(['parent_id'=>0])->get();
        return view("dashboard.products.categories.create", compact("levels"));
    }

    
    public function store(CategoryStoreRequest $request)
    {
        if ($request->isMethod("post")) {
            $data = $request->all();
            // if (empty($data["status"])) {
            //     $status=0;
            // }else{
            //     $status=1;
            // }

            $category = new Category;
            $category->parent_id= $data["parent_id"];
            $category->name= $data["name"];
            $category->slug= $data["slug"];
            // $category->status= $status;
            $category->description= $data["description"];
            $category->save();
        }

         return redirect()->route("categories.index")->with("info", "¡Categoría creada con éxito!");
    }

    
    public function show($id)
    {
        // return view("dashboard.products.categories.show", ['category'=>Category::findOrFail($id)]);
    }

    
    public function edit($id)
    {
        $category = Category::find($id);
        $level = Category::where(['parent_id'=>0])->get();
        return view("dashboard.products.categories.edit", compact("category", "level"));
    }

   
    public function update(CategoryUpdateRequest $request, $id)
    {
        // if ($request->ismethod("post")) {
        //     $data=$request->all();

        //     if (empty($data["status"])) {
        //         $status=0;
        //     }else{
        //         $status=1;
        //     }

        //  Category::where(["id"=>$id])->update(["name"=>$data["name"], "slug"=>$data["slug"], "description"=>$data["description"], "status"=>$status]);
        // 
        //}


        // $category=Category::findOrFail($id);
        // $category->parent_id=$request->get("parent_id");
        // $category->name=$request->get("name");
        // $category->slug=$request->get("slug");
        
        // $category->status=$status;
        // $category->description=$request->get("description");
        // $category->update();

        $category = Category::find($id);

        $category->fill($request->all())->save();

        return Redirect::to("dashboard/product/categories")->with("info", "La categoría <span class='text-uppercase'>". $category->name."</span> se ha actualizado con éxito");
    }

    
    public function destroy($id)
    {
        $category = Category::find($id)->delete();
        // $category = json_decode(json_encode($category));

        return back()->with("info", "La Categoría ha sido enviada a la papelera" );
    }

    // public function categories_products_restore($id)
    // {
    //     $category = Category::find($id);

    //     $category->status="Activa";
    //     $category->save();

    //     return redirect()->route("categories_products_restore")->with("info","restaurada");
    // }

}
