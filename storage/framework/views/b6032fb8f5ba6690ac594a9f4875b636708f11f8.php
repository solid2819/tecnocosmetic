<?php $__env->startSection("title"); ?>
	| Crear categoría del producto
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
      <div class="page-header">
          <h1 class="page-title font_lato">
            Nueva Categoría
          </h1>
          <div class="page-header-actions">
            <ol class="breadcrumb">
              <li><a href="<?php echo e(URL::to('/dashboard')); ?>"><?php echo e(trans('app.home')); ?></a></li>
              <li><a href="<?php echo e(URL::to('/dashboard/product/categories')); ?>"><?php echo e(trans('Categories')); ?></a></li>
              <li class="active">Create</li>
            </ol>
          </div>
        </div> 
	     <div class="col-sm-12 col-md-6 ol-lg-6 col-md-offset-2">
          <!-- Panel Floating Lables -->
          <?php if(count($errors)>0): ?>
            <?php echo $__env->make("layouts.messages", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <?php endif; ?>
         <div class="panel">
            <div class="panel-heading">
              <h2 class="panel-title">Crear nueva categoría</h2>
            </div>
            <div class="panel-body">
              
            <?php echo Form::open(["route" => "categories.store"]); ?>

             <?php echo $__env->make("dashboard.products.categories.partials.form-create", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo Form::close(); ?>


            </div>
          </div>
          <!-- End Panel Floating Lables -->
        </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>