<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTrashTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_trash', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id');

            $table->string("name",100);
            $table->string("slug",200)->unique();
            $table->text("description");
            $table->tinyInteger("status");
            $table->rememberToken();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories_trash');
    }
}
