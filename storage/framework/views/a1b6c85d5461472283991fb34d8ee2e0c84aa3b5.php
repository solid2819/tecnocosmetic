<?php $__env->startSection("title"); ?>
	| Productos
<?php $__env->stopSection(); ?>
<?php $__env->startSection("styles"); ?>
  <link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/global/vendor/webui-popover/webui-popover.css">
  <link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/global/vendor/toolbar/toolbar.css">
  <style type="text/css" media="screen">
    a{
      text-decoration: none!important;
    }  
    .td1{
      width: 4vw!important;
  </style>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php echo $__env->make("layouts.messages", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="page-header">
    <h1 class="page-title font_lato">
      Productos

    </h1>
    <div class="page-header-actions">
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/dashboard')); ?>"><?php echo e(trans('app.home')); ?></a></li>
        <li class="active">Products</li>
      </ol>
    </div>
  </div>
	 <div class="panel">
        <div class="panel-body">
          <!-- Categories table -->
          <div class="example-wrap">
            <h2 class="example-title">Lista de productos</h2>
            <p> 
              <a href="<?php echo e(url("dashboard/products/create")); ?>">
                <button class="fa fa-plus btn btn-outline btn-primary btn-xs "> Nuevo</button>
              </a>
            </p>
            <div class="example">

             <div class="row">
              <div class="col-md-5">
                <a href="<?php echo e(url("dashboard/product/trash")); ?>" class="text-danger">
                  <button class="btn btn-outline btn-danger btn-xs btn1" data-content="Papelera"data-trigger="hover" data-toggle="popover" tabindex="0">
                    <i class="fa fa-trash-o"> </i>
                  </button>
                </a>
              </div>
               <div class="col-md-5 pull-right">
                <?php if(count($products)): ?>
                <?php echo $__env->make("dashboard.products.partials.search", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
              </div>
             </div>  

              <div class="table-responsive">
                  <table class="table table-hover table-condensed table-bordered table-striped">
                  <thead>
                    <tr>
                      <td class="text-center td1"><input type="checkbox" name="" value="">
                       All
                      </td>
                      <th class="text-center">&nbsp</th>
                      <th class="text-center">Código</th>
                      <th class="text-center">Producto</th>
                      <th class="text-center">Categoría</th>
                      
                     
                      <th class="text-center"><span class="fa fa-wrench"></span></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td class="text-center td1"><input type="checkbox" name="" value=""> &nbsp 
                        <?php echo e($product->id); ?>

                      </td>
                      <td class="text-center">
                        <?php if($product->image): ?>
                          <img src="<?php echo e(asset("images/dashboard/products/$product->image")); ?>" width="150px">
                        <?php else: ?>
                          <span class="fa fa-image"></span>
                        <?php endif; ?>
                        <p>
                            <?php if($product->status==1): ?>
                              <small class="text-success">Producto publicado</small>
                            <?php else: ?>
                              <small class="text-danger">No publicado</small>
                            <?php endif; ?>
                          </p>
                      </td>
                      <td><?php echo e($product->code); ?></td>
                      <td class="text-center">
                            <a class="text-capitalize" href="<?php echo e(route("products.edit", $product->id )); ?>"><?php echo e($product->name); ?>  
                             &nbsp 
                              <i class="fa fa-pencil" aria-hidden="true"></i>
                            </a>
                          <br>
                      </td>
                      <td class="text-center text-capitalize"><?php echo e($product->category_name); ?></td>
                     
                      
                      <td class="text-center">
                         <div class="toolbar-icons hidden" id="set-05-options">
                            <a href="javascript:void(0)" type="button" data-target="#modal-view-<?php echo e($product->id); ?>" data-toggle="modal">
                              <i class="fa fa-eye" aria-hidden="true"></i>
                            </a>
                            <a href="javascript:void(0)" data-target="#modal-delete-<?php echo e($product->id); ?>" data-toggle="modal" type="button">
                              <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                        </div>
                        <a href="<?php echo e(route("product-attributes", $product->id)); ?>">
                           <button class="btn btn-success btn-icon btn-outline btn-round"   
                               data-plugin="webuiPopover"
                               data-trigger="hover" 
                               data-placement="left" 
                               data-delay-show="0"
                               data-delay-hide="0" 
                               data-title="Añadir atributos" 
                               data-content="&lt;p&gt; Edita el producto en el apartado de Atributos del producto .&lt;/p&gt;"  
                               data-toolbar-style="primary" >
                               <i class="fa fa-edit" aria-hidden="true"></i>
                          </button>
                        </a>
                        <button class="btn btn-primary btn-icon btn-outline btn-round" 
                                data-plugin="toolbar" 
                                data-toolbar="#set-05-options" 
                                data-toolbar-animation="grow" 
                                data-toolbar-style="primary" 
                                type="button">
                                <i class="icon wb-settings" aria-hidden="true"></i>
                        </button>
                      </td>
                    </tr>
                    <?php echo $__env->make("dashboard.products.partials.modal_delete", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make("dashboard.products.partials.modal_details", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                </table>
                <?php else: ?>
                  <p>Lista de productos vacía.</p>
                <?php endif; ?>
                <?php echo e($products->render()); ?>

              </div>
            </div>
          </div>
          <!-- End table categories -->
          
        </div>
      </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>