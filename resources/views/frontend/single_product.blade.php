@extends("layouts.frontend.theme")
@section("title")
	{{$productDetails->name}}
@endsection
@section("styles")
	<style type="text/css" media="screen">
		.quantity-item{
			width: 5vw!important;
		}

		
	</style>
@endsection

@section("content")
	@include("layouts.messages")
		<div class="breadcrumb">
	   		<!-- container -->
			<div class="container">
				<h1>{{$productDetails->name}}</h1>
				<ol class="item-breadcrumb">
		            <li><a href="{{ url("/") }}">Inicio</a></li>
		            <li><a href="{{ route("products") }}">Productos</a></li>
		            <li class="active">{{$productDetails->name}}</li>     
	            </ol>
			</div>
			<!-- /container -->
	   	</div>

	   	<div class="single-product">
			<!-- Container -->
			<div class="container">
				<!-- Row -->
				<div class="row">
					<div class="product-info">
						<div class="col-md-6">
							<div class="row">
		                        <ul class="featuredPostSlider col-md-10 col-sm-11">
				                     <li>
				                        <img class="product-featured-image" src="{{ asset("images/dashboard/products/" .$productDetails->image) }}" alt="">
				                    </li>
				                     <li>
				                       <img class="product-featured-image" src="{{ asset("images/dashboard/products/" .$productDetails->image2) }}" alt="">
				                    </li>		                     
				                    <li>
				                       <img class="product-featured-image" src="{{ asset("images/dashboard/products/" .$productDetails->image3) }}" alt="">
				                    </li>
				                     <li>
				                        <img class="product-featured-image" src="{{ asset("images/dashboard/products/" .$productDetails->image4) }}" alt="">
				                    </li>
		                   		</ul>
			                    <div id="slick-page">
				                    <div class="pagerNavigation pagerNavigationTop">
				                        <span class="top"><i class="fa fa-angle-up"></i></span>                        
				                    </div>                    
				                    <div class="slick-pager">
				                      	<a data-slide-index="1" href="javascript:void(0)">    
				                      	  <img class="product-featured-image" src="{{ asset("images/dashboard/products/" .$productDetails->image) }}" alt="">                  
				                      	</a>
				                      	<a data-slide-index="3" href="javascript:void(0)">    
				                      	  <img class="product-featured-image" src="{{ asset("images/dashboard/products/" .$productDetails->image2) }}" alt="">                   
				                      	</a>
				                      	<a data-slide-index="4" href="javascript:void(0)">    
				                      	  <img class="product-featured-image" src="{{ asset("images/dashboard/products/" .$productDetails->image3) }}" alt="">                  
				                      	</a>
				                      	<a data-slide-index="5" href="javascript:void(0)">    
				                      	  <img class="product-featured-image" src="{{ asset("images/dashboard/products/" .$productDetails->image4) }}" alt="">                   
				                      	</a>
				                    </div>
				                    <div class="pagerNavigation pagerNavigationBottom">                        
				                        <span class="bottom"><i class="fa fa-angle-down"></i></span>
				                    </div>
				                </div>
			                </div>
				 		</div>

				 		{{-- Form add to cart --}}
				 		<form name="addtocartForm" id="addtocartForm" action="{{ url("/product/add-cart") }}" method="post"> 
				 			{{ csrf_field() }}
				 			<input type="hidden" name="product_id" value="{{ $productDetails->id }}">
				 			<input type="hidden" name="name" value="{{ $productDetails->name }}">
				 			<input type="hidden" name="code" value="{{ $productDetails->code }}">
				 			<input type="hidden" name="color" value="{{ $productDetails->color }}">
				 			<input type="hidden" name="price" id="price" value="{{ $productDetails->price }}">

				 			<div class="col-md-6">
							<div class="product-meta clearfix">
								<span class="product-rating" data-rating="">
									<span class="star-rating">
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
									</span>
								</span>
								<!-- end rating -->
								<h4 class="product-name">
									<a href="#" title="">{{$productDetails->name}}</a>
								</h4><!-- /.product-product -->

								<div class="product-price">
									<span class="amout">
										<span>
											<span class="money" data-currency-usd="${{$productDetails->price}}" id="getPrice" style="font-size: 3rem;">$ {{$productDetails->price}}</span>
										</span>
									</span>
								</div><!-- /.product-price -->
								<div class="excerpt">

									<p>{{ $productDetails->exerpt }}</p>
							        <p>Stock: 
										<span id="availability">
											@if($total_stock>0)Disponible @else No Disponible @endif 
										 </p>
										</span>  

									@if($productDetails->care)
										<p>Material de Diseño: {{ $productDetails->care }}</p>  
									@endif         
							    </div>

	                            <table class="variations">
	                            	<tbody>
	                            		
	                            		<tr>
	                            			<td class="label"><label>Size:</label></td>
	                            			<td class="value">
	                            				<div class="tawcvs-swatches" id="size_selector" data-attribute_name="attribute_pa_size">
	                            					<select name="size" class="form-control" id="selSize">
	                            						<option value="">Seleccionar Tamaño</option>
	                            						@foreach($productDetails->attributes as $sizes)
	                            							<option value="{{$productDetails->id}}-{{ $sizes->size }}">
	                            								{{ $sizes->size }}
	                            							</option>
	                            						@endforeach
	                            					</select>
	                            				</div>
	                            			</td>
	                            		</tr>
	                            		<tr>
	                            			<td class="label"><label>Color:</label></td>
	                            			<td class="value">
	                            				<div class="tawcvs-swatches color_selector" data-attribute_name="attribute_pa_color">
	                            					<span class="swatch swatch-color .color-item" title="Blue" data-value="blue">{{ $productDetails->color }}</span>
	                            					
	                            				</div>
	                            			</td>
	                            		</tr>
	                            	</tbody>
	                            </table>

							    <div class="product-footer">
									{{-- <form action="#" class="add-to-cart" method="post" enctype="multipart/form-data">
										<input class="cart-qty-box" type="number" name="qtybutton" value="0">
										<input type="hidden" name="id" value="">
									</form> --}}

									{{-- @if($total_stock>0) --}}
										<button type="submit" class="btn btn-primary" id="cartButton">Add to Cart<i class="fa fa-shopping-bag" aria-hidden="true"></i></button>
									{{-- @endif --}}

									<div class="product-actions">
										<span>
										<input type="number" name="quantity" value="1" width="50px" class="quantity-item" minlength="1">
											
										</span>
										<a href="#" data-id="" class="btn wishlist product-quick-whistlist" title="Añadir ala lista de Favoritos">
										<i class="fa fa-heart-o"></i>
										</a>
										<a href="" data-id="" class="btn product-quick-compare btn-compare" title="Comparte">
										    <i class="fa fa-retweet"></i>
										</a>
									</div>
								</div>
								<div class="social-sharing">
									<h6 class="text-uppercase">Share:</h6>
	                                <a class="facebook social-icon" href="#"><i class="fa fa-facebook"></i></a>
	                                <a class="twitter social-icon" href="#"><i class="fa fa-twitter"></i></a>
	                                <a class="pinterest social-icon" href="#"><i class="fa fa-pinterest"></i></a>
	                                <a class="gplus social-icon" href="#"><i class="fa fa-google-plus"></i></a>
	                                <a class="linkedin social-icon" href="#"><i class="fa fa-linkedin"></i></a>
								</div>
							</div><!-- /.product-meta -->
				 			</div>
				 		</form>
				 		{{-- End form add to cart --}}

				 	</div>
				 	<!-- product-tab-description -->
				 	<div class="col-md-12 product-description-tabs">
		                <ul class="nav">
						    <li class="active"><a data-toggle="tab" href="#tab_description">Descripción</a></li>
						    <li><a data-toggle="tab" href="#tab_information"> Detalles</a></li>
						    <li><a data-toggle="tab" href="#tab_reviews">Review</a></li>
						</ul>
						<div class="tab-content">
						    <div id="tab_description" class="tab-pane fade in active">
						      	{!! $productDetails->description !!}
						    </div>
						    <div id="tab_information" class="tab-pane fade">
						      	In a urna a ipsum lacinia fermentum id nec est. Ut eleifend neque at rutrum malesuada. Aenean dignissim rhoncus felis, sed varius eros elementum vitae. Donec vitae molestie tellus. Mauris mollis vestibulum condimentum. Curabitur eu nibh vitae ante rhoncus auctor tincidunt vitae ligula. Morbi tempus sit amet risus a scelerisque. Duis eget condimentum nibh, sit amet tincidunt lacus. Quisque vel diam suscipit neque bibendum viverra in eu nisi.
						      	In a urna a ipsum lacinia fermentum id nec est. Ut eleifend neque at rutrum malesuada. Aenean dignissim rhoncus felis, sed varius eros elementum vitae. Donec vitae molestie tellus. Mauris mollis vestibulum condimentum. Curabitur eu nibh vitae ante rhoncus auctor tincidunt vitae ligula. Morbi tempus sit amet risus a scelerisque. Duis eget condimentum nibh, sit amet tincidunt lacus. Quisque vel diam suscipit neque bibendum viverra in eu nisi.
						    </div>
						    <div id="tab_reviews" class="tab-pane fade">
					            <div class="rattings-wrapper">
					            	<div class="review-ratting">
	                                    <div class="star-author-all">
	                                        <div class="ratting-author">
	                                            <h3>Jane Watson</h3>
	                                            <span>12:24</span>
	                                            <span>9 March 2018</span>
	                                        </div>
	                                        <div class="ratting-star">
	                                            <span class="fa fa-star" aria-hidden="true"></span>
	                                            <span class="fa fa-star" aria-hidden="true"></span>
	                                            <span class="fa fa-star" aria-hidden="true"></span>
	                                            <span class="fa fa-star" aria-hidden="true"></span>
	                                            <span class="fa fa-star" aria-hidden="true"></span>
	                                            <span>(5)</span>
	                                        </div>
	                                    </div>

	                                    <div class="reviews_content clearfix">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nost rud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nost.</div>
	                                </div>
	                                <div class="review-ratting">
	                                    <div class="star-author-all">
	                                        <div class="ratting-author">
	                                            <h3>Emma Doe</h3>
	                                            <span>12:24</span>
	                                            <span>9 March 2018</span>
	                                        </div>
	                                        <div class="ratting-star">
	                                            <span class="fa fa-star" aria-hidden="true"></span>
	                                            <span class="fa fa-star" aria-hidden="true"></span>
	                                            <span class="fa fa-star" aria-hidden="true"></span>
	                                            <span class="fa fa-star" aria-hidden="true"></span>
	                                            <span class="fa fa-star" aria-hidden="true"></span>
	                                            <span>(5)</span>
	                                        </div>
	                                    </div>

	                                    <div class="reviews_content clearfix">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nost rud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nost.</div>
	                                </div>
					            </div>
						        <form class="form-product-review" action="do_action" method="post">
					                <h5>ADD YOUR REVIEW</h5>
					                <div class="form-group">
				                    	<label class="control-label">Your Rating</label>
					                    <p class="ratting-star">
				                    	 	<span class="fa fa-star" aria-hidden="true"></span>
	                                        <span class="fa fa-star" aria-hidden="true"></span>
	                                        <span class="fa fa-star" aria-hidden="true"></span>
	                                        <span class="fa fa-star" aria-hidden="true"></span>
	                                        <span class="fa fa-star" aria-hidden="true"></span>
				                    	</p>
				                    </div>
				                    <div class="form-group">
				                    	<label class="control-label" for="comment">Your Review</label>
				                    	<textarea id="comment" class="form-control" name="comment" cols="45" rows="8" aria-required="true"></textarea>
				                    </div>
					                <div class="row">
					                    <div class="form-group col-md-6">
					                      	<label for="author" class="control-label">Name 
						                      	<span class="required">*</span>
						                     </label>
					                       <input id="author" class="form-control" name="author" type="text" value="" size="30" aria-required="true">
					                    </div>
					                    <div class="form-group col-md-6">
					                      	<label for="email" class="control-label">Email 
					                      		<span class="required">*</span>
					                      	</label>
					                      	<input id="email" class="form-control" name="email" type="text" value="" size="30" aria-required="true">
					                    </div>
					                    <div class="form-submit col-md-12">
											<input name="submit" type="submit" id="submit" class="btn btn-primary" value="Send"> 
											<input type="hidden" name="comment_post_ID" value="" id="comment_post_ID"> 
											<input type="hidden" name="comment_parent" id="comment_parent" value="">
										</div>
					                </div>
					             </form>
						    </div>    
						</div>
				 	</div>
				 	<!--/product-tab-description -->
		 		</div>
		 	</div>
		 	<!-- end Container -->
		</div>

		{{-- Productos relacionados --}}		
		<div class="up-sells pt-20">
			<!-- Container -->
			<div class="container">
				<!-- Row -->
				<div class="row">
					<div class="col-md-12">
						<div class=" up-sells_content pt-30">
							<h3 class="theme-heading">Productos relacionados</h3>
							<div class="product products-grid">
								<div class="owl-carousel owl-theme" data-pagination="true" data-nav="flase" data-items="3" data-large="3" data-medium="3" data-smallmedium="2" data-extrasmall="1" data-verysmall="1" data-autoplay="true">

									@foreach($relatedProducts->chunk(4) as $chunk)
									@foreach($chunk as $item)
					            		<div class="item">
					            			<div class="product-block" data-publish-date="">
												<div class="product-image ">
													<div class="product-thumbnail">
														<a href="product_single.html" title="">
															<img class="product-featured-image" src="{{ asset("images/dashboard/products/" .$item->image3) }}" alt="">
														</a>
													</div>
													<div class="product-actions">
														<a href="#" data-id="" class="btn wishlist product-quick-whistlist" title="Add to whistlist">
														<i class="fa fa-heart-o"></i>
														</a>
														<a href="" data-id="" class="btn product-quick-view btn-quickview" title="Quickview">
														    <i class="fa fa-eye"></i>
														</a>
														<a href="" data-id="" class="btn product-quick-compare btn-compare" title="Compare">
														    <i class="fa fa-retweet"></i>
														</a>
													</div>
												</div><!-- /.product-image -->
												<div class="product-meta">
													<span class="product-rating" data-rating="">
														<span class="star-rating">
															<i class="fa fa-star-o"></i>
															<i class="fa fa-star-o"></i>
															<i class="fa fa-star-o"></i>
															<i class="fa fa-star-o"></i>
															<i class="fa fa-star-o"></i>
														</span>
													</span>
													<!-- end rating -->
													<h4 class="product-name">
														<a href="product_single.html" title="">
															{{ $item->name }}
														</a>
													</h4>
													<div class="product-price">
														<span class="amout">
															<span class="money" data-currency-usd="$700.00">
																{{ $item->price }}
															</span>
														</span>
														<a href="product_single.html" class="add_to_cart_button">Add to Cart</a>
													</div>
												</div><!-- /.product-meta -->
											</div>
					            		</div>
				            		@endforeach
				            		@endforeach
				            	</div>
						 	</div>
						 </div>
			 		</div>
		 		</div>
		 	</div>
		</div>
		{{-- Productos relacionados --}}

@endsection

@section("scripts")
	 <script src="{{URL::to('/')}}/js/mainFront.js"></script>

@endsection