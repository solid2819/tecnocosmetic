<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();

            $table->string('name', 100);
            $table->string('code');
            $table->string('color');
            $table->string('slug',200);
            $table->string('excerpt',255);
            $table->string('care')->nullable();
            $table->text('description')->nullable();
            $table->float('price', 8,2);
            $table->boolean('status')->default(0);
            $table->string('image',255)->nullable();
             $table->string('image2',255)->nullable();
              $table->string('image3',255)->nullable();
               $table->string('image4',255)->nullable();

            $table->timestamps();

            //Relations
            $table->foreign('category_id')->references('id')->on('categories')
             ->onDelete('cascade')
             ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
