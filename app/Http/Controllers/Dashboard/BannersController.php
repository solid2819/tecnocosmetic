<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Banner;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\BannersUpdateRequest;
use DB;

class BannersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request){
            $query=trim($request->get('searchText'));//filtro de busqueda
            $banner=DB::table('banners')
                ->where('title', 'LIKE', '%' .$query. '%')
                ->orderBy("title", "asc")
                ->paginate(10);
        }

        return view("dashboard.configuracion.design.banners",  ["banner" => $banner, "searchText"=>$query]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        return view("dashboard.configuracion.design.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->isMethod("post")) {
            $data = $request->all();

            $banner = new Banner;
            $banner->title=$data["title"];
            $banner->url=$data["url"];
            $banner->description=$data["description"];
            $banner->status="1";

            if (Input::hasFile("image")) {
            $file = Input::file("image");
            $file->move(public_path() . "/images/frontend/banners", $file->getClientOriginalName());

            $banner->image=$file->getClientOriginalName();
            }

            $banner->save();
        }

        return redirect("dashboard/config/design/banners")->with("info", "Banner creado exitosamente");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $banner = Banner::find($id);

        // return response()->json([
        //     "message" => "datos de banner",
        //     "data" => $banner

        // ], 200);

        return view("dashboard.configuracion.design.edit_banner", compact("banner"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BannersUpdateRequest $request, $id=null)
    {
        
            $banner=Banner::findOrFail($id);

            $banner->title= $request->get("title");
            $banner->url= $request->get("url");
            $banner->description= $request->get("description");
            if (Input::hasFile("image")) {
                $file = Input::file("image");
                $file->move(public_path() . "/images/frontend/banners", $file->getClientOriginalName());

                $banner->image=$file->getClientOriginalName();
            }
            if (!$banner["status"] == "") {
                $status = 0;
            }else{
                 $status = 1;
            }

            $banner->status= $status;

        $banner->update();
       
        return redirect()->back()->with("info", "Banner actualizado");

    }

    /**
     * Enviar recurso a la papelera.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function softDelete($id)
    {
        $banner = Banner::find($id)->delete();

        return back()->with("info", "¡Enviado a papelera!" );
    }

   //Listado de banners en la papelera
    public function banner_trashes()
    {
        $banner_trash = Banner::onlyTrashed()->paginate(10);
         return view("dashboard.configuracion.design.trash", compact("banner_trash"));
    }

    //Restaurar un banners
    public function banner_restore($id)
    {
        $banner_restore = banner::withTrashed()->where("id", $id)->restore();

        return redirect()->route("banners.index")->with("info", "Banner restaurado existosamente!");
    }

    public function banner_hard_delete($id)
    {
        $banner_hard_delete = Banner::withTrashed()->where("id", $id)->forceDelete();

        return redirect()->back()->with("info", "Banner eliminado del sistema!");
    }
}
