@extends('layouts.template')
@section("title")
	| Crear producto 
@endsection
@section('content')
       @if(count($errors)>0)
          @include("layouts.messages"){{--message--}}
      @endif
      <div class="page-header">
          <h1 class="page-title font_lato">
            Nuevo producto
          </h1>
          <div class="page-header-actions">
            <ol class="breadcrumb">
              <li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
              <li><a href="{{URL::to('/dashboard/products')}}">{{ trans('Products')}}</a></li>
              <li class="active">Create</li>
            </ol>
          </div>
        </div> 
	     <div class="col-sm-12 col-md-12 ol-lg-12 ">
          <!-- Panel Floating Lables -->
         <div class="panel">
            <div class="panel-heading">
              <h2 class="panel-title">Crear un nuevo producto</h2>
            </div>
            <div class="panel-body">
              
            {!! Form::open(["route" => "products.store", "id" => "form-products", "enctype" => "multipart/form-data"]) !!}
              {{ csrf_field() }}
             @include("dashboard.products.partials.form-create")
            {!! Form::close() !!}

            </div>
          </div>
          <!-- End Panel Floating Lables -->
        </div>
@endsection