<div class="content-dropdown left">
								<div class="account-inner ">
								    <div class="login-form-head">
								      	<span class="login-form-title">Entrar</span>
								      	<span class="pull-right">
								     	 	<a class="register-link" href="{{ route("register") }}" title="Register">
								     	 	 Crear una cuenta
								     	   </a>
								     	 </span>
								    </div>
								    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        				{{ csrf_field() }}         
		 
										<div>
											<input id="email" type="text" class="" placeholder="Igresar E-mail" name="email" value="{{ old('email') }}" required>

											@if ($errors->has('email'))
												<span class="help-block">
													<strong>{{ $errors->first('email') }}</strong>
												</span>
											@endif					
										</div>
			
										 <div>			
												<input id="password" type="password" required class="form-control" name="password" placeholder="Contraseña">

												@if ($errors->has('password'))
													<span class="help-block">
														<strong>{{ $errors->first('password') }}</strong>
													</span>
												@endif
										</div>
										<button type="submit" class="btn btn-primary ladda-button btn-block" data-plugin="ladda" data-style="expand-left">
											  {{ trans('Entrar')}}
										<span class="ladda-spinner"></span><div class="ladda-progress" style="width: 0px;"></div>
										</button>
										
									</form>
								    <div class="login-form-bottom">
								      		<a href="{{ url('/password/reset') }}" class="lostpass-link" title="Lost your password?"><small>Recuperar contraseña</small></a>
								    </div>          
								</div>
							</div>