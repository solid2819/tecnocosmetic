<?php $__env->startSection("title"); ?>
	| Categorías recicladas
<?php $__env->stopSection(); ?>
<style type="text/css" media="screen">
  a{
    text-decoration: none!important;
  }  
  .btn1{
      width: 3.7vw;
    }
</style>

<?php $__env->startSection('content'); ?>
   <?php echo $__env->make("layouts.messages", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
   <div class="page-header">
      <h1 class="page-title font_lato">
        Papelera
      </h1>
      <div class="page-header-actions">
          <ol class="breadcrumb">
          <li><a href="<?php echo e(URL::to('/dashboard')); ?>"><?php echo e(trans('app.home')); ?></a></li>
          <li><a href="<?php echo e(URL::to('/dashboard/product/categories')); ?>"><?php echo e(trans('Categories')); ?></a></li>
          <li class="active"><?php echo e(trans('Trash')); ?></li>
        </ol>
      </div>
  </div>
	 <div class="panel">
        <div class="panel-body">
          <!-- Categories table -->
          <div class="example-wrap">
            <h2 class="example-title">Productos reciclados</h2>
            <p> 
              <a href="<?php echo e(url("dashboard/product/create")); ?>" title="">
                <button class="fa fa-plus btn btn-outline btn-primary btn-xs btn1">Nuevo</button>
              </a>
            </p>
            <small><i class="fa fa-info"></i> Nota: El sigiente listado de productos, pertenece al grupo de productos inactivos.</small>
            <div class="example">

             <div class="row">
              <div class="col-md-7 ml-auto">
               <a href="<?php echo e(route("products.index")); ?>">
                  <button class="btn btn-outline btn-warning btn-xs btn1"><i class="fa fa-arrow-left"></i> Volver</button>
               </a>
              </div>

               <div class="col-md-5 pull-right">
               
              </div>
             </div>  

              <div class="table-responsive">
                <?php if(count($products_trash)): ?>
                  <table class="table table-hover table-condensed table-bordered table-striped">
                  <thead>
                    <tr>
                      <td class="text-center"><input type="checkbox" name="" value=""> Select</td>
                      <th class="text-center">#</th>
                      <th class="text-center">Categoría</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Eliminado el:</th>
                     
                      <th class="text-center"><span class="fa fa-wrench"></span></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $__currentLoopData = $products_trash; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_t): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td class="text-center"><input type="checkbox" name="" value=""></td>
                      <td class="text-center"><?php echo e($product_t->id); ?></td>
                      <td class="text-center"><?php echo e($product_t->name); ?></td>
                      <td class="text-center"><?php echo e($product_t->status); ?></td>
                      <td class="text-center"><?php echo e($product_t->created_at); ?></td>
                      
                      <td>
                        <a href="<?php echo e(url("product/product-restore", $product_t->id)); ?>">
                        <button class="btn btn-outline btn-success btn-xs"> Restaurar</button></a>
                          <button class="btn btn-outline btn-danger btn-xs" data-target="#modal-delete-<?php echo e($product_t->id); ?>" data-toggle="modal" type="button">
                              <span class="fa fa-trash"></span>
                            </button>
                      </td>
                    </tr>
                    <?php echo $__env->make("dashboard.products.partials.modal_delete_trash", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                </table>
                <?php else: ?>
                  No hay productos en la papelera
                <?php endif; ?>
                <?php echo e($products_trash->render()); ?>

              </div>
            </div>
          </div>
          <!-- End table categories -->
          
        </div>
      </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>