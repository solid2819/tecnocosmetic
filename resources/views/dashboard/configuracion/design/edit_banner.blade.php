@extends('layouts.template')
@section("title")
	| Editar Banner
@endsection
@section("styles")
  <link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/webui-popover/webui-popover.css">
  <link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/toolbar/toolbar.css">
  <style type="text/css" media="screen">
    a{
      text-decoration: none!important;
    }  
    .td1{
      width: 4vw!important;
  </style>

@endsection

@section('content')
@include("layouts.messages"){{--message--}}
  <div class="page-header">
    <h1 class="page-title font_lato">
     Editando Banner

    </h1>
    <div class="page-header-actions">
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
        <li><a href="{{URL::to('dashboard/config/design/banners')}}">Banners</a></li>
        <li class="active">Create</li>
      </ol>
    </div>
  </div>
	 <div class="panel">
        <div class="panel-body">
          <!-- Categories table -->
          <div class="example-wrap">
            <h2 class="example-title"></h2>
            <div class="example">
              {{-- Body de la seccion --}}

              {!! Form::model($banner,["route" => ["banners.update", $banner->id], "id" => "form-banners", "enctype" => "multipart/form-data", "method" => "PUT"]) !!}
                {{ csrf_field() }}
                 @include("dashboard.configuracion.design.partials.form_edit_banner")
              {{ Form::close() }}  
            </div>
              
          </div>
          <!-- End table categories -->
          
        </div>
      </div>
@endsection