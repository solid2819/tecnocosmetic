$(document).ready(function() {
	//Difrentes precios y stock en el Front del single product
	$("#selSize").change(function() {
		var idSize = $(this).val();
		if (idSize == "") {
			return false;
		}
		$.ajax({
			type:'get',
			url:'/get-product-price',
			data:{idSize:idSize},
			success:function(resp){
				// alert(resp); return false;
				var arr  = resp.split("#");
				$("#getPrice").html("$ " + arr[0]);
				$("#price").val(arr[0]);
				if (arr[1]==0) {
					$("#cartButton").hide();
					$("#availability").text("No disponible");
				}else{
					$("#cartButton").show();
					$("#availability").text("Disponible");
				}
			},error:function(){
				alert("Error 800");
			}
		});
	});


});