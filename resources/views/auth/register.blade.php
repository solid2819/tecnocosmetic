@extends("layouts.frontend.theme")
@section("title")
	Registro
@endsection
@section("styles")
	<link rel="stylesheet" href="front/assets/css/passtrength.css">
@endsection
@section("content")
	<div class="breadcrumb">
   		<!-- container -->
		<div class="container">
			<h1>Registro de usuario</h1>
			<ol class="item-breadcrumb">
	            <li><a href="index.html">Home</a></li>
	            <li>account</li>     
            </ol>
		</div>
		<!-- /container -->
   	</div>
	<div class="page-account">
		<!-- container -->
		<div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="account-wrapper">
                    	<ul class="account-tab-list nav">
                        	<li class="active"><a data-toggle="tab" href="#">registrarse</a></li>
                        </ul>	
                         @foreach($settingdata as $view)	
                                <div class="account-form-container register-form">
                                    <div class="account-form">
                                       <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
							                   {{ csrf_field() }}				   
											<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">                         
														<input id="username" required placeholder="Username" type="text" class="form-control" name="username" value="{{ old('username') }}">
														@if ($errors->has('username'))
															<span class="help-block">
																<strong>{{ $errors->first('username') }}</strong>
															</span>
														@endif				  
												</div>
												<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">				   
														<input id="email" required type="email"  placeholder="Email" class="form-control" name="email" value="{{ old('email') }}">
														@if ($errors->has('email'))
															<span class="help-block">
																<strong>{{ $errors->first('email') }}</strong>
															</span>
														@endif
												</div>
												<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">				   
														<input required id="password_id" placeholder="Contraseña" type="password" class="form-control" name="password">
														@if ($errors->has('password'))
															<span class="help-block">
																<strong>{{ $errors->first('password') }}</strong>
															</span>
														@endif
												</div>

												<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">				   
														<input required id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmar contraseña">
														@if ($errors->has('password_confirmation'))
															<span class="help-block">
																<strong>{{ $errors->first('password_confirmation') }}</strong>
															</span>
														@endif
												</div>
								          <!--<button type="submit" class="btn btn-primary btn-block" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Loading..">{{ trans('app.sign_up')}}</button>-->
								        <button type="submit" class="btn btn-primary ladda-button btn-block" data-plugin="ladda" data-style="expand-left">
											  Registrarse
										<span class="ladda-spinner"></span><div class="ladda-progress" style="width: 0px;"></div>
										</button>
									</form>
                                    </div>
                                </div>
                            @endforeach
                    </div>
                </div>
            </div>
		</div>
		<!-- /container -->
	</div>
@endsection

@section("scripts")
	<script src="front/assets/js/jquery.passtrength.js">
		
	</script>

	<script>
		$("#password_id").passtrength({
			 minChars: 4,
			 passwordToggle:true,
			 tooltip:true,
			 eyeImg:"front/assets/images/arrow.png"
		});
	</script>

 @endsection
