@extends('layouts.template')
@section("title")
	| Imagenes del producto
@endsection

@section('content')
    @include("layouts.messages"){{--message--}}
      <div class="page-header">
          <h1 class="page-title font_lato">
            Agregar imagenes al producto
          </h1>
          <div class="page-header-actions">
            <ol class="breadcrumb">
              <li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
              <li><a href="{{URL::to('/dashboard/products')}}">{{ trans('Products')}}</a></li>
              <li><a href="{{ route("products.edit", $product_add_images->id) }}">{{$product_add_images->id}}</a></li>
              <li class="active">Product Images</li>
            </ol>
          </div>
        </div> 
	     <div class="col-sm-12 col-md-12 ol-lg-12 ">
          <!-- Panel Floating Lables -->
          @if(count($errors)>0)
            @include("layouts.messages"){{--message--}}
          @endif
         <div class="panel">
            <div class="panel-heading">
              <h2> {!! $product_add_images->name !!}</h2>
            </div>

            <div class="panel-body">   
             <div class="row">
               <div class="col-md-8">
                
                 <p>
                   {!! $product_add_images->description !!}
                 </p>
               </div>

               <div class="col-md-4">
                 
               </div>

             </div>

             <div class="row"> 
              <form class="" method="POST" action="{{ url("dashboard/products/add-images/".$product_add_images->id) }}" enctype="multipart/form-data" name="add_image">
                {{ csrf_field() }}

                
                <input type="file" name="image[]" value="" multiple="multiple">
                <br>
                <button class="btn btn-outline btn-success "> Añadir imagenes</button>
                 
               </form>
             </div>
            </div>

          </div>
          <!-- End Panel Floating Lables -->
        </div>
@endsection

