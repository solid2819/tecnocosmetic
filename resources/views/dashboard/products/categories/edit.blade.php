@extends('layouts.template')
@section("title")
	| Edición de categoría
@endsection
@section('content')
    @include("layouts.messages"){{--message--}}
      <div class="page-header">
          <h1 class="page-title font_lato">
           Estas editando la categoría : {{ $category->name }}
          </h1>
          <div class="page-header-actions">
            <ol class="breadcrumb">
              <li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
              <li><a href="{{URL::to('/dashboard/product/categories')}}">{{ trans('Categories')}}</a></li>
              <li class="active">Edit</li>
            </ol>
          </div>
        </div> 
	     <div class="col-sm-12 col-md-6 col-lg-6 col-md-offset-2">
          <!-- Panel Floating Lables -->
          @if(count($errors)>0)
            @include("layouts.messages"){{--message--}}
          @endif
         <div class="panel">
            <div class="panel-heading">
              <h2 class="panel-title">Editar categoría</h2>
            </div>
            <div class="panel-body">
              
            {!! Form::model($category,["method" => "PATCH", "route" => ["categories.update", $category->id]]) !!}
                    {!! Form::token() !!}
             @include("dashboard.products.categories.partials.form-edit")
            {!! Form::close() !!}

            </div>
          </div>
          <!-- End Panel Floating Lables -->
        </div>
@endsection