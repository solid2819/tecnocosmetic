                <div class="modal fade modal-3d-flip-vertical" id="modal-delete-{{ $product->id }}" aria-hidden="true"
                  aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
                  {{--  {{{ Form::Open(array("action" => array("Dashboard\ProductsController@destroy", $product->id), "method" => "DELETE")) }}} --}}
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header bg-danger">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <h4 class="modal-title" style="color: white!important;">Enviar a la papelera</h4>
                        </div>
                        <div class="modal-body">
                          <p>¿Estas seguro de enviar el producto {{ $product->name }} a la papelera? <br>
                            <small class="text-success">Tienes la opción de restaurarlo más tarde</small>
                          </p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-outline btn-default margin-0" data-dismiss="modal">Cancelar</button>
                         <a href="{{ url("product/product-trash", $product->id) }}">
                            <button type="submit" class="btn btn-outline btn-danger">Continuar</button>
                         </a>
                        </div>
                      </div>
                    </div>
                    {{--  {{ Form::close() }} --}}
                  </div>

