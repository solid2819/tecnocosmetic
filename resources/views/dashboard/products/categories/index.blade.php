@extends('layouts.template')
@section("title")
	| Categorías de productos
@endsection
@section("styles")
  <link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/webui-popover/webui-popover.css">
  <link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/toolbar/toolbar.css">
  <style type="text/css" media="screen">
    a{
      text-decoration: none!important;
    }  
    .td1{
      width: 4vw!important;
  </style>

@endsection

@section('content')
   @include("layouts.messages"){{--message--}}
   <div class="page-header">
      <h1 class="page-title font_lato">
        Categorías
      </h1>
      <div class="page-header-actions">
          <ol class="breadcrumb">
          <li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
          <li class="active">{{ trans('Categories')}}</li>
        </ol>
      </div>
  </div>
	 <div class="panel">
        <div class="panel-body">
          <!-- Categories table -->
          <div class="example-wrap">
            <h2 class="example-title">Categorías para productos</h2>

            <p> 
              <a href="{{ url("dashboard/product/categories/create") }}" title="">
                <button class="fa fa-plus btn btn-outline btn-primary btn-xs btn1"> Nueva</button>
              </a>
            </p>
            <div class="example">

             <div class="row">{{--Row Search--}}
              <div class="col-md-5">
                <a href="{{ route("categories_products_trash")}}" class="text-danger">
                  <button class="btn btn-outline btn-danger btn-xs btn1" data-content="Papelera"data-trigger="hover" data-toggle="popover" tabindex="0">
                    <i class="fa fa-trash-o"> </i>
                  </button>
                </a>
              </div>
               <div class="col-md-5 pull-right">
                @if(count($categories))
                @include("dashboard.products.categories.partials.search")
              </div>
             </div>{{--End row Search--}}  

              <div class="table-responsive">
                  <table class="table table-hover table-condensed table-bordered table-striped">
                  <thead>
                    <tr>
                      <td class="text-center td1"><input type="checkbox" name="" value=""> All</td>
                      <th class="text-center">Categoría</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Fecha de creación</th>
                     {{--  <th>Última actualización</th> --}}
                      <th class="text-center"><span class="fa fa-wrench"></span></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($categories as $category)
                    <tr>
                      <td class="text-center td1">
                        <input type="checkbox" name="" value=""> &nbsp {{ $category->id }}
                      </td>
                      <td class="text-center text-capitalize">
                        <a href="{{ route("categories.edit", $category->id) }}">
                          {{ $category->name }} &nbsp <i class="fa fa-pencil"></i>
                        </a><br>
                        <small>Subcategory: {{ $category->parent_id }}</small>
                      </td>
                      <td class="text-center">{{ $category->status }}</td>
                      <td class="text-center">{{ $category->created_at }}</td>
                      {{-- <td>{{ $category->updated_at }}</td> --}}
                     <td class="text-center">
                        <div class="toolbar-icons hidden" id="set-05-options">
                            <a href="javascript:void(0)" rel="{{ $category->id }}"  rel1="delete-category" type="button" data-target="#modal-view-{{ $category->id }}" data-toggle="modal">
                              <i class="fa fa-eye" aria-hidden="true"></i>
                            </a>
                            <a href="javascript:void(0)" data-target="#modal-delete-{{ $category->id }}" data-toggle="modal" type="button">
                              <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                        </div>
                        <button class="btn btn-primary btn-icon btn-outline btn-round" data-plugin="toolbar" data-toolbar="#set-05-options" data-toolbar-animation="grow" data-toolbar-style="primary" type="button"><i class="icon wb-settings" aria-hidden="true"></i>
                        </button>
                      </td>
                    </tr>
                    @include("dashboard.products.categories.partials.modal_delete")
                    @endforeach
                  </tbody>
                </table>
                @else
                  <p>No hay categorías agregadas.</p>
                @endif
                {{ $categories->render() }}
              </div>
            </div>
          </div>
          <!-- End table categories -->
          
        </div>
      </div>
@endsection