<?php $__env->startSection("title"); ?>
	| Edición de categoría
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
        
	     <div class="col-sm-12 col-md-8 ol-lg-8 col-md-offset-2" style="margin-top: 20px;">
          <!-- Panel Floating Lables -->
         <div class="panel">
            <div class="panel-heading">
              <h3 class="panel-title">Categoría : <?php echo e($category->name); ?></h3>
            </div>
            <div class="panel-body">
              
            <?php echo Form::model($category,["method" => "PATCH", "route" => ["categories.update", $category->id]]); ?>

                    
             <?php echo $__env->make("dashboard.products.categories.partials.form-edit", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo Form::close(); ?>


            </div>
          </div>
          <!-- End Panel Floating Lables -->
        </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>