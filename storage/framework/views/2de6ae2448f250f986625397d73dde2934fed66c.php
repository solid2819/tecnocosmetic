<?php $__env->startSection("title"); ?>
	| Atributos del producto
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
     <?php echo $__env->make("layouts.messages", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
     <?php if(count($errors)>0): ?>
        <?php echo $__env->make("layouts.messages", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php endif; ?>
      <div class="page-header">
          <h1 class="page-title font_lato">
            Atributos del producto
          </h1>
          <div class="page-header-actions">
            <ol class="breadcrumb">
              <li><a href="<?php echo e(URL::to('/dashboard')); ?>"><?php echo e(trans('app.home')); ?></a></li>
              <li><a href="<?php echo e(URL::to('/dashboard/products')); ?>"><?php echo e(trans('Products')); ?></a></li>
              <li><a href="<?php echo e(route("products.edit", $productsDetails->id)); ?>"><?php echo e($productsDetails->id); ?></a></li>
              <li class="active">Attributes</li>
            </ol>
          </div>
        </div> 
	     <div class="col-sm-12 col-md-12 ol-lg-12 ">
          <!-- Panel Floating Lables -->
         <div class="panel">
            <div class="panel-heading">
              <h2 class="panel-title">Añadir atributos</h2>
            </div>

            <div class="panel-body">   
              <form enctype="multipart/form-data"  method="post" action="<?php echo e(url('dashboard/products/attributes/' . $productsDetails->id)); ?>" name="add_attribute" id="add_attribute exampleStandardForm" novalidate="novalidate" class="row">
                <?php echo e(csrf_field()); ?>

               <?php echo $__env->make("dashboard.products.partials.form_attribute_product", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
              </form>
            </div>

            <div class="table-responsive">
              
              <form action="<?php echo e(url("dashboard/products/attributes/edit", $productsDetails->id)); ?>" method="post">
                  <?php echo e(csrf_field()); ?>

                  <table class="table table-hover table-condensed table-bordered table-striped">
                    <thead>
                      <tr>
                        <td class="text-center td1"><input type="checkbox" name="" value="">
                         All
                        </td>
                        <th class="text-center">SKU</th>
                        <th class="text-center">Size</th>
                        <th class="text-center">Precio</th>
                        <th class="text-center">Stock</th>
                        <th class="text-center"><span class="fa fa-wrench"></span></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $__currentLoopData = $productsDetails['attributes']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $attribute): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <tr>
                        <td class="text-center td1"><input type="checkbox" name="" value=""> &nbsp 
                          <input type="hidden" name="idAttr[]" value="<?php echo e($attribute->id); ?>"> <?php echo e($attribute->id); ?>

                        </td>
                        <td><?php echo e($attribute->sku); ?></td>
                        <td class="text-center">
                              <?php echo e($attribute->size); ?>

                               &nbsp 
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                              </a>
                            <br>
                        </td>
                        <td class="text-center text-capitalize">
                          <input type="text" name="price[]" value="<?php echo e($attribute->price); ?>">
                        </td>
                        <td>
                         <input type="text" name="stock[]" value="<?php echo e($attribute->stock); ?>">
                        </td>
                        <td class="text-center">
                           <div class="toolbar-icons hidden" id="set-05-options">
                              <a href="javascript:void(0)" type="button" data-target="#modal-view-<?php echo e($attribute->id); ?>" data-toggle="modal">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                              </a>
                              <a href="javascript:void(0)" >
                                <i class="fa fa-trash" aria-hidden="true"></i>
                              </a>
                          </div>
                            <button type="submit" class="btn btn-icon btn-success btn-outline btn-round">
                              <i class="fa fa-edit"></i>
                            </button>
                             <a href="javascript:void(0)" class="btn btn-danger btn-icon btn-outline btn-round"   
                                 data-target="#modal-delete-<?php echo e($attribute->id); ?>" 
                                 data-toggle="modal" type="button"
                                 data-toolbar-style="danger" >
                                 <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                         
                        </td>
                        
                      </tr>
                       <?php echo $__env->make("dashboard.products.partials.modal_delete_attributes", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
              <form>
            </div>
          </div>
          <!-- End Panel Floating Lables -->
        </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>