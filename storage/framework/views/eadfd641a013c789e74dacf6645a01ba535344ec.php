                <div class="modal fade modal-3d-flip-vertical" id="modal-delete-<?php echo e($product_t->id); ?>" aria-hidden="true"
                  aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
                  
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header bg-danger">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <h4 class="modal-title" style="color: white!important;">Eliminar producto</h4>
                        </div>
                        <div class="modal-body">
                          <p>¿Estás seguro de eliminar el producto <?php echo e($product_t->name); ?> permanentemente?</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-outline btn-default margin-0" data-dismiss="modal">No</button>
                          <a href="<?php echo e(url("dashboar/product/product-deleted", $product_t->id)); ?>">
                          <button type="submit" class="btn btn-outline btn-danger">Si</button></a>
                        </div>
                      </div>
                    </div>
                    
                  </div>

