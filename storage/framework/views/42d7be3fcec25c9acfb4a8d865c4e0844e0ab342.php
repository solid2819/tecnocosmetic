<div class="form-group">
  <?php echo e(Form::label("name", "Nombre de la categoría")); ?>

  <?php echo e(Form::text("name",null, ["class" => "form-control", "id" => "name"])); ?> 
</div>

<div class="form-group">
  <label for="">Subcategoría</label>
  <select class="form-control" name="parent_id">
    <?php $__currentLoopData = $level; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <option value="<?php echo e($val->id); ?>" <?php if($val->id == $category->parent_id ): ?>
        selected <?php endif; ?>> <?php echo e($val->name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </select>
</div>

<div class="form-group">
  <?php echo e(Form::label("slug", "Slug")); ?>

  <?php echo e(Form::text("slug",null, ["class" => "form-control", "id" => "slug"])); ?> 
</div>

<div class="form-group">
 

  <div class="form-group" style="margin-top:20px">
  <?php echo e(Form::label("status", "Estado")); ?>

  <label style="display: inline-block;"><?php echo e(Form::radio("status", "Activa")); ?> Activar</label>

  <label style="display: inline-block;"><?php echo e(Form::radio("status", "No Activa")); ?> Desactivar</label>
</div>
</div>

<div class="form-group">
  <?php echo e(Form::label("description", "Descripción")); ?>

  <?php echo e(Form::textarea("description",null, ["class" => "form-control", "id" => "description"])); ?> 
</div>

<div class="form-group">
  <?php echo e(Form::submit("Actualizar", ["class" => "btn btn-success btn-sm"])); ?>

  <a href="<?php echo e(route("categories.index")); ?>">
    <button type="button" class="btn btn-warning btn-sm">
      Volver
    </button>
  </a>
</div>


<?php $__env->startSection("scripts"); ?>
  <script src="<?php echo e(asset("vendor/jquerystringToSlug/jquery-stringToSlug.min.js")); ?> "></script>
  
  <script>
    $(document).ready(function () {
      $("#name, #slug").stringToSlug({
        callback: function (text) {
          $("#slug").val(text);
        }
      });
    });
  </script>
<?php $__env->stopSection(); ?>