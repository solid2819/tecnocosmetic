<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Ecommerce, Tienda de relojes">
  	<meta name="author" content="Henry Ruiz">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
  	<title>E-commerce | @yield("title")</title>

	<!-- preloader CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset("front/assets/css/preloader.css") }}"> 

	<!-- prettyPhoto CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset("front/assets/css/prettyPhoto.css") }}">

	<!-- slider-pro CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset("front/assets/css/slider-pro.css") }}">

	<!-- slick CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset("front/assets/css/slick.css") }}">

    <!-- font-awesome CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset("front/assets/css/font-awesome.css") }}">

	<!-- Main Style CSS CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset("front/assets/css/style.css") }}">

	 <!-- Fonts --> 
	<link href='https://fonts.googleapis.com/css?family=Yantramanav:300,400,500,700,900' rel='stylesheet' type='text/css'>
	@yield("styles")
</head>


	<body>
		<!-- Header -->
		@include("layouts.frontend.header")
		<!-- header-mobile -->
		<!-- /Header -->

	 	@yield("content")
		
	   <!-- =====================================================================
	    	==== Start footer -->
		
	    @include("layouts.frontend.footer")
		<!-- =====================================
	    	==== End all js here -->

	</body>
</html>