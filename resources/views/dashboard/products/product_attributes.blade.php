@extends('layouts.template')
@section("title")
	| Atributos del producto
@endsection

@section('content')
     @include("layouts.messages"){{--message--}}
     @if(count($errors)>0)
        @include("layouts.messages"){{--message--}}
      @endif
      <div class="page-header">
          <h1 class="page-title font_lato">
            Atributos del producto
          </h1>
          <div class="page-header-actions">
            <ol class="breadcrumb">
              <li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
              <li><a href="{{URL::to('/dashboard/products')}}">{{ trans('Products')}}</a></li>
              <li><a href="{{ route("products.edit", $productsDetails->id) }}">{{$productsDetails->id}}</a></li>
              <li class="active">Attributes</li>
            </ol>
          </div>
        </div> 
	     <div class="col-sm-12 col-md-12 ol-lg-12 ">
          <!-- Panel Floating Lables -->
         <div class="panel">
            <div class="panel-heading">
              <h2 class="panel-title">Añadir atributos</h2>
            </div>

            <div class="panel-body">   
              <form enctype="multipart/form-data"  method="post" action="{{ url('dashboard/products/attributes/' . $productsDetails->id) }}" name="add_attribute" id="add_attribute exampleStandardForm" novalidate="novalidate" class="row">
                {{ csrf_field() }}
               @include("dashboard.products.partials.form_attribute_product")
              </form>
            </div>

            <div class="table-responsive">
              {{-- Formulario de edición de atributos del producto --}}
              <form action="{{ url("dashboard/products/attributes/edit", $productsDetails->id) }}" method="post">
                  {{ csrf_field() }}
                  <table class="table table-hover table-condensed table-bordered table-striped">
                    <thead>
                      <tr>
                        <td class="text-center td1"><input type="checkbox" name="" value="">
                         All
                        </td>
                        <th class="text-center">SKU</th>
                        <th class="text-center">Size</th>
                        <th class="text-center">Precio</th>
                        <th class="text-center">Stock</th>
                        <th class="text-center"><span class="fa fa-wrench"></span></th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($productsDetails['attributes'] as $attribute)
                      <tr>
                        <td class="text-center td1"><input type="checkbox" name="" value=""> &nbsp 
                          <input type="hidden" name="idAttr[]" value="{{ $attribute->id }}"> {{ $attribute->id }}
                        </td>
                        <td>{{ $attribute->sku }}</td>
                        <td class="text-center">
                              {{-- <a class="text-capitalize" href="{{ route("products.edit", $product->id ) }}"> --}}{{ $attribute->size }}
                               &nbsp 
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                              </a>
                            <br>
                        </td>
                        <td class="text-center text-capitalize">
                          <input type="text" name="price[]" value="{{ $attribute->price }}">
                        </td>
                        <td>
                         <input type="text" name="stock[]" value="{{ $attribute->stock }}">
                        </td>
                        <td class="text-center">
                           <div class="toolbar-icons hidden" id="set-05-options">
                              <a href="javascript:void(0)" type="button" data-target="#modal-view-{{ $attribute->id }}" data-toggle="modal">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                              </a>
                              <a href="javascript:void(0)" >
                                <i class="fa fa-trash" aria-hidden="true"></i>
                              </a>
                          </div>
                            <button type="submit" class="btn btn-icon btn-success btn-outline btn-round">
                              <i class="fa fa-edit"></i>
                            </button>
                             <a href="javascript:void(0)" class="btn btn-danger btn-icon btn-outline btn-round"   
                                 data-target="#modal-delete-{{ $attribute->id }}" 
                                 data-toggle="modal" type="button"
                                 data-toolbar-style="danger" >
                                 <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                         {{--  <button class="btn btn-primary btn-icon btn-outline btn-round" 
                                  data-plugin="toolbar" 
                                  data-toolbar="#set-05-options" 
                                  data-toolbar-animation="grow" 
                                  data-toolbar-style="primary" 
                                  type="button">
                                  <i class="icon wb-settings" aria-hidden="true"></i>
                          </button> --}}
                        </td>
                        
                      </tr>
                       @include("dashboard.products.partials.modal_delete_attributes")
                      @endforeach
                    </tbody>
                </table>
              <form>
            </div>
          </div>
          <!-- End Panel Floating Lables -->
        </div>
@endsection

