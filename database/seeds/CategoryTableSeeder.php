<?php

use Illuminate\Database\Seeder;
use App\Category;
use Carbon\Carbon;
class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
        	'id' =>   1,
        	'name' => 'Hair liss',
        	'slug' => 'hair-liss',
        	'description' => 'Keratina para todo tipo de Cabellos',
        	'status' => 1,
        	'remember_token' => str_random(15),
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now(),
        ]);

        Category::create([
        	'id' =>   2,
        	'name' => 'Keratin Botox Xtreme',
        	'slug' => 'keratin-botox-xtreme',
        	'description' => 'Keratina para cabellos rebeldes',
        	'status' => 1,
        	'remember_token' => str_random(15),
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now(),
        ]);

         Category::create([
        	'id' =>   3,
        	'name' => 'Keratin Botox Seduction',
        	'slug' => 'keratin-botox-seduction',
        	'description' => 'Keratina para cabellos grasos',
        	'status' => 0,
        	'remember_token' => str_random(15),
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now(),
        ]);
    }
}
