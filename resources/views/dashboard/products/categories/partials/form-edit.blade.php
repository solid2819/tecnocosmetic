<div class="form-group">
  {{ Form::label("name", "Nombre de la categoría") }}
  {{ Form::text("name",null, ["class" => "form-control", "id" => "name"])}} 
</div>

<div class="form-group">
  <label for="">Subcategoría</label>
  <select class="form-control" name="parent_id">
    @foreach($level as $val)
      <option value="{{ $val->id }}" @if($val->id == $category->parent_id )
        selected @endif> {{ $val->name }}</option>
    @endforeach
  </select>
</div>

<div class="form-group">
  {{ Form::label("slug", "Slug") }}
  {{ Form::text("slug",null, ["class" => "form-control", "id" => "slug"])}} 
</div>

<div class="form-group">
 {{--  {{ Form::label("status", "Estado") }}
 <input type="checkbox" name="status" id="status" @if($category->status=="1") checked @endif value="1"> <br>
  <small>Marca o desmarca el checkbox para activar o desactivar la categoría.</small> --}}

  <div class="form-group" style="margin-top:20px">
  {{ Form::label("status", "Estado") }}
  <label style="display: inline-block;">{{ Form::radio("status", "Activa") }} Activar</label>

  <label style="display: inline-block;">{{ Form::radio("status", "No Activa") }} Desactivar</label>
</div>
</div>

<div class="form-group">
  {{ Form::label("description", "Descripción") }}
  {{ Form::textarea("description",null, ["class" => "form-control", "id" => "description"])}} 
</div>

<div class="form-group">
  {{ Form::submit("Actualizar", ["class" => "btn btn-success btn-sm"]) }}
  <a href="{{ route("categories.index") }}">
    <button type="button" class="btn btn-warning btn-sm">
      Volver
    </button>
  </a>
</div>


@section("scripts")
  <script src="{{ asset("vendor/jquerystringToSlug/jquery-stringToSlug.min.js") }} "></script>
  
  <script>
    $(document).ready(function () {
      $("#name, #slug").stringToSlug({
        callback: function (text) {
          $("#slug").val(text);
        }
      });
    });
  </script>
@endsection