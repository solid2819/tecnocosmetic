<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
       public function rules()
        {
            return [
                "category_id" => "required",
                "name" => "required|min:10|max:100",
                "slug" => "required|min:5|unique:products,slug,". $this->product,
                "price" => "numeric|min:1|max:999999999|",
                "code" => "required|min:4|max:10|unique:products,code,". $this->product
            ];
        }

        public function messages()
        {
            return [

                'name.required' => '¡El titulo del producto es un campo obligatorio!',
                'name.min'      => '¡El titulo del producto debe tener al menos 10 caracteres!',
                'name.max'      => '¡El titulo del producto debe tener 100 caracteres como máximo!',

                'category_id.required' => '¡La categoría del producto es un campo obligatorio!',

                'slug.required' => "¡La url del producto es un campo obligario!",
                'slug.min'      => "¡La url del producto debe contener al menos 5 caracteres!",
                'slug.max'      => "¡La url del producto debe contener máximo 200 caracteres!",
                'slug.unique'   => "¡La url del producto ya existe!",

                'price.required' => "¡El precio es requerido!",
                'price.numeric'  => "¡El campo precio debe ser un valor númerico!",
                'price.min'  => "¡El campo precio debe poseer al menos 1 caracter!",
                'price.max'  => "¡Error en el precio!",

                'code.required' => "¡El código del producto es requerido!",
                'code.unique' => "¡El código del producto ya existe!",
                'code.min'  => "¡El campo código del producto debe poseer al menos 4 caracteres!",
                'code.max'  => "¡El campo código del producto no debe superar 10 caracteres!"
            ];
        }
}
