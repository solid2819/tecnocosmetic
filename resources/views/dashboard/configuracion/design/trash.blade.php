@extends('layouts.template')
@section("title")
	| Banners reciclados
@endsection
<style type="text/css" media="screen">
  a{
    text-decoration: none!important;
  }  
  .btn1{
      width: 3.7vw;
    }
</style>

@section('content')
   @include("layouts.messages"){{--message--}}
   <div class="page-header">
      <h1 class="page-title font_lato">
        Papelera
      </h1>
      <div class="page-header-actions">
          <ol class="breadcrumb">
          <li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
          <li><a href="{{URL::to('/dashboard/product/categories')}}">{{ trans('Banners')}}</a></li>
          <li class="active">{{ trans('Trash')}}</li>
        </ol>
      </div>
  </div>
	 <div class="panel">
        <div class="panel-body">
          <!-- Categories table -->
          <div class="example-wrap">
            <h2 class="example-title">Banners reciclados</h2>
            <p> 
              <a href="{{ url("dashboard/config/design/banners/create") }}" title="">
                <button class="fa fa-plus btn btn-outline btn-primary btn-xs btn1">Nuevo</button>
              </a>
            </p>
            <small><i class="fa fa-info"></i> Nota: El sigiente listado de banners, pertenece al grupo de banners inactivos.</small>
            <div class="example">

             <div class="row">{{--Row Search--}}
              <div class="col-md-7 ml-auto">
               <a href="{{ route("banners.index") }}">
                  <button class="btn btn-outline btn-warning btn-xs btn1"><i class="fa fa-arrow-left"></i> Volver</button>
               </a>
              </div>

               <div class="col-md-5 pull-right">
               {{--  @include("dashboard.products.categories.partials.searchTrash") --}}
              </div>
             </div>{{--End row Search--}}  

              <div class="table-responsive">
                @if(count($banner_trash))
                  <table class="table table-hover table-condensed table-bordered table-striped">
                  <thead>
                    <tr>
                      <td class="text-center"><input type="checkbox" name="" value=""> Select</td>
                      <th class="text-center">#</th>
                      <th class="text-center">Slider Image</th>
                      <th class="text-center">Categoría</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Eliminado el:</th>
                     {{--  <th>Última actualización</th> --}}
                      <th class="text-center"><span class="fa fa-wrench"></span></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($banner_trash as $banner_t)
                    <tr>
                      <td class="text-center"><input type="checkbox" name="" value=""></td>
                      <td class="text-center">{{ $banner_t->id }}</td>
                      <td class="text-center">
                         <img src="{{ asset("images/frontend/banners/$banner_t->image") }}" width="100px" >
                      </td>
                      <td class="text-center">{{ $banner_t->title }}</td>
                      <td class="text-center">{{ $banner_t->status }}</td>
                      <td>{{ $banner_t->deleted_at }}</td>
                      <td>
                        <a href="{{ url("dashboard/config/d-sign/banners/restore", $banner_t->id) }}">
                        <button class="btn btn-outline btn-success btn-xs"> Restaurar</button></a>
                          <button class="btn btn-outline btn-danger btn-xs" data-target="#modal-delete-{{ $banner_t->id }}" data-toggle="modal" type="button">
                              <span class="fa fa-trash"></span>
                            </button>
                      </td>
                    </tr>
                    @include("dashboard.configuracion.design.partials.modal_delete")
                    @endforeach
                  </tbody>
                </table>
                @else
                  No hay productos en la papelera
                @endif
                {{ $banner_trash->render() }}
              </div>
            </div>
          </div>
          <!-- End table categories -->
          
        </div>
      </div>
@endsection