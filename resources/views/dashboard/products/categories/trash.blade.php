@extends('layouts.template')
@section("title")
  | Categorías recicladas
@endsection
<style type="text/css" media="screen">
  a{
    text-decoration: none!important;
  }  
  .btn1{
      width: 3.7vw;
    }
</style>

@section('content')
   @include("layouts.messages"){{--message--}}
   <div class="page-header">
      <h1 class="page-title font_lato">
        Papelera
      </h1>
      <div class="page-header-actions">
          <ol class="breadcrumb">
          <li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
          <li><a href="{{URL::to('/dashboard/product/categories')}}">{{ trans('Categories')}}</a></li>
          <li class="active">{{ trans('Trash')}}</li>
        </ol>
      </div>
  </div>
   <div class="panel">
        <div class="panel-body">
          <!-- Categories table -->
          <div class="example-wrap">
            <h2 class="example-title">Categorías recicladas</h2>
            <p> 
              <a href="{{ url("dashboard/product/categories/create") }}" title="">
                <button class="fa fa-plus btn btn-outline btn-primary btn-xs btn1">Nueva</button>
              </a>
            </p>
            <small><i class="fa fa-info"></i> Nota: El sigiente listado de categorías, pertenece al grupo de categorías inactivas.</small>
            <div class="example">

             <div class="row">{{--Row Search--}}
              <div class="col-md-7 ml-auto">
               <a href="{{ route("categories.index") }}">
                  <button class="btn btn-outline btn-warning btn-xs btn1"><i class="fa fa-arrow-left"></i> Volver</button>
               </a>
              </div>

               <div class="col-md-5 pull-right">
                @include("dashboard.products.categories.partials.searchTrash")
              </div>
             </div>{{--End row Search--}}  

              <div class="table-responsive">
                @if(count($categories))
                  
                  <table class="table table-hover table-condensed table-bordered table-striped">
                  <thead>
                    <tr>
                      <td class="text-center"><input type="checkbox" name="" value=""> Select</td>
                      <th class="text-center">#</th>
                      <th class="text-center">Categoría</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Eliminado el:</th>
                     {{--  <th>Última actualización</th> --}}
                      <th class="text-center"><span class="fa fa-wrench"></span></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($categories as $category)
                    <tr>
                      <td class="text-center"><input type="checkbox" name="" value=""></td>
                      <td class="text-center">{{ $category->id }}</td>
                      <td class="text-center">{{ $category->name }}</td>
                      <td class="text-center">{{ $category->status }}</td>
                      <td class="text-center">{{ $category->created_at }}</td>
                      {{-- <td>{{ $category->updated_at }}</td> --}}
                      <td>
                        {{-- <a  href="{{ url("dashboard/product/categories/restore", $category->id) }}" > --}}
                        <button class="btn btn-outline btn-success btn-xs"> Restaurar</button>
                        </a>
                      </td>
                         {{--  <button class="btn btn-outline btn-danger btn-xs" data-target="#modal-delete-{{ $category->id }}" data-toggle="modal" type="button">
                              <span class="fa fa-trash"></span>
                            </button> --}}
                      </td>
                    </tr>
                    @include("dashboard.products.categories.partials.modal_delete_trash")
                    @endforeach
                  </tbody>
                </table>
                @else
                  No hay categorías agregadas
                @endif
                {{ $categories->render() }}
              </div>
            </div>
          </div>
          <!-- End table categories -->
          
        </div>
      </div>
@endsection