<?php $__env->startSection("styles"); ?>
   <link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/global/vendor/blueimp-file-upload/jquery.fileupload.css">
  <link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/global/vendor/dropify/dropify.css">
<?php $__env->stopSection(); ?>

<div class="row">

  <div class="col-xs-12  col-sm-12 col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <?php echo e(Form::label("name", "Titulo del producto")); ?>

      <?php echo e(Form::text("name",null, ["class" => "form-control", "id" => "name"])); ?> 
    </div>

    <div class="form-group">
      <?php echo e(Form::label("slug", "Slug")); ?>

      <?php echo e(Form::text("slug",null, ["class" => "form-control", "id" => "slug"])); ?> 
    </div>
  </div>

   <div class="col-xs-12  col-sm-12 col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      <?php echo e(Form::label("categoria", "Categoría")); ?>

      <select name="category_id" class="form-control">
       <?php echo $categories_dropdown; ?>
      </select>
    </div>

    <div class="row">

      <div class="col-md-4">
        <div class="form-group">
          <?php echo e(Form::label("code", "Código")); ?>

          <input type="text" name="code" value="PN-" class="form-control" /> 
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <?php echo e(Form::label("price", "Precio")); ?>

          <?php echo e(Form::number("price",null, ["class" => "form-control", "id" => "price"])); ?> 
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <?php echo e(Form::label("color", "Color")); ?>

          <?php echo e(Form::color("color",null, ["class" => "form-control", "id" => "price"])); ?> 
        </div>
      </div>
    </div>
    
  </div>


</div>
<div class="row">
  <div class="col-md-9">
    <div class="form-group">
        <?php echo e(Form::label("material", "Material de diseño")); ?>

        <input type="text" name="care"  class="form-control" id="care" /> 
    </div>

   <div class="form-group">
      <?php echo e(Form::label("description", "Descripción")); ?>

      <?php echo e(Form::textarea("description",null, ["class" => "form-control", "id" => "description"])); ?> 
    </div>

    <div class="form-group">
      <?php echo e(Form::label("excerpt", "Descripción corta")); ?>

      <?php echo e(Form::textarea("excerpt",null, ["class" => "form-control", "id" => "excerpt"])); ?> 
    </div>

    <div class="form-group">
      <div class="checkbox-custom checkbox-primary">
        <input type="checkbox" id="status" name="status" value="" />
        <label for="inputChecked">Activo</label>
      </div>
    </div>

  </div>
  <div class="col-md-3">
    <div class="example-wrap">
      <?php echo e(Form::label("image", "Imagen destacada")); ?>

      <p><small>Elige o arrastra una imagen para el producto.</small></p>
      <div class="example">
        <input type="file" id="input-file-now-custom-3" data-plugin="dropify" data-height="500"
                  data-default-file="<?php echo e(URL::to('/')); ?>/global/photos/placeholder.png" name="image" />
      </div>
    </div> 

    <div class="row">
    <div>
      <?php echo e(Form::label("image", "Agrega imagenes opcionales")); ?>

    </div>

    <div class="col-md-4" >
      <div class="example" >
        <input type="file" id="input-file-now-custom-3" data-plugin="dropify" data-height="100"
                  data-default-file="<?php echo e(URL::to('/')); ?>/global/photos/placeholder.png" name="image2" />
      </div>
    </div>

     <div class="col-md-4">
      <div class="example">
        <input type="file" id="input-file-now-custom-3" data-plugin="dropify" data-height="100"
                  data-default-file="<?php echo e(URL::to('/')); ?>/global/photos/placeholder.png" name="image3" />
      </div>
    </div>

     <div class="col-md-4">
      <div class="example">
        <input type="file" id="input-file-now-custom-3" data-plugin="dropify" data-height="100"
                  data-default-file="<?php echo e(URL::to('/')); ?>/global/photos/placeholder.png" name="image4" />
      </div>
    </div>


  </div>

  </div>
</div>

<div class="row">
    <div class="form-group">
      <?php echo e(Form::submit("Crear", ["class" => "btn btn-success btn-sm"])); ?>


       <button type="reset" class="btn btn-danger  btn-sm">
        Borrar
      </button>

      <a href="<?php echo e(route("products.index")); ?>">
        <button type="button" class="btn btn-warning btn-sm">
          Volver
        </button>
      </a>
      
    </div>
</div>


<?php $__env->startSection("scripts"); ?>
  <script src="<?php echo e(asset("vendor/jquerystringToSlug/jquery-stringToSlug.min.js")); ?> "></script>
  <script src="<?php echo e(asset("vendor/ckeditor/ckeditor.js")); ?> "></script>
  
  
  <script>
    $(document).ready(function () {
      $("#name, #slug").stringToSlug({
        callback: function (text) {
          $("#slug").val(text);
        }
      });
    });

    /*CK EDITOR*/
    CKEDITOR.config.height=400;
    CKEDITOR.config.width="auto";

    CKEDITOR.replace("description");

    $(document).ready(function() {
      $("#form-products").validate();
    });
  </script>
<?php $__env->stopSection(); ?>