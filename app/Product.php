<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    protected $fillable = ["category_id", "name", "code", "color", "slug",  "excerpt", "care", "description", "price", "status", "image"];

    public function attributes()
    {
    	return $this->hasMany("App\ProductAttribute", "product_id");
    }

   
    // public function product_images()
    // {
    // 	return $this->hasMany("App\ProductImage", "product_id");
    // }


    //SoftDelete
    use SoftDeletes;


    protected $dates = ["delete_at"];

}
