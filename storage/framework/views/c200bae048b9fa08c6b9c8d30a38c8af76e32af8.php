<?php $__env->startSection("title"); ?>
	| Categorías de productos
<?php $__env->stopSection(); ?>
<?php $__env->startSection("styles"); ?>
  <link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/global/vendor/webui-popover/webui-popover.css">
  <link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/global/vendor/toolbar/toolbar.css">
  <style type="text/css" media="screen">
    a{
      text-decoration: none!important;
    }  
    .td1{
      width: 4vw!important;
  </style>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
   <?php echo $__env->make("layouts.messages", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
   <div class="page-header">
      <h1 class="page-title font_lato">
        Categorías
      </h1>
      <div class="page-header-actions">
          <ol class="breadcrumb">
          <li><a href="<?php echo e(URL::to('/dashboard')); ?>"><?php echo e(trans('app.home')); ?></a></li>
          <li class="active"><?php echo e(trans('Categories')); ?></li>
        </ol>
      </div>
  </div>
	 <div class="panel">
        <div class="panel-body">
          <!-- Categories table -->
          <div class="example-wrap">
            <h2 class="example-title">Categorías para productos</h2>

            <p> 
              <a href="<?php echo e(url("dashboard/product/categories/create")); ?>" title="">
                <button class="fa fa-plus btn btn-outline btn-primary btn-xs btn1"> Nueva</button>
              </a>
            </p>
            <div class="example">

             <div class="row">
              <div class="col-md-5">
                <a href="<?php echo e(route("categories_products_trash")); ?>" class="text-danger">
                  <button class="btn btn-outline btn-danger btn-xs btn1" data-content="Papelera"data-trigger="hover" data-toggle="popover" tabindex="0">
                    <i class="fa fa-trash-o"> </i>
                  </button>
                </a>
              </div>
               <div class="col-md-5 pull-right">
                <?php if(count($categories)): ?>
                <?php echo $__env->make("dashboard.products.categories.partials.search", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
              </div>
             </div>  

              <div class="table-responsive">
                  <table class="table table-hover table-condensed table-bordered table-striped">
                  <thead>
                    <tr>
                      <td class="text-center td1"><input type="checkbox" name="" value=""> All</td>
                      <th class="text-center">Categoría</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Fecha de creación</th>
                     
                      <th class="text-center"><span class="fa fa-wrench"></span></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td class="text-center td1">
                        <input type="checkbox" name="" value=""> &nbsp <?php echo e($category->id); ?>

                      </td>
                      <td class="text-center text-capitalize">
                        <a href="<?php echo e(route("categories.edit", $category->id)); ?>">
                          <?php echo e($category->name); ?> &nbsp <i class="fa fa-pencil"></i>
                        </a><br>
                        <small>Subcategory: <?php echo e($category->parent_id); ?></small>
                      </td>
                      <td class="text-center"><?php echo e($category->status); ?></td>
                      <td class="text-center"><?php echo e($category->created_at); ?></td>
                      
                     <td class="text-center">
                        <div class="toolbar-icons hidden" id="set-05-options">
                            <a href="javascript:void(0)" rel="<?php echo e($category->id); ?>"  rel1="delete-category" type="button" data-target="#modal-view-<?php echo e($category->id); ?>" data-toggle="modal">
                              <i class="fa fa-eye" aria-hidden="true"></i>
                            </a>
                            <a href="javascript:void(0)" data-target="#modal-delete-<?php echo e($category->id); ?>" data-toggle="modal" type="button">
                              <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                        </div>
                        <button class="btn btn-primary btn-icon btn-outline btn-round" data-plugin="toolbar" data-toolbar="#set-05-options" data-toolbar-animation="grow" data-toolbar-style="primary" type="button"><i class="icon wb-settings" aria-hidden="true"></i>
                        </button>
                      </td>
                    </tr>
                    <?php echo $__env->make("dashboard.products.categories.partials.modal_delete", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                </table>
                <?php else: ?>
                  <p>No hay categorías agregadas.</p>
                <?php endif; ?>
                <?php echo e($categories->render()); ?>

              </div>
            </div>
          </div>
          <!-- End table categories -->
          
        </div>
      </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>