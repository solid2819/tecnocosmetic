<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
	protected $table = "product_attribute";

	protected $primaryKey = "id";

    protected $filleable = ["product_id", "sku", "size", "price", "stock"

    ];

}
