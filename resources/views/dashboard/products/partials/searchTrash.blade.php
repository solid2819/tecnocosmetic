 <div class="form-group">
   <div class="input-search">
   	<form role="search" method="get" action="{{ route("products.index") }}">
	     <button type="submit" class="input-search-btn"><i class="icon wb-search" aria-hidden="true"></i></button>
	     <input type="text" class="form-control" name="searchTextTrash" placeholder="Buscar categorías..." value="{{$searchTextTrash}}">
	   </div>
	</form>
 </div>