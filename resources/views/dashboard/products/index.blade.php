@extends('layouts.template')
@section("title")
	| Productos
@endsection
@section("styles")
  <link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/webui-popover/webui-popover.css">
  <link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/toolbar/toolbar.css">
  <style type="text/css" media="screen">
    a{
      text-decoration: none!important;
    }  
    .td1{
      width: 4vw!important;
  </style>

@endsection

@section('content')
@include("layouts.messages"){{--message--}}
  <div class="page-header">
    <h1 class="page-title font_lato">
      Productos

    </h1>
    <div class="page-header-actions">
      <ol class="breadcrumb">
        <li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
        <li class="active">Products</li>
      </ol>
    </div>
  </div>
	 <div class="panel">
        <div class="panel-body">
          <!-- Categories table -->
          <div class="example-wrap">
            <h2 class="example-title">Lista de productos</h2>
            <p> 
              <a href="{{ url("dashboard/products/create") }}">
                <button class="fa fa-plus btn btn-outline btn-primary btn-xs "> Nuevo</button>
              </a>
            </p>
            <div class="example">

             <div class="row">{{--Row Search--}}
              <div class="col-md-5">
                <a href="{{ url("dashboard/product/trash") }}" class="text-danger">
                  <button class="btn btn-outline btn-danger btn-xs btn1" data-content="Papelera"data-trigger="hover" data-toggle="popover" tabindex="0">
                    <i class="fa fa-trash-o"> </i>
                  </button>
                </a>
              </div>
               <div class="col-md-5 pull-right">
                @if(count($products))
                @include("dashboard.products.partials.search")
              </div>
             </div>{{--End row Search--}}  

              <div class="table-responsive">
                  <table class="table table-hover table-condensed table-bordered table-striped">
                  <thead>
                    <tr>
                      <td class="text-center td1"><input type="checkbox" name="" value="">
                       All
                      </td>
                      <th class="text-center">&nbsp</th>
                      <th class="text-center">Código</th>
                      <th class="text-center">Producto</th>
                      <th class="text-center">Categoría</th>
                      {{-- <th class="text-center">Fecha de creación</th> --}}
                     {{--  <th>Última actualización</th> --}}
                      <th class="text-center"><span class="fa fa-wrench"></span></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($products as $product)
                    <tr>
                      <td class="text-center td1"><input type="checkbox" name="" value=""> &nbsp 
                        {{ $product->id }}
                      </td>
                      <td class="text-center">
                        @if($product->image)
                          <img src="{{ asset("images/dashboard/products/$product->image") }}" width="150px">
                        @else
                          <span class="fa fa-image"></span>
                        @endif
                        <p>
                            @if($product->status==1)
                              <small class="text-success">Producto publicado</small>
                            @else
                              <small class="text-danger">No publicado</small>
                            @endif
                          </p>
                      </td>
                      <td>{{ $product->code }}</td>
                      <td class="text-center">
                            <a class="text-capitalize" href="{{ route("products.edit", $product->id ) }}">{{ $product->name }}  
                             &nbsp 
                              <i class="fa fa-pencil" aria-hidden="true"></i>
                            </a>
                          <br>
                      </td>
                      <td class="text-center text-capitalize">{{ $product->category_name }}</td>
                     {{--  <td class="text-center">{{ $product->created_at }}</td> --}}
                      {{-- <td>{{ $product->updated_at }}</td> --}}
                      <td class="text-center">
                         <div class="toolbar-icons hidden" id="set-05-options">
                            <a href="javascript:void(0)" type="button" data-target="#modal-view-{{ $product->id }}" data-toggle="modal">
                              <i class="fa fa-eye" aria-hidden="true"></i>
                            </a>
                            <a href="javascript:void(0)" data-target="#modal-delete-{{ $product->id }}" data-toggle="modal" type="button">
                              <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                        </div>
                        <a href="{{ route("product-attributes", $product->id) }}">
                           <button class="btn btn-success btn-icon btn-outline btn-round"   
                               data-plugin="webuiPopover"
                               data-trigger="hover" 
                               data-placement="left" 
                               data-delay-show="0"
                               data-delay-hide="0" 
                               data-title="Añadir atributos" 
                               data-content="&lt;p&gt; Edita el producto en el apartado de Atributos del producto .&lt;/p&gt;"  
                               data-toolbar-style="primary" >
                               <i class="fa fa-edit" aria-hidden="true"></i>
                          </button>
                        </a>
                        <button class="btn btn-primary btn-icon btn-outline btn-round" 
                                data-plugin="toolbar" 
                                data-toolbar="#set-05-options" 
                                data-toolbar-animation="grow" 
                                data-toolbar-style="primary" 
                                type="button">
                                <i class="icon wb-settings" aria-hidden="true"></i>
                        </button>
                      </td>
                    </tr>
                    @include("dashboard.products.partials.modal_delete")
                    @include("dashboard.products.partials.modal_details")
                    @endforeach
                  </tbody>
                </table>
                @else
                  <p>Lista de productos vacía.</p>
                @endif
                {{ $products->render() }}
              </div>
            </div>
          </div>
          <!-- End table categories -->
          
        </div>
      </div>
@endsection