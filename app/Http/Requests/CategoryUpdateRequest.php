<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;

class CategoryUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|min:5|max:100|unique:categories,name,". $this->category,
            "slug" => "required|min:5|max:200|unique:categories,slug,". $this->category
        ];
    }
     public function messages()
    {
        return [
            'name.required'     => '¡El nombre de la categoría es un campo obligatorio!',
            'name.min'          => '¡El nombre de la categoría debe tener al menos 10 caracteres!',
            'name.max'          => '¡El nombre de la categoría debe tener 100 caracteres como máximo!',
            'name.unique'       => "¡La catgeoría ya existe!",

            'slug.required' => "¡La url de la categoría es un campo obligario!",
            'slug.min'      => "¡La url de la categoría debe contener al menos 5 caracteres!",
            'slug.max'      => "¡La url de la categoría debe contener máximo 200 caracteres!",
            'slug.unique'   => "¡La url de la categoría ya existe!"
        ];
    }
}
