<?php if(count($errors)>0): ?>
	<div class="alert dark alert-icon alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
		<i class="icon wb-close" aria-hidden="true"></i> ¡Error!
		<ul>
		<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<li><?php echo e($error); ?></li>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</ul>
	</div>
<?php endif; ?>

<?php if(session("info")): ?>
	<div class="alert dark alert-icon alert-success alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	    </button>
	    <p class="icon-text"><i class="fa fa-info-circle" aria-hidden="true"></i> Información</p>
		<?php echo session("info"); ?>

	</div>
<?php endif; ?>

<?php if(session("info-error")): ?>
	<div class="alert dark alert-icon alert-danger alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	    </button>
	    <p class="icon-text"><i class="fa fa-info-circle" aria-hidden="true"></i> Error</p>
		<?php echo session("info-error"); ?>

	</div>
<?php endif; ?>
