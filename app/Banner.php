<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Banner extends Model
{
    //SoftDelete
    use SoftDeletes;

    protected $dates = ["delete_at"];

    protected $guard = array();
}
