<?php

namespace App\Http\Controllers\Front;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use DB;
use App\ProductAttribute;
use App\Banner;
use Illuminate\Support\Facades\Session;//Important

class FrontController extends Controller
{
	//Inicio
 	public function index(){
 	   
 		$banner = Banner::get();
 		$url =  URL::current();

 	   	return view("frontend.index", compact("banner", "url"));
 	}  

 	// Vista de todos los  Productos
 	public function products()
 	{	
 		
 		//Order ascendente
 	 	$front_products = Product::orderBy("id","desc")->paginate(10);
 	 	//Order desceniente
 	 	$front_products = Product::orderBy("id","desc")->paginate(10);
 	 	//Random order
 	 	$front_products = Product::inRandomOrder()->paginate(10);

 	 	//Categories
 	 	$categories = Category::with("categories")->where(["parent_id"=>0])->get();
 	 	
 	 	//Disponible en Stock

	 	
 	 	return view("frontend.products", compact("front_products","categories","category"));
 	} 

 	//Productos relacionados a una categoría
 	public function products_category($slug)
 	{	

 		$countCategory = Category::where(["slug"=>$slug,"status"=>"Activa"])->count();
 		if ($countCategory==0) {
 			return view("frontend.errors.404");
 		}
 		// $categories = Category::with("categories")->where(["parent_id"=>0])->get();
 		// $categoriesDetails = Category::where(["url"=>$url])->first();
 		// $productsAll = Product::where(["category_id"=>$categoriesDetails->id])->get();
 		$categories = Category::with("categories")->where(["parent_id"=>0])->get();
 		$category 	= Category::where(["slug" => $slug])->first();
        $products   = Product::where(["category_id"=>$category->id])->paginate();

        return view("frontend.category_products", compact("products", "category","categories"));
 	}

 	//Single Product
 	public function single_product($slug, $id=null)
 	{
 		$categories = Category::with("categories")->where(["parent_id"=>0])->get();
 		// if ($countProduct==0) {
 		// 	return view("frontend.errors.404");
 		// }
 		$productDetails = Product::with("attributes")->where("slug", $slug)->first();

 		//Productos relacionados
 		$relatedProducts= Product::where("id", "!=", $id)->where(["category_id"=>$productDetails->category_id])->get();

 		foreach ($relatedProducts->chunk(4) as $chunk) {
 			foreach ($chunk as $item) {
 				// echo $item; echo '<br>';
 			}

 			// echo "<br><br><br>";
 		}

 		//Disponibilidad de Stock en la tienda
 		$total_stock  = ProductAttribute::where("product_id", $id)->sum("stock"); 

 		return view("frontend.single_product", compact("productDetails", "categories", "total_stock", "relatedProducts"));


 	}

 	//Precio Dinamico/Ajax -> mainFront.js
 	public function get_product_price(Request $request)
 	{
 		$data = $request->all();
 		// echo "<pre>";print_r($data); die;
 		$proArr = explode("-",$data['idSize']);
 		// echo $proArr[0]; echo $proArr[1]; die;
 		$proAttr = ProductAttribute::where(["product_id" => $proArr[0], "size" => $proArr[1]])->first();
 		 echo $proAttr->price;

 		 echo '#';

 		 echo $proAttr->stock;

 	}

 	//Añadir al carrito de compras
 	public function addCart(Request $request){
 		$data = $request->all();

 		if (empty($data["user_email"])) {
 			$data["user_email"]="";
 		}

 		$session_id = Session::get("session_id");
 		if (empty($session_id)) {
 			$session_id = str_random(40);
 			Session::put("session_id", $session_id);
 		}

 		$sizeArr = explode("-", $data["size"]);//Almacena el valor del tamaño del producto que se pasa por parametro

 		//#############Evitar duplicacones de productos en el carrito#################
 		$countProduct = DB::table("car")->where(["product_id"=>$data["product_id"], "color"=>$data["color"], "size"=>$sizeArr[1], "session_id" => $session_id])->count();

 		if($countProduct>0){
 			return redirect()->back()->with("info", "El producto ya está agregado en el carrito");
 		}else{
 			$getSKU = ProductAttribute::select("sku")->where(["product_id"=>$data["product_id"], "size"=>$sizeArr[1]])->first();

 			//Inserta productos en la tabla "car", para la vista de carrito
 			DB::table("car")->insert(["product_id"=>$data["product_id"], "name"=>$data["name"], "code"=>$getSKU->sku, "color"=>$data["color"], "price"=>$data["price"], "size"=>$sizeArr[1], "quantity"=>$data["quantity"], "user_email"=>$data["user_email"], "session_id" => $session_id]);
 		}
 		//############# End Evitar duplicacones de productos en el carrito#################

 		//$sizeArr[1] = Se registra el valor seleccionado dependiendo del tamaño del objeto

 		return redirect("products/cart")->with("info", "Se ha agregado al carrito el producto " . $data["name"]);
 	}

 	public function cartPage()
 	{
 		$monto_total = 0;
 		//Usuarios que agregan productos al acrrito
 		$session_id = Session::get("session_id");
 		$userCart = DB::table("car")->where(["session_id" => $session_id])->get();
 		$productDetails = "";
 		//Enviar imagen para mostrar en el carrito
 		foreach ($userCart as $key => $product) {
 			$productDetails = Product::where("id", $product->product_id)->first();//Consulta a la tabla de productos
 			$userCart[$key]->image = $productDetails->image;
 		}
 		// echo '<pre>'; print_r($userCart); die;

 		return view("frontend.cart", compact("session_id", "userCart", "$productDetails", 'monto_total'));
 	}

 	public function upQuantity($id = null, $quantity = null)
 	{	
 		//Se hace el chequeo de que el producto se encuentre en stock para poder ser llevado al carrito
 		$getCarDetails = DB::table("car")->where("id", $id)->first();
 		$getAttributesStock = ProductAttribute::where("sku", $getCarDetails->code)->first();
 		echo $getAttributesStock->sku; echo '--';
 		$updateQuantity = $getCarDetails->quantity+$quantity;

 		if($getAttributesStock->stock >= $updateQuantity){
 			//Actualiza el carrito de compras. La varialble CartUpdate almacena los valores de modificación de cantidades
 			$cartUpdate = DB::table("car")->where("id", $id)->increment("quantity", $quantity);
 			return redirect("products/cart")->with("info", "Se ha actualizado el carrito");
 		}else{
 			return redirect("products/cart")->with("info-error", "Solo tenemos " . $getAttributesStock->stock. " unidades de este producto");
 		}
 		
 	}

 	public function monto_total($value='')
 	{
 		

 		return view("frontend.cart", compact("session_id", "userCart", "$productDetails"));
 	}


 	public function deleteCart($id = null)
 	{
 		DB::table("car")->where("id", $id)->delete();

 		return redirect("products/cart")->with("info", "Se ha retirado el producto del carrito");
 	}


 	public function about()
 	{
 	 	return view("frontend.about");
 	} 

 	public function contact()
 	{
 	 	return view("frontend.contact");
 	} 
}
