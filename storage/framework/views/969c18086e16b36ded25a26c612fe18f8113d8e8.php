<footer class="site-footer site-footer-1 clearfix">
    	<div class="site-footer__top">
    		<div class="container">
	    		<div class="newsletter text-white">
		    	    <h3>Subscribe Newsletter</h3>
					<p class="newsletter-description">Sign up to receive 15% off your first order! </p>
					<form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank" class="form-horizontal">
						<div class="newsletter-form">
						<input type="email" value="" placeholder="Email address" name="EMAIL" id="mail" aria-label="email@example.com">
						<input type="submit" class="btn" name="subscribe" id="subscribe" value="">
						</div>
					</form>
				</div>
			</div>
		</div>
    	<div class="site-footer__main">
	    	<div class="container">
	    		<div class="row">
			    	<div class="col-md-3 col-sm-3 col-xs-12 footer-block footer-1 text-center">
		    			<h3 class="widget-title">Quick Links</h3>
		    			<ul>
		    				<li><a href="about.html">About Us</a></li>
		    				<li><a href="contact.html">Contact Us</a></li>
		    				<li><a href="blog.html">Blog</a></li>
		    				<li><a href="account.html">My Account</a></li>
		    				<li><a href="product_grid.html">Shop</a></li>
		    				<li><a href="account.html">Register</a></li>
		    			</ul>
			    	</div>
			    	<div class="col-md-6 col-sm-5 col-xs-12 footer-block footer-center text-center">
			    		<img src="<?php echo e(asset("front/assets/images/logo.png")); ?>" alt="">
			    		<p class="address">Office: 972 Sylvan Street South Angelina, NL S0B2V9<br/>
							Phone: (633) 497-1888 / (062) 109-9222<br/>
							Email: example.com
						</p>
						<ul class="social">
							<li>
								<a href="#"><i class="fa fa-facebook"></i></a>
							</li>
							<li>
								<a href="#"><i class="fa fa-twitter"></i></a>
							</li>
							<li>
								<a href="#"><i class="fa fa-google"></i></a>
							</li>
							<li>
								<a href="#"><i class="fa fa-instagram"></i></a>
							</li>
							<li>
								<a href="#"><i class="fa fa-pinterest"></i></a>
							</li>
						</ul>
			    	</div>
			    	<div class="col-md-3 col-sm-4 col-xs-12 footer-block footer-3">
			    		<h3 class="widget-title">OPENING TIME</h3>
		    			<ul>
		    				<li>
		    					<span>Monday :</span>
		    					<span>9am - 5pm</span>
		    				</li>
		    				<li>
		    					<span>Tuesday :</span>
		    					<span>9am - 1pm</span>
		    				</li>
		    				<li>
		    					<span>Wendsday :</span>
		    					<span>9am - 5pm</span>
		    				</li>
		    				<li>
		    					<span>Thursday :</span>
		    					<span>9am - 1pm</span>
		    				</li>
		    				<li>
		    					<span>Friday :</span>
		    					<span>9am - 1pm</span>
		    				</li>
		    				<li>
		    					<span>Saturday - Sunday :</span>
		    					<span>closed</span>
		    				</li>
		    			</ul>
			    	</div>
			    </div>
		    </div>
	   </div>
	   <div class="site-footer__copyright">
	   		<div class="container">
	    		<div class="copyright pull-left">
	        		© 2018. All rights reserved.<a href="#" target="_blank">Enercos</a>
	        	</div>
	        	<div class="payment pull-right">
	        		<img src="<?php echo e(asset("front/assets/images/payment.png")); ?>" alt="">
	        	</div>
	       </div>
	   </div>
    </footer>
    <!-- =====================================================================
    	==== End slider -->

   	<!-- Back to top -->
    <div id="back-to-top"></div>
    <!--/ Back to top -->

   <!-- =====================================================================
    	==== Start preloader -->
    <div id="preloader">
		<div class="preloader">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
   <!-- =====================================================================
    	==== End preloader -->

	<!-- =====================================
    	====Start all js here -->

	<!-- jquery js-->
	<script src="<?php echo e(asset("front/assets/js/jquery.min.js")); ?>"></script>

	<!-- bootstrap js-->
	<script src="<?php echo e(asset("front/assets/js/bootstrap.min.js")); ?>"></script>

	<!-- slick js-->
	<script src="<?php echo e(asset("front/assets/js/slick.min.js")); ?>"></script>

	<!-- spritespin js view  360-->
	<script src="<?php echo e(asset("front/assets/js/spritespin.min.js")); ?>"></script>

	<!-- jquery-ui js price -->
	<script src="<?php echo e(asset("front/assets/js/jquery-ui.min.js")); ?>"></script>

	<!-- sliderPro js -->
	<script src="<?php echo e(asset("front/assets/js/jquery.sliderPro.min.js")); ?>"></script>

	<!-- prettyPhoto js-->
	<script src="<?php echo e(asset("front/assets/js/jquery.prettyPhoto.js")); ?>"></script>

	<!-- counterup js-->
    <script src="<?php echo e(asset("front/assets/js/jquery.waypoints.js")); ?>"></script>
	<script src="<?php echo e(asset("front/assets/js/jquery.counterup.min.js")); ?>"></script>

	<!-- main js-->
	<script src="<?php echo e(asset("front/assets/js/custom.js")); ?>"></script>

	<script src="<?php echo e(asset("front/assets/js/mainFront.js")); ?>"></script>

	<?php echo $__env->yieldContent("scripts"); ?>