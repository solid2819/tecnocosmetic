<?php echo $__env->make("layouts.header", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


  <div class="site-menubar">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu">
           <!-- <li class="site-menu-category">General</li>-->
          <br/>
 <!-------------- Dashboard menu --------------->			
	 <li class="site-menu-category" style="margin-top:1px; text-transform:none;font-size:16px; color:white; "><b>Menú</b></li>
	<!-------------- Users menu --------------->	
 <?php if (\Entrust::can(['users.manage', 'users.activity'])) : ?>	
           
			<li class="site-menu-item has-sub  <?php echo e(Request::is('userlist','registration','activity') ? 'active open' : ''); ?>">
              <a href="javascript:void(0)">
			  <i class="site-menu-icon wb-user" aria-hidden="true"></i>
                <span class="site-menu-title"><?php echo e(trans('app.users')); ?></span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
               <?php if (\Entrust::can('users.manage')) : ?> 
				<li class="site-menu-item <?php echo e(Request::is('registration') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/registration')); ?>">
                    <span class="site-menu-title"><?php echo e(trans('app.add_user')); ?></span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('userlist') ? 'active' : ''); ?>">
                  <a class="animsition-link " href="<?php echo e(URL::to('userlist')); ?>">
                    <span class="site-menu-title"><?php echo e(trans('app.users')); ?></span>
                  </a>
                </li>
				<?php endif; // Entrust::can ?>
				<?php if (\Entrust::can('users.activity')) : ?> 
                <li class="site-menu-item <?php echo e(Request::is('activity') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('activity')); ?>">
                    <span class="site-menu-title"><?php echo e(trans('app.activity_log')); ?></span>
                  </a>
                </li>  
				<?php endif; // Entrust::can ?>
              </ul>			  
            </li>
			<?php endif; // Entrust::can ?>
	<!-------------- language menu --------------->
			 <?php if (\Entrust::can('languages.languages')) : ?>
			 <li class="site-menu-item has-sub <?php echo e(Request::is('language') ? 'active open' : ''); ?>">
              <a href="javascript:void(0)">
			  <i class="site-menu-icon fa-language" aria-hidden="true"></i>
                <span class="site-menu-title"><?php echo e(trans('app.languages')); ?></span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item <?php echo e(Request::is('language') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL('/language')); ?>">
                    <span class="site-menu-title"><?php echo e(trans('app.languages')); ?></span>
                  </a>
                </li>
              </ul>
            </li>
			<?php endif; // Entrust::can ?>
<!-------------- roles and permission  menu --------------->
		 <?php if (\Entrust::can(['roles.manage', 'permissions.manage'])) : ?>
			 <li class="site-menu-item has-sub <?php echo e(Request::is('permissions','roles') ? 'active open' : ''); ?>">
              <a href="javascript:void(0)">
			  <i class="site-menu-icon fa-lock" aria-hidden="true"></i>
                <span class="site-menu-title"><?php echo e(trans('app.roles_and_permissions')); ?></span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
			   <?php if (\Entrust::can('roles.manage')) : ?>
                <li class="site-menu-item <?php echo e(Request::is('roles') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/roles')); ?>">
                    <span class="site-menu-title"><?php echo e(trans('app.roles')); ?></span>
                  </a>
                </li>
				<?php endif; // Entrust::can ?>
				 <?php if (\Entrust::can('roles.manage')) : ?>
				<li class="site-menu-item <?php echo e(Request::is('permissions') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/permissions')); ?>">
                    <span class="site-menu-title"><?php echo e(trans('app.permissions')); ?></span>
                  </a>
                </li>
				<?php endif; // Entrust::can ?>
              </ul>
            </li>
		<?php endif; // Entrust::can ?>	
<!-------------- Message  menu --------------->
		<?php if (\Entrust::can('message.messages')) : ?>
			 <li class="site-menu-item has-sub <?php echo e(Request::is('message') ? 'active open' : ''); ?>">
              <a href="javascript:void(0)">
			  <i class="site-menu-icon fa fa-envelope-o" aria-hidden="true"></i>
                <span class="site-menu-title"><?php echo e(trans('app.messages')); ?></span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item <?php echo e(Request::is('message') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/message')); ?>">
                    <span class="site-menu-title"><?php echo e(trans('app.messages')); ?></span>
                  </a>
                </li>
              </ul>
            </li>
		<?php endif; // Entrust::can ?>		
<!-------------- Settings  menu --------------->
		<?php if (\Entrust::can('settings.general')) : ?>
			 <li class="site-menu-item has-sub <?php echo e(Request::is('settings') ? 'active open' : ''); ?>">
              <a href="javascript:void(0)">
			  <i class="site-menu-icon wb-settings" aria-hidden="true"></i>
                <span class="site-menu-title"><?php echo e(trans('app.settings')); ?></span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item <?php echo e(Request::is('settings') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/settings')); ?>">
                    <span class="site-menu-title"><?php echo e(trans('app.general_settings')); ?></span>
                  </a>
                </li>
              </ul>
            </li>			
         <?php endif; // Entrust::can ?>	
     

  <?php echo $__env->make("layouts.sidebar", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>




	 <li class="site-menu-category" style="text-transform:none;font-size:16px; color:white;"><b>Basic Elements</b></li>
		<li class="site-menu-item has-sub <?php echo e(Request::is('buttons','dropdowns','icons','tooltip','modals','tabsAccordions','imagesDes','badgesLabels','progressBars','carousel','typography','colors') ? 'active open' : ''); ?>">
              <a href="javascript:void(0)">
                <i class="site-menu-icon wb-bookmark" aria-hidden="true"></i>
                <span class="site-menu-title">Basic UI</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">               
                <li class="site-menu-item <?php echo e(Request::is('buttons') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/buttons')); ?>">
                    <span class="site-menu-title">Buttons</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('dropdowns') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/dropdowns')); ?>">
                    <span class="site-menu-title">Dropdowns</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('icons') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/icons')); ?>">
                    <span class="site-menu-title">Icons</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('tooltip') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/tooltip')); ?>">
                    <span class="site-menu-title">Tooltip &amp; Popover</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('modals') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/modals')); ?>">
                    <span class="site-menu-title">Modals</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('tabsAccordions') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/tabsAccordions')); ?>">
                    <span class="site-menu-title">Tabs &amp; Accordions</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('imagesDes') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/imagesDes')); ?>">
                    <span class="site-menu-title">Images</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('badgesLabels') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/badgesLabels')); ?>">
                    <span class="site-menu-title">Badges &amp; Labels</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('progressBars') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/progressBars')); ?>">
                    <span class="site-menu-title">Progress Bars</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('carousel') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/carousel')); ?>">
                    <span class="site-menu-title">Carousel</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('typography') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/typography')); ?>">
                    <span class="site-menu-title">Typography</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('colors') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/colors')); ?>">
                    <span class="site-menu-title">Colors</span>
                  </a>
                </li>
              </ul>
            </li>
	<!----------- end basic ui ------------->		
		
<!------- Start stacture ------------->	
			<li class="site-menu-item has-sub <?php echo e(Request::is('alerts','ribbon','pricingTables','overlay','cover','timeline','step','chat') ? 'active open' : ''); ?>">
              <a href="javascript:void(0)">
                <i class="site-menu-icon wb-plugin" aria-hidden="true"></i>
                <span class="site-menu-title">Structure</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item <?php echo e(Request::is('alerts') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/alerts')); ?>">
                    <span class="site-menu-title">Alerts</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('ribbon') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/ribbon')); ?>">
                    <span class="site-menu-title">Ribbon</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('pricingTables') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/pricingTables')); ?>">
                    <span class="site-menu-title">Pricing Tables</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('overlay') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/overlay')); ?>">
                    <span class="site-menu-title">Overlay</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('cover') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/cover')); ?>">
                    <span class="site-menu-title">Cover</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('timeline') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/timeline')); ?>">
                    <span class="site-menu-title">Timeline</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('step') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/step')); ?>">
                    <span class="site-menu-title">Step</span>
                  </a>
                </li>                
                <li class="site-menu-item <?php echo e(Request::is('chat') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/chat')); ?>">
                    <span class="site-menu-title">Chat</span>
                  </a>
                </li>                
              </ul>
            </li>	
<!------- End stacture  ------------->	

	
<!------- Start Forms  ------------->	
		<li class="site-menu-item has-sub <?php echo e(Request::is('general','material','layouts','wizard','validation','masks','imageCropping','fileUploads') ? 'active open' : ''); ?>">
              <a href="javascript:void(0)">
                <i class="site-menu-icon wb-library" aria-hidden="true"></i>
                <span class="site-menu-title">Forms</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item <?php echo e(Request::is('general') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/general')); ?>">
                    <span class="site-menu-title">General Elements</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('material') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/material')); ?>">
                    <span class="site-menu-title">Material Elements</span>
                  </a>
                </li>               
                <li class="site-menu-item <?php echo e(Request::is('layouts') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/layouts')); ?>">
                    <span class="site-menu-title">Form Layouts</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('wizard') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/wizard')); ?>">
                    <span class="site-menu-title">Form Wizard</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('validation') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/validation')); ?>">
                    <span class="site-menu-title">Form Validation</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('masks') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/masks')); ?>">
                    <span class="site-menu-title">Form Masks</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('imageCropping') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/imageCropping')); ?>">
                    <span class="site-menu-title">Image Cropping</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('fileUploads') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/fileUploads')); ?>">
                    <span class="site-menu-title">File Uploads</span>
                  </a>
                </li>
              </ul>
            </li>
<!------- End Forms  ------------->	

<!------- Start Tables  ------------->
		<li class="site-menu-item has-sub <?php echo e(Request::is('basicTables','responsiveTable','editableTable','jsgrid') ? 'active open' : ''); ?>">
              <a href="javascript:void(0)">
                <i class="site-menu-icon wb-table" aria-hidden="true"></i>
                <span class="site-menu-title">Tables</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item <?php echo e(Request::is('basicTables') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/basicTables')); ?>">
                    <span class="site-menu-title">Basic Tables</span>
                  </a>
                </li>
               
                <li class="site-menu-item <?php echo e(Request::is('responsiveTable') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/responsiveTable')); ?>">
                    <span class="site-menu-title">Responsive Tables</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('editableTable') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/editableTable')); ?>">
                    <span class="site-menu-title">Editable Tables</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('jsgrid') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/jsgrid')); ?>">
                    <span class="site-menu-title">jsGrid</span>
                  </a>
                </li>                
              </ul>
            </li>
<!------- End Tables  ------------->
	
<!------- Start Pages  ------------->
         <li class="site-menu-item has-sub <?php echo e(Request::is('error400','error500','faq','gallery','invoice','searchResult','profiledemo') ? 'active open' : ''); ?>">
              <a href="javascript:void(0)">
                <i class="site-menu-icon wb-file" aria-hidden="true"></i>
                <span class="site-menu-title">Pages</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Errors</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item <?php echo e(Request::is('error400') ? 'active' : ''); ?>">
                      <a class="animsition-link" href="<?php echo e(URL::to('/error400')); ?>">
                        <span class="site-menu-title">400</span>
                      </a>
                    </li>                    
                    <li class="site-menu-item <?php echo e(Request::is('error500') ? 'active' : ''); ?>">
                      <a class="animsition-link" href="<?php echo e(URL::to('/error500')); ?>">
                        <span class="site-menu-title">500</span>
                      </a>
                    </li>                    
                  </ul>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('faq') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/faq')); ?>">
                    <span class="site-menu-title">FAQ</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('gallery') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/gallery')); ?>">
                    <span class="site-menu-title">Gallery</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('searchResult') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/searchResult')); ?>">
                    <span class="site-menu-title">Search Result</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('invoice') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/invoice')); ?>">
                    <span class="site-menu-title">Invoice</span>
                  </a>
                </li>
                
                <li class="site-menu-item <?php echo e(Request::is('profiledemo') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/profiledemo')); ?>">
                    <span class="site-menu-title">Profile</span>
                  </a>
                </li>                
              </ul>
            </li>	
<!------- End pages  ------------->	
<!----------- Start Advanced ui ------------->	
			<li class="site-menu-item has-sub <?php echo e(Request::is('animation','lightbox','scrollable','rating','toastr','sortableNestable','bootboxSweetalert') ? 'active open' : ''); ?>">
              <a href="javascript:void(0)">
                <i class="site-menu-icon wb-hammer" aria-hidden="true"></i>
                <span class="site-menu-title">Advanced UI</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">                
                <li class="site-menu-item <?php echo e(Request::is('animation') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/animation')); ?>">
                    <span class="site-menu-title">Animation</span>
                  </a>
                </li>               
                <li class="site-menu-item <?php echo e(Request::is('lightbox') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/lightbox')); ?>">
                    <span class="site-menu-title">Lightbox</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('scrollable') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/scrollable')); ?>">
                    <span class="site-menu-title">Scrollable</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('rating') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/rating')); ?>">
                    <span class="site-menu-title">Rating</span>
                  </a>
                </li>                
                <li class="site-menu-item <?php echo e(Request::is('toastr') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/toastr')); ?>">
                    <span class="site-menu-title">Toastr</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('sortableNestable') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/sortableNestable')); ?>">
                    <span class="site-menu-title">Sortable &amp; Nestable</span>
                  </a>
                </li>
                <li class="site-menu-item <?php echo e(Request::is('bootboxSweetalert') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/bootboxSweetalert')); ?>">
                    <span class="site-menu-title">Bootbox &amp; Sweetalert</span>
                  </a>
                </li>
              </ul>
            </li>	
<!------- end advanced ui ------------->			
<!------- Start Widgets  ------------->
		<li class="site-menu-item has-sub <?php echo e(Request::is('social') ? 'active open' : ''); ?>">
              <a href="javascript:void(0)">
                <i class="site-menu-icon wb-extension" aria-hidden="true"></i>
                <span class="site-menu-title">Widgets</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">  
                <li class="site-menu-item <?php echo e(Request::is('social') ? 'active' : ''); ?>">
                  <a class="animsition-link" href="<?php echo e(URL::to('/social')); ?>">
                    <span class="site-menu-title">Social Widgets</span>
                  </a>
                </li>                
              </ul>
            </li>	
<!------- End Widgets  ------------->
	
          </ul>
        </div>
      </div>
    </div>
    <div class="site-menubar-footer">
	<a href="<?php echo e(URL::to('profileEdit')); ?>" data-placement="top" data-toggle="tooltip" data-original-title="<?php echo e(trans('app.edit_profile')); ?>">
       <i class="icon wb-pencil" aria-hidden="true"></i>
      </a>
       <?php if(Auth::user()->hasRole('Admin')): ?>
	  <a href="<?php echo e(URL::to('settings')); ?>" class="fold-show" data-placement="top" data-toggle="tooltip"
      data-original-title="<?php echo e(trans('app.settings')); ?>">
        <span class="icon wb-settings" aria-hidden="true"></span>
      </a>
	  <?php else: ?>
	  <a class="fold-show" data-placement="top" data-toggle="tooltip"
      data-original-title="">
        &nbsp;
      </a>
	  <?php endif; ?>
      <a href="<?php echo e(url('/logout')); ?>"
			onclick="event.preventDefault();
					 document.getElementById('logout-form').submit();" data-placement="top" data-toggle="tooltip" data-original-title="<?php echo e(trans('app.logout')); ?>">
			<span class="icon wb-power" aria-hidden="true"></span>
		</a>
		<form id="logout-form" action="<?php echo e(url('/logout')); ?>" method="POST" style="display: none;">
			<?php echo e(csrf_field()); ?>

		</form>
    </div>
  </div>
<!-------------------- logo click menu---------------->
  <div class="site-gridmenu">
    <div>
      <div>
        <ul>
		 <li>
            <a href="<?php echo e(URL::to('/dashboard')); ?>">
              <i class="icon wb-dashboard"></i>
              <span><?php echo e(trans('app.dashboard')); ?></span>
            </a>
          </li>
		  <?php if (\Entrust::can('users.manage')) : ?>
		  <li>
            <a href="<?php echo e(URL::to('userlist')); ?>">
              <i class="icon wb-users" aria-hidden="true"></i>
              <span><?php echo e(trans('app.users')); ?></span>
            </a>
          </li>
		  <?php endif; // Entrust::can ?>
		  <?php if (\Entrust::can('languages.languages')) : ?>
		   <li>
            <a href="<?php echo e(URL::to('language')); ?>">
            <i class="icon fa-language" aria-hidden="true"></i>			
             
              <span><?php echo e(trans('app.languages')); ?></span>
            </a>
          </li>
		   <?php endif; // Entrust::can ?>
		   <?php if (\Entrust::can('message.messages')) : ?>
          <li>
            <a href="<?php echo e(URL::to('message')); ?>">
              <i class="icon wb-envelope"></i>
              <span><?php echo e(trans('app.messages')); ?></span>
            </a>
          </li> 
			 <?php endif; // Entrust::can ?>
			 <?php if (\Entrust::can('permissions.manage')) : ?>
          <li>
            <a href="<?php echo e(URL::to('permissions')); ?>">
              <i class="icon wb-lock"></i>
              <span><?php echo e(trans('app.permissions')); ?></span>
            </a>
          </li>
          <?php endif; // Entrust::can ?>
		  <?php if (\Entrust::can('settings.general')) : ?>
          <li>
            <a href="<?php echo e(URL::to('settings')); ?>">
              <i class="icon wb-settings" aria-hidden="true"></i>
              <span><?php echo e(trans('app.settings')); ?></span>
            </a>
          </li>
         <?php endif; // Entrust::can ?>
		
        </ul>
      </div>
    </div>
  </div>
  <!-- Page -->
 
 <maindd>
<div class="loadersjew">
	<div class="loaderjew"><div class="line-scale"><div></div><div></div><div></div><div></div><div></div></div></div>
	<!--<div class="loaderjew"><div class="line-scale-party"><div></div><div></div><div></div><div></div></div></div>-->
	<!--<div class="loader"><div class="line-scale-pulse-out"><div></div><div></div><div></div><div></div><div></div></div></div>
	<div class="loader"><div class="line-scale-pulse-out-rapid"><div></div><div></div><div></div><div></div><div></div></div></div>-->
	</div>
</maindd>

  <div class="page" style="animation-duration: 800ms; opacity: 1;">
   <!-- page content -->
   
	<?php echo $__env->yieldContent('content'); ?>
	<!-- /page content -->
  </div>
  <!-- End Page -->
  <!-- Footer -->
  <?php echo $__env->make("layouts.footer", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->yieldContent("scripts"); ?>


   <!--<div class="loader-fb"></div>-->
<script>
 $(window).load(function() {    
  $(".loadersjew").fadeOut("slow");;
 });
</script>
   
</body>
</html>








