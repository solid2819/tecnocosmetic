<?php $__env->startSection("title"); ?>
	| Imagenes del producto
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make("layouts.messages", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <div class="page-header">
          <h1 class="page-title font_lato">
            Agregar imagenes al producto
          </h1>
          <div class="page-header-actions">
            <ol class="breadcrumb">
              <li><a href="<?php echo e(URL::to('/dashboard')); ?>"><?php echo e(trans('app.home')); ?></a></li>
              <li><a href="<?php echo e(URL::to('/dashboard/products')); ?>"><?php echo e(trans('Products')); ?></a></li>
              <li><a href="<?php echo e(route("products.edit", $product_add_images->id)); ?>"><?php echo e($product_add_images->id); ?></a></li>
              <li class="active">Product Images</li>
            </ol>
          </div>
        </div> 
	     <div class="col-sm-12 col-md-12 ol-lg-12 ">
          <!-- Panel Floating Lables -->
          <?php if(count($errors)>0): ?>
            <?php echo $__env->make("layouts.messages", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <?php endif; ?>
         <div class="panel">
            <div class="panel-heading">
              <h2> <?php echo $product_add_images->name; ?></h2>
            </div>

            <div class="panel-body">   
             <div class="row">
               <div class="col-md-8">
                
                 <p>
                   <?php echo $product_add_images->description; ?>

                 </p>
               </div>

               <div class="col-md-4">
                 
               </div>

             </div>

             <div class="row"> 
              <form class="" method="POST" action="<?php echo e(url("dashboard/products/add-images/".$product_add_images->id)); ?>" enctype="multipart/form-data" name="add_image">
                <?php echo e(csrf_field()); ?>


                
                <input type="file" name="image[]" value="" multiple="multiple">
                <br>
                <button class="btn btn-outline btn-success "> Añadir imagenes</button>
                 
               </form>
             </div>
            </div>

          </div>
          <!-- End Panel Floating Lables -->
        </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>