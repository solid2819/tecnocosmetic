@extends('layouts.template')
@section("title")
	| Edición de producto
@endsection
@section('content')
        @if(count($errors)>0)
            @include("layouts.messages"){{--message--}}
          @endif
       <div class="page-header">
          <h1 class="page-title font_lato">
            Estas editando el producto : {{ $product->name }}
          </h1>
          <div class="page-header-actions">
            <ol class="breadcrumb">
              <li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
              <li><a href="{{URL::to('/dashboard/products')}}">{{ trans('Products')}}</a></li>
              <li class="active">Edit</li>
            </ol>
          </div>
        </div> 
	     <div class="col-sm-12 col-md-12 col-lg-12">
          <!-- Panel Floating Lables -->
         <div class="panel">
            <div class="panel-heading">
              <h2 class="panel-title">Editar producto</h2>
               <div class="container">
                 <a href="{{ route("product-attributes", $product->id) }}" class="btn-xs btn btn-outline btn-primary ">
                  <i class="fa fa-edit"></i> Editar Atributos
                 </a>
              </div>
              <div class="container">
                <p class="pull-right">Creado el: <span style="font-weight: bold">{{ $product->created_at->format('d-m-Y') }}</span></p>
              </div>
              <div class="container">
                <p class="pull-right">Actualizado el: <span style="font-weight: bold">{{ $product->updated_at->format('d-m-Y') }}</span></p>
              </div>
            </div>
            <div class="panel-body">
              
            {!! Form::model($product,["method" => "PUT", "route" => ["products.update", $product->id], "files" => true]) !!}
                    {!! Form::token() !!}
             @include("dashboard.products.partials.form-edit")
            {!! Form::close() !!}

            </div>
          </div>
          <!-- End Panel Floating Lables -->
        </div>
@endsection