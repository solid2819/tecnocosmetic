<div class="form-group">
  {{ Form::label("name", "Nombre de la categoría") }}
  {{ Form::text("name",null, ["class" => "form-control", "id" => "name"])}} 
</div>

<div class="form-group">
  <label for="">Subcategoría</label>
  <select class="form-control" name="parent_id">
    <option value="0">Categoría principal</option>
    @foreach($levels as $val)
      <option value="{{ $val->id }}">{{ $val->name }}</option>
    @endforeach
  </select>
</div>

<div class="form-group">
  {{ Form::label("slug", "Slug") }}
  {{ Form::text("slug",null, ["class" => "form-control", "id" => "slug"])}} 
</div>

{{-- <div class="form-group">
  {{ Form::label("status", "Estado") }}
  {{ Form::checkbox("status",null, ["class" => "form-control", "id" => "status"])}} <br>
  <small>Estado de la categoría: Marca esta opción si deseas que la categoría esté activa al momento de crearla. Deja la casilla desmarcada si quieres que la categoría se cree, pero no este activa.</small>
</div> --}}

<div class="form-group">
  {{ Form::label("description", "Descripción") }}
  {{ Form::textarea("description",null, ["class" => "form-control", "id" => "description"])}} 
</div>

<div class="form-group">
  {{ Form::submit("Crear", ["class" => "btn btn-success btn-sm"]) }}
   <button type="reset" class="btn btn-danger  btn-sm">
    Borrar
  </button>
  <a href="{{ route("categories.index") }}">
    <button type="button" class="btn btn-warning btn-sm">
      Volver
    </button>
  </a>
  
</div>


@section("scripts")
  <script src="{{ asset("vendor/jquerystringToSlug/jquery-stringToSlug.min.js") }} "></script>
  
  <script>
    $(document).ready(function () {
      $("#name, #slug").stringToSlug({
        callback: function (text) {
          $("#slug").val(text);
        }
      });
    });
  </script>
@endsection