@extends('layouts.template')
@section("title")
	| Edición de categoría
@endsection
@section('content')
        
	     <div class="col-sm-12 col-md-8 ol-lg-8 col-md-offset-2" style="margin-top: 20px;">
          <!-- Panel Floating Lables -->
         <div class="panel">
            <div class="panel-heading">
              <h3 class="panel-title">Categoría : {{ $category->name }}</h3>
            </div>
            <div class="panel-body">
              
            {!! Form::model($category,["method" => "PATCH", "route" => ["categories.update", $category->id]]) !!}
                    {{-- {!! Form::token() !!} --}}
             @include("dashboard.products.categories.partials.form-edit")
            {!! Form::close() !!}

            </div>
          </div>
          <!-- End Panel Floating Lables -->
        </div>
@endsection