<?php $__env->startSection("styles"); ?>
  <style type="text/css">
    .inp_attr_product{
      margin-left: 1vw;
 
    }
  </style>
<?php $__env->stopSection(); ?>

<div class="row">

  <div class="col-xs-12  col-sm-12 col-md-9 col-lg-9 col-xl-9 offset-1">
    <input type="hidden" name="product_id" value="<?php echo e($productsDetails->id); ?>">
	    <div class="form-group">
	      <?php echo e(Form::label("name", "Producto: ")); ?>

	      &nbsp<label><strong><?php echo e($productsDetails->name); ?></strong></label>
	    </div>

		  <div class="form-group">
	      <?php echo e(Form::label("code", "Código: ")); ?>

	      &nbsp<label><strong><?php echo e($productsDetails->code); ?></strong></label>
	    </div>

		  <div class="form-group">
	      <?php echo e(Form::label("color", "Color: ")); ?>

	      &nbsp<label><strong><?php echo e($productsDetails->color); ?></strong></label>
	    </div>

       <div class="form-group">
        <div class="wrapper">
          <div>
            <input class="inp_attr_product" type="text" name="sku[]" id="sku_attr_product" placeholder="SKU" />
            <input class="inp_attr_product" type="text" name="size[]" id="size_attr_product" placeholder="Size" />
            <input class="inp_attr_product" type="text" name="price[]" id="price_attr_product" placeholder="Precio" />
            <input class="inp_attr_product" type="text" name="stock[]" id="stock_attr_product" placeholder="Stock" />

            <button  class="add_fields btn btn-xs btn-outline btn-info" >
              <i class="fa fa-plus"></i> Añadir
            </button>
          </div>
        </div>
      </div>
	</div>
</div>

<div class="row">
    <div class="form-group">
      <?php echo e(Form::submit("Añadir atributos", ["class" => "btn btn-success btn-sm"])); ?>


      <a href="<?php echo e(route("products.edit", $productsDetails->id)); ?>">
        <button type="button" class="btn btn-warning btn-sm">
          Volver
        </button>
      </a>
      
    </div>
</div>


<?php $__env->startSection("scripts"); ?>
  <script>
    //Add Input Fields
    $(document).ready(function() {
        var max_fields = 10; //Maximum allowed input fields 
        var wrapper    = $(".wrapper"); //Input fields wrapper
        var add_button = $(".add_fields"); //Add button class or ID
        var x = 1; //Initial input field is set to 1
      
      //When user click on add input button
      $(add_button).click(function(e){
            e.preventDefault();
        //Check maximum allowed input fields
            if(x < max_fields){ 
                x++; //input field increment
           //add input field
                $(wrapper).append('<div><input type="text" name="sku[]" id="sku_attr_product" placeholder="SKU" style=" margin-left: 1vw; margin-top:0.5vw;"/> <input type="text" name="size[]" id="size_attr_product" placeholder="Size" style=" margin-left: 1vw; margin-top:0.5vw;"/> <input type="text" name="price[]" id="size_attr_product" placeholder="Precio" style=" margin-left: 1vw; margin-top:0.5vw;"/> <input type="text" name="stock[]" id="stock_attr_product" placeholder="Stock" style=" margin-left: 1vw; margin-top:0.5vw;"/> <a href="javascript:void(0);" class="remove_field"><i class="fa fa-remove text-danger"></i></a></div>');
            }
        });
      
        //when user click on remove button
        $(wrapper).on("click",".remove_field", function(e){ 
            e.preventDefault();
        $(this).parent('div').remove(); //remove inout field
        x--; //inout field decrement
        })
    });
  </script>
<?php $__env->stopSection(); ?>