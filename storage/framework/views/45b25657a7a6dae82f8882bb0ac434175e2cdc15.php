<li class="site-menu-item has-sub <?php echo e(Request::is('dashboard') ? 'active open' : ''); ?>">
  <a href="javascript:void(0)" >
    <i class="site-menu-icon fa fa-dashboard" aria-hidden="true"></i>
    <span class="site-menu-title">Dashboard</span>
    <span class="site-menu-arrow"></span>
  </a>
  <ul class="site-menu-sub">
    <li class="site-menu-item <?php echo e(Request::is('dashboard/') ? 'active open' : ''); ?>" style="margin-left: 1.5vw;">
      <a class="animsition-link" href="<?php echo e(URL::to('/dashboard')); ?>">
        <span class="site-menu-title">
        <span class="fa fa-home"> </span>&nbsp
        Home</span>
      </a>
    </li>

    <li class="site-menu-item <?php echo e(Request::is('dashboard/product/categories') ? 'active open' : ''); ?>" style="margin-left: 1.5vw;">
      <a class="animsition-link" href="/" target="_blank">
        <span class="site-menu-title">
        <span class="fa fa-globe"> </span>&nbsp
        Sitio web</span>
      </a>
    </li>

  </ul>
</li>

<li class="site-menu-item has-sub <?php echo e(Request::is('dashboard/products*', 'dashboard/product/categories*', 'dashboard/products/edit', 'dashboard/product/trash', 'dashboard/products/attributes/') ? 'active open' : ''); ?>">
  <a href="javascript:void(0)" >
    <i class="site-menu-icon fa fa-cubes" aria-hidden="true"></i>
    <span class="site-menu-title">Productos</span>
    <span class="site-menu-arrow"></span>
  </a>
  <ul class="site-menu-sub">
    <li class="site-menu-item <?php echo e(Request::is('dashboard/products/create*') ? 'active open' : ''); ?>" style="margin-left: 1.5vw;">
      <a class="animsition-link" href="<?php echo e(URL::to('dashboard/products/create')); ?>">
        <span class="site-menu-title">
        <span class="fa fa-plus"> </span>&nbsp
        Crear</span>
      </a>
    </li>

    <li class="site-menu-item <?php echo e(Request::is('dashboard/products', 'dashboard/product/trash') ? 'active open' : ''); ?>" style="margin-left: 1.5vw;">
      <a class="animsition-link" href="<?php echo e(URL::to('dashboard/products')); ?>">
        <span class="site-menu-title">
        <span class="fa fa-cube"> </span>&nbsp
        Ver productos</span>
      </a>
    </li>

    <li class="site-menu-item <?php echo e(Request::is('dashboard/product/categories*') ? 'active open' : ''); ?>" style="margin-left: 1.5vw;">
      <a class="animsition-link" href="<?php echo e(URL::to('dashboard/product/categories')); ?>">
        <span class="site-menu-title">
        <span class="fa fa-sitemap"> </span>&nbsp
        Categorías</span>
      </a>
    </li>

  </ul>
</li>


<li class="site-menu-category" style="text-transform:none;font-size:16px; color:white;"><b>Configuraciones</b></li>
    <li class="site-menu-item has-sub <?php echo e(Request::is('dashboard/config/design/banners', 'dashboard/config/design/banners/create', 'dashboard/config/design/banners/{id}/edit') ? 'active open' : ''); ?>">
              <a href="javascript:void(0)">
                <i class="fa fa-columns" aria-hidden="true"></i>
                <span class="site-menu-title">Diseño</span>
               
              </a>
              <ul class="site-menu-sub">               
                 <li class="site-menu-item <?php echo e(Request::is('dashboard/config/design/banners', 'dashboard/config/design/banners/{id}/edit') ? 'active open' : ''); ?>" style="margin-left: 1.5vw;">
                    <a class="animsition-link" href="<?php echo e(URL::to('dashboard/config/design/banners')); ?>">
                      <span class="site-menu-title">
                      <span class="fa fa-building-o"> </span>&nbsp
                      Banners</span>
                    </a>
                  </li>

                   <li class="site-menu-item <?php echo e(Request::is('dashboard/config/design/banners/create') ? 'active open' : ''); ?>" style="margin-left: 1.5vw;">
                    <a class="animsition-link" href="<?php echo e(URL::to('dashboard/config/design/banners/create')); ?>">
                      <span class="site-menu-title">
                      <span class="fa fa-plus"> </span>&nbsp
                      Nuevo Banner</span>
                    </a>
                  </li>
              </ul>
            </li>
