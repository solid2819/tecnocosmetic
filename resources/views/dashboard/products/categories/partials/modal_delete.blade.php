                <div class="modal fade modal-3d-flip-vertical" id="modal-delete-{{ $category->id }}" aria-hidden="true"
                  aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
                   {{{ Form::Open(array("action" => array("Dashboard\CategoryController@destroy", $category->id), "method" => "DELETE")) }}}
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header bg-danger">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <h4 class="modal-title" style="color: white!important;">Borrar categoría</h4>
                        </div>
                        <div class="modal-body">
                          <p>¿Estás seguro de desactivar la categoría {{ $category->name }}?</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-outline btn-default margin-0" data-dismiss="modal">No</button>
                          <button type="submit" class="btn btn-outline btn-danger">Si</button>
                        </div>
                      </div>
                    </div>
                     {{ Form::close() }}
                  </div>

