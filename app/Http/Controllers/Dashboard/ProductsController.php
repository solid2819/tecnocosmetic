<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Requests\ProductAttributeRequest;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
// use Illuminate\Image\Facades\Image;
use App\Category;
use App\Product;
use App\ProductImage;
use App\ProductAttribute;
use Auth;
use Session;
use Image;
use DB;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware("auth");
    }
    
    public function index(Request $request, $id=null)
    {
        if($request){
            $query=trim($request->get('searchText'));//filtro de busqueda
            $products=DB::table('products')
                ->where('name', 'LIKE', '%' .$query. '%')
                ->orWhere('code', 'LIKE', '%' .$query. '%')
                //->where('status' , 1)
                ->orderBy('id', 'desc')
                ->paginate(10);

        }
        foreach ($products as $key => $val){
            $category_name = Category::where(["id"=>$val->category_id])->first();
            $products[$key]->category_name = $category_name->name;
        }
 
        return view("dashboard.products.index", ["products" => $products, "searchText"=>$query]);
        // $products = Product::orderBy("id", "DESC")->paginate(10);

        //  return view("dashboard.products.index", ["products" => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where(["parent_id"=> 0, "status" => "Activa"])->get();
        $categories_dropdown="<option value='' selected disable>Seleccionar</option>";
        foreach($categories as $cat){
            $categories_dropdown .= "<option value='".$cat->id. "'>".$cat->name."<option>";
            $subcategories = Category::where(["parent_id"=>$cat->id])->get();
            foreach ($subcategories as $sub_cat) {
                $categories_dropdown .= "<option value = '".$sub_cat->id."'>&nbsp; &nbsp; --&nbsp;".$sub_cat->name."</option>";
            }
        }
        return view("dashboard.products.create")->with(compact("categories_dropdown"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request){
        // $product = Product::create($request->all());

        if ($request->isMethod("post")) {
            $data = $request->all();
            $product = new Product;
            $product->category_id= $data["category_id"];
            $product->name= $data["name"];
            $product->code= $data["code"];
            $product->color= $data["color"];
            $product->slug= $data["slug"];
            $product->excerpt= $data["excerpt"];
            $product->price= $data["price"];
            $product->care= $data["care"];
            $product->description= $data["description"];

           
            if (Input::hasFile("image")) {
            $file = Input::file("image");
            $file->move(public_path() . "/images/dashboard/products/", $file->getClientOriginalName());

            $product->image=$file->getClientOriginalName();
            }

            if (Input::hasFile("image2")) {
            $file = Input::file("image2");
            $file->move(public_path() . "/images/dashboard/products/", $file->getClientOriginalName());

            $product->image2=$file->getClientOriginalName();
            }

            if (Input::hasFile("image3")) {
            $file = Input::file("image3");
            $file->move(public_path() . "/images/dashboard/products/", $file->getClientOriginalName());

            $product->image3=$file->getClientOriginalName();
            }

            if (Input::hasFile("image4")) {
            $file = Input::file("image4");
            $file->move(public_path() . "/images/dashboard/products/", $file->getClientOriginalName());

            $product->image4=$file->getClientOriginalName();
            }

        }


        if(empty($product->status))
            $product->status = 0;
        else{
            $product->status = $data["status"];
        }
       
       
        $product->save();
        return redirect()->route("products.index")->with("info", "¡Producto creado existosamente!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::orderBy("name", "ASC")->where("status", "Activa")->pluck("name", "id");
        
        $product = Product::find($id);

        return view("dashboard.products.edit", compact("product", "categories"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, $id)
    {
        $product=Product::findOrFail($id);
        $product->category_id=$request->get('category_id');
        $product->name=$request->get("name");
        $product->code=$request->get("code");
        $product->slug=$request->get("slug");
        $product->color=$request->get("color");
        $product->excerpt=$request->get('excerpt');
        $product->price=$request->get('price');
        $product->care=$request->get('care');
        $product->description=$request->get('description');

        if (empty($data["status"])) {
            $product->status = 0;
        }else{
            $product->status = 1;
        }

        return  $product;

        //Update Image
        if (Input::hasFile("image")) {
            $file = Input::file("image");
            $file->move(public_path() . "/images/dashboard/products/", $file->getClientOriginalName());

            $product->image=$file->getClientOriginalName();
        }
        if (Input::hasFile("image2")) {
            $file = Input::file("image2");
            $file->move(public_path() . "/images/dashboard/products/", $file->getClientOriginalName());

            $product->image2=$file->getClientOriginalName();
        }

        if (Input::hasFile("image3")) {
            $file = Input::file("image3");
            $file->move(public_path() . "/images/dashboard/products/", $file->getClientOriginalName());

            $product->image3=$file->getClientOriginalName();
        }
        if (Input::hasFile("image4")) {
            $file = Input::file("image4");
            $file->move(public_path() . "/images/dashboard/products/", $file->getClientOriginalName());

            $product->image4=$file->getClientOriginalName();
        }
        $product->update();

        return redirect()->back()->with("info", "Producto actualizado con exito");
    }


    //Envia el producto a la papelera
    public function softDelete($id)
    {
        $product = Product::find($id)->delete();
        // $category = json_decode(json_encode($category));
        return back()->with("info", "¡Enviado a papelera!" );
    }

    //Restaurar un producto
    public function product_restore($id)
    {
        $product_restore = Product::withTrashed()->where("id", $id)->restore();

        return redirect()->route("products.index")->with("info", "¡Producto restaurado existosamente!");
    }


    //Agrega atributos al producto SKU, Size, price, Stock
    public function product_attributes(Request $request, $id=null)
    {
        $productsDetails = Product::with("attributes")->where(["id"=>$id])->first();
        // $productsDetails = json_decode(json_encode($productsDetails));
        //  echo "<pre>"; print_r($productsDetails); die;

        if ($request->isMethod("post")) {
            $data=$request->all();

            // echo "<pre>"; print_r($data); die;
            foreach ($data["sku"] as $key => $val) {
                if (!empty($val)) {
                    //Verificación de SKU Duplicado
                    $attrCountSKU = ProductAttribute::where("sku", $val)->count();
                    if ($attrCountSKU>0) {
                        return redirect()->back()->with("info-error", "SKU duplicado. Intenta colocar un código SKU diferente");
                    }

                    //Verificar de Size Duplicado
                    $attrCountSizes = ProductAttribute::where(["product_id" => $id, "size" => $data["size"][$key]])->count();
                    if ($attrCountSizes>0) {
                        return redirect()->back()->with("info-error", "Size duplicado. Intenta colocar un tamaño del producto diferente. Verifica si se ha agregado a la lista de atributos");
                    }

                    $attribute = new ProductAttribute;
                    $attribute->product_id = $id;
                    $attribute->sku = $val;
                    $attribute->size = $data["size"][$key];
                    $attribute->price = $data["price"][$key];
                    $attribute->stock = $data["stock"][$key];
                    $attribute->save();
                }
            }

            return redirect()->back()->with("info", "Se ha agregado atributos al producto ". $productsDetails->name);
        }

        return view("dashboard.products.product_attributes", compact("productsDetails"));
    }

    //Edita los atributos del producto
    public function product_attributes_edit(Request $request, $id=null)
    {
        if($request->isMethod("post")){
            $data  = $request->all();
            //echo "<pre>"; print_r($data); die;

            foreach ($data["idAttr"] as $key => $attr) {
                ProductAttribute::where(["id"=>$data["idAttr"][$key]])->update(["price"=>$data["price"][$key], "stock"=>$data["stock"][$key]]);
            }

            return redirect()->back()->with("info", "Los atributos del producto se han actualizado.");
        }
    }

    //Agrega imagenes opcionales: -############Código en espera######
    // public function add_image_product(Request $request, $id=null)
    // {
    //     $product_add_images = Product::with("attributes")->where(["id"=>$id])->first();
    //     // $categoryDetails  = Category::where(["id"=>$product_add_images->category_id])->first();
    //     // $category_name $categoryDetails->name;

    //     if ($request->isMethod("post")) {
    //          // Add Images Optionals 
    //         $data=$request->all();
    //         if ($request->hasFile("image")) {
    //             $files = $request->file("image");
    //             foreach ($files as $file) {
    //                //Upload Images
    //                 $image = new ProductImage;
    //                 $extension = $file->getClientOriginalExtension();
    //                 $filename = rand(111, 99999). '.' .$extension;
    //                 $large_image_path = "images/dashboard/products" . $filename;
    //                 Image::make($file)->save($large_image_path);
    //                 $image->image = $filename;
    //                 $image->product_id = $data["product_id"];
    //                 $image->save(); 
    //             }   

                
    //         }return redirect()->back()->with("info", "Imagenes agregadas correctamente");
            
    //     }
    //     return view("dashboard.products.product_images", compact("product_add_images"));
    // }

    //Elimina atributos del producto en la vista dashboard.products.product_attributes
    public function product_attributes_delete($id = null){
        ProductAttribute::where(["id"=>$id])->delete();
        // $category = json_decode(json_encode($category));
        return redirect()->back()->with("info", "¡Algunos atributos han sido eliminados!" );
     }
}
