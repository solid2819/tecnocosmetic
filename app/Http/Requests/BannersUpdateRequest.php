<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannersUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            "title" =>  "required|max:30",
            "url" =>   "unique:banners,url," . $this->banner,
            "description"   => "max:40|min:20",
             "image" =>  'dimensions:min_width=545,min_height=630',
        ];
    }

    public function messages(){

        return[
            "title.required" => "El titulo es un campo obligatorio.",
            "title.max" => "El titulo no debe contener más de 30 caracteres.",

            "url.unique" => "La url ya existe",

            "description.max" => "La descripción del banner no debe albergar más de 40 caracteres",
            "description.min"  => "Descripción del banner muy corta.",

            "image.dimensions"    => "La imagen debe tener un Ancho de 545px por un alto de 630px",
        ];
    }


}
