@extends("layouts.frontend.theme")
@section("title")
	Login
@endsection


@section("content")
	<div class="breadcrumb">
   		<!-- container -->
		<div class="container">
			<h1>Login</h1>
			<ol class="item-breadcrumb">
	            <li><a href="index.html">Home</a></li>
	            <li>Login</li>     
            </ol>
		</div>
		<!-- /container -->
   	</div>
	<div class="page-account">
		<!-- container -->
		<div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="account-wrapper">
                    	<ul class="account-tab-list nav">
                        	<li class="active"><a data-toggle="tab" href="#">Ingresar</a></li>
                        </ul>  
                        	@foreach($settingdata as $view)	
                                <div class="account-form-container register-form">
                                    <div class="account-form">
                                       <form class="form-horizontal" role="form"method="POST" action="{{ url('/login') }}">
							                   {{ csrf_field() }}				   
											<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">                         
														<input id="email" type="text" class="form-control" placeholder="Igresar E-mail" name="email" value="{{ old('email') }}" required>

														@if ($errors->has('email'))
															<span class="help-block">
																<strong>{{ $errors->first('email') }}</strong>
															</span>
														@endif				  
												</div>
												
												<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">				   
														<input id="password_id" type="password" required class="form-control" name="password" placeholder="Contraseña">
														@if ($errors->has('password'))
															<span class="help-block">
																<strong>{{ $errors->first('password') }}</strong>
															</span>
														@endif
												</div>

												 <div class="form-group clearfix">
												  @if($view->remember_me == 'ON')
										            <div class="checkbox-custom checkbox-inline checkbox-primary pull-left">
										              <input type="checkbox" id="remember" name="checkbox">
										              <label for="inputCheckbox">{{ trans('Recordar')}}</label>
										            </div>
													@endif
													 
														<a class="pull-right" href="{{ url('/password/reset') }}">{{ trans('Recuperar contraseña')}} </a>
													 
													
												  </div>
								         <button type="submit" class="btn btn-primary ladda-button btn-block" data-plugin="ladda" data-style="expand-left">
											  {{ trans('Entrar')}}
										<span class="ladda-spinner"></span><div class="ladda-progress" style="width: 0px;"></div>
										</button>
										
									</form>
                                    </div>

                                </div>
                    </div>
                </div>
            </div>
		</div>
		<!-- /container -->
	</div>
	@endforeach
@endsection

