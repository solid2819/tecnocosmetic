@section("styles")
   <link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/blueimp-file-upload/jquery.fileupload.css">
   <link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/dropify/dropify.css">
   <link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/animsition/animsition.css">
   <link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/magnific-popup/magnific-popup.css">
  <link rel="stylesheet" href="{{URL::to('/')}}/assets/examples/css/advanced/lightbox.css">
@endsection

<div class="row">

  <div class="col-xs-12  col-sm-12 col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      {{ Form::label("name", "Titulo del producto") }}
      {{ Form::text("name",null, ["class" => "form-control", "id" => "name"])}} 
    </div>

    <div class="form-group">
      {{ Form::label("slug", "Slug") }}
      {{ Form::text("slug",null, ["class" => "form-control", "id" => "slug"])}} 
    </div>
  </div>

   <div class="col-xs-12  col-sm-12 col-md-6 col-lg-6 col-xl-6">
    <div class="form-group">
      {{ Form::label("categoria", "Categoría") }}
      {{ Form::select("category_id", $categories, null, ["class"=> "form-control"]) }}
    </div>

    <div class="row">

      <div class="col-md-4">
        <div class="form-group">
          {{ Form::label("code", "Código") }}
          <input type="text" name="code" value="{{ $product->code }}" class="form-control" /> 
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          {{ Form::label("price", "Precio") }}
          {{ Form::number("price",null, ["class" => "form-control", "id" => "price"])}} 
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          {{ Form::label("color", "Color") }}
          {{ Form::color("color",null, ["class" => "form-control", "id" => "color"])}} 
        </div>
      </div>
    </div>
    
  </div>


</div>
<div class="row">
  <div class="col-md-9">
    <div class="form-group">
      {{ Form::label("care", "Material de diseño") }}
      {{ Form::text("care",null, ["class" => "form-control", "id" => "care"])}} 
    </div>

   <div class="form-group">
      {{ Form::label("description", "Descripción") }}
      {{ Form::textarea("description",null, ["class" => "form-control", "id" => "description"])}} 
    </div>

    <div class="form-group">
      {{ Form::label("excerpt", "Descripción corta") }}
      {{ Form::textarea("excerpt",null, ["class" => "form-control", "id" => "excerpt"])}} 
    </div>

    <div class="form-group">
     <div>
        <input type="checkbox" id="status" name="status" @if($product->status==1) checked @endif />
        <label for="inputChecked">Activo</label>
      </div> 
    </div>


  </div>

  <div class="col-md-3">
    <div class="example-wrap">
      {{ Form::label("image", "Imagen destacada") }}
      <p><small>Elige o arrastra una imagen para el producto.</small></p>
        <div class="example">
          @if(!$product->image)
            <input type="file" id="input-file-now-custom-3" data-plugin="dropify" data-height="500" data-default-file="{{URL::to('/')}}/global/photos/placeholder.png" name="image" /> 
          @else
           <input type="file" id="input-file-now-custom-3" data-plugin="dropify" data-height="500" data-default-file="{{URL::to("images/dashboard/products/" . $product->image)}}" name="image" />
           <br>
            <a class="btn btn-outline btn-xs btn-danger" href="{{ route("delete-image-product", $product->id) }}">
              Borrar Imagen destacada
            </a>
            <br>
            {{-- <a class="btn btn-outline btn-xs btn-info" href="{{ route("add_image_product", $product->id) }}">
              Añadir imagenes
            </a> --}}
          @endif

          <div class="row" style="margin-top: 2vw">
              <div class="example-wrap">
                {{ Form::label("image", "Imagenes opcionales") }}<br>
                <small>Estas son imagenes opcionales del producto, puedes cambiarlas en cualquier momento haciendo clic en el boton "Elegir nueva imagen"</small>
                <div class="example" id="exampleZoomGallery">

                  <a class="inline-block" href="{{URL::to("images/dashboard/products/" . $product->image2)}}" title="{{ $product->image2 }}"
                  data-source="{{URL::to("images/dashboard/products/" . $product->image2)}}">
                    <img class="img-responsive" src="{{URL::to("images/dashboard/products/" . $product->image2)}}" alt="..."
                    width="220" />
                  </a>
                   <input type="file" name="image2" /><br>

                 <a class="inline-block" href="{{URL::to("images/dashboard/products/" . $product->image3)}}" title="{{ $product->image3 }}"
                  data-source="{{URL::to("images/dashboard/products/" . $product->image3)}}">
                    <img class="img-responsive" src="{{URL::to("images/dashboard/products/" . $product->image3)}}" alt="..."
                    width="220" />
                  </a>
                   <input type="file" name="image3" /><br>

                   <a class="inline-block" href="{{URL::to("images/dashboard/products/" . $product->image4)}}" title="{{ $product->image4 }}"
                  data-source="{{URL::to("images/dashboard/products/" . $product->image4)}}">
                    <img class="img-responsive" src="{{URL::to("images/dashboard/products/" . $product->image4)}}" alt="..."
                    width="220" />
                  </a>
                   <input type="file" name="image4" />
                </div>
              </div>
            </div>
        
    </div> 

    {{-- Imagenes adicionales --}}    

  </div>
</div>

<div class="container">
  <div class="row">
    <div class="form-group">
      {{ Form::submit("Actualizar", ["class" => "btn btn-success btn-sm"]) }}

      <a href="{{ route("products.index") }}">
        <button type="button" class="btn btn-warning btn-sm">
          Volver
        </button>
      </a>
      
    </div>
</div>
</div>


@section("scripts")
  <script src="{{ asset("vendor/jquerystringToSlug/jquery-stringToSlug.min.js") }} "></script>
  <script src="{{ asset("vendor/ckeditor/ckeditor.js") }} "></script>
  
  
  <script>
    $(document).ready(function () {
      $("#name, #slug").stringToSlug({
        callback: function (text) {
          $("#slug").val(text);
        }
      });
    });

    /*CK EDITOR*/
    CKEDITOR.config.height=400;
    CKEDITOR.config.width="auto";

    CKEDITOR.replace("description");

    $(document).ready(function() {
      $("#form-products").validate();
    });
  </script>
@endsection