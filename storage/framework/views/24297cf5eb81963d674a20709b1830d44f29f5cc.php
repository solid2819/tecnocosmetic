 <div class="form-group">
   <div class="input-search">
   	<form role="search" method="get" action="<?php echo e(route("products.index")); ?>">
	     <button type="submit" class="input-search-btn"><i class="icon wb-search" aria-hidden="true"></i></button>
	     <input type="text" class="form-control" name="searchText" placeholder="Buscar productos por nombre o código..." value="<?php echo e($searchText); ?>">
	   </div>
	</form>
 </div>