 <div class="form-group">
   <div class="input-search">
   	<form role="search" method="get" action="<?php echo e(route("categories.index")); ?>">
	     <button type="submit" class="input-search-btn"><i class="icon wb-search" aria-hidden="true"></i></button>
	     <input type="text" class="form-control" name="searchTextTrash" placeholder="Buscar categorías..." value="<?php echo e($searchTextTrash); ?>">
	   </div>
	</form>
 </div>