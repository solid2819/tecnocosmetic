                <div class="modal fade modal-3d-flip-vertical" id="modal-delete-{{ $attribute->id }}" aria-hidden="true"
                  aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
                   
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header bg-danger">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <h4 class="modal-title" style="color: white!important;">Borrar Attributos</h4>
                        </div>
                        <div class="modal-body">
                          <p>¿Estás seguro de eliminar estos attributos del producto?</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-outline btn-default margin-0" data-dismiss="modal">No</button>
                          <a href="{{ route("product_attributes_delete", $attribute->id) }}" class="btn btn-outline btn-danger">Si</a>
                        </div>
                      </div>
                    </div>
                    
                  </div>

