<?php $__env->startSection("title"); ?>
	| Crear Banner
<?php $__env->stopSection(); ?>
<?php $__env->startSection("styles"); ?>
  <link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/global/vendor/webui-popover/webui-popover.css">
  <link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/global/vendor/toolbar/toolbar.css">
  <style type="text/css" media="screen">
    a{
      text-decoration: none!important;
    }  
    .td1{
      width: 4vw!important;
  </style>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php echo $__env->make("layouts.messages", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="page-header">
    <h1 class="page-title font_lato">
      Crear Nuevo Banner

    </h1>
    <div class="page-header-actions">
      <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('/dashboard')); ?>"><?php echo e(trans('app.home')); ?></a></li>
        <li><a href="<?php echo e(URL::to('dashboard/config/design/banners')); ?>">Banners</a></li>
        <li class="active">Create</li>
      </ol>
    </div>
  </div>
	 <div class="panel">
        <div class="panel-body">
          <!-- Categories table -->
          <div class="example-wrap">
            <h2 class="example-title"></h2>
            <div class="example">
              

              <?php echo Form::open(["route" => "banners.store", "id" => "form-banners", "enctype" => "multipart/form-data"]); ?>

                <?php echo e(csrf_field()); ?>

                 <?php echo $__env->make("dashboard.configuracion.design.partials.form_create_banner", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
              <?php echo e(Form::close()); ?>  
            </div>
              
          </div>
          <!-- End table categories -->
          
        </div>
      </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>