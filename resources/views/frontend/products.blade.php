@extends("layouts.frontend.theme")
@section("title")
	Productos
@endsection

@section("content")
	<div class="breadcrumb">
   		<!-- container -->
		<div class="container">
			<h1>Productos</h1>
			<ol class="item-breadcrumb">
	            <li><a href="{{ url("/") }}">Home</a></li>
	            <li class="active">Productos</li>     
            </ol>
		</div>
		<!-- /container -->
   	</div>

   	<div class="archive-product">
		<!-- Container -->
		<div class="container">
			<!-- Row -->
			<div class="row">
				<div class="col-md-3">
					{{-- Aside --}}
					@include("frontend.template_parts.aside_products")
					{{-- End aside --}}
				</div>
				<div class="col-md-9">
                    <!-- product-shorting -->
					<div class="product-shorting d-flex align-items-center justify-content-between">
						<div class="grid-list-view">
		                    <ul class="nav tabs-area">
		                        <li class="active">
		                        	<a data-toggle="tab" href="#grid-view">
		                            	<i class="fa fa-th"></i>
		                            </a>
		                        </li>
		                        <li>
		                        	<a data-toggle="tab" href="#list-view" class="">
		                        		<i class="fa fa-list-ul"></i>
		                        	</a>
		                        </li>
		                    </ul>
		                    <span class="show-items">Showing 1 to 9</span>
		                </div>
		                <div class="toolbar-sorter">
		                    <select name="orderby" class="orderby">
								<option value="menu_order" selected="selected">Default sorting</option>
								<option value="popularity">Sort by popularity</option>
								<option value="rating">Sort by average rating</option>
								<option value="date">Sort by newness</option>
								<option value="price">Sort by price: low to high</option>
								<option value="price-desc">Sort by price: high to low</option>
							</select>
		                </div>
		            </div>
		            <!--/product-shorting -->

		             <!-- tab-content -->
		            <div class="tab-content">
		            	<div id="grid-view" class="tab-pane fade in active">
							<div class="product products-grid">
								<div class="row row-products">   
						 			@foreach($front_products as $product)

						 				<div class="col-md-4 col-sm-6">
							 				<div class="product-block" data-publish-date="">
												<div class="product-image product_1">
													<div class="product-thumbnail">
														<a href="{{ route("single_product", $product->slug) }}" title="">
															@if($product->image)
																<img class="product-featured-image" src="{{ asset("images/dashboard/products/" .$product->image) }}" alt="">
															@endif
														</a>
													</div>
													<div class="tawcvs-swatches color_selector" data-attribute_name="attribute_pa_color">
			                        					<span class="swatch swatch-color swatch-blue" title="Blue" data-value="blue">Blue</span>
			                        					<span class="swatch swatch-color swatch-orange" title="Orange" data-value="orange">Orange</span>
			                        					<span class="swatch swatch-color swatch-purple" title="Purple" data-value="purple">Purple</span>
			                        				</div>
													<div class="product-actions">
														<a href="#" data-id="" class="btn wishlist product-quick-whistlist" title="Add to whistlist">
														<i class="fa fa-heart-o"></i>
														</a>
														<a href="" data-id="" class="btn product-quick-view btn-quickview" title="Quickview">
														    <i class="fa fa-eye"></i>
														</a>
														<a href="" data-id="" class="btn product-quick-compare btn-compare" title="Compare">
														    <i class="fa fa-retweet"></i>
														</a>
													</div>
												</div><!-- /.product-image -->
												<div class="product-meta">
													<span class="product-rating" data-rating="">
														<span class="star-rating">
															<i class="fa fa-star-o"></i>
															<i class="fa fa-star-o"></i>
															<i class="fa fa-star-o"></i>
															<i class="fa fa-star-o"></i>
															<i class="fa fa-star-o"></i>
														</span>
													</span>
													<!-- end rating -->
													<h4 class="product-name">
														<a href="product_single.html" title="">
															{{ $product->name }}
														</a>
													</h4>
													<div class="product-price">
														<span class="amout">
															<span class="money" data-currency-usd="$700.00">{{ $product->price }}</span>
														</span>
														<a href="product_single.html" class="add_to_cart_button">Add to Cart</a>
													</div>
												</div><!-- /.product-meta -->
											</div>
							 			</div>
						 			@endforeach
						 		</div>
						 	</div>
						</div>
						<div id="list-view" class="tab-pane fade">
						 	<div class="product products-list">
								<div class="row row-products"> 
									@foreach($front_products as $product)  
							 			<div class="col-md-12">
							 				<div class="product-block">
							 					<div class="row">
								 					<div class="col-md-4 col-lg-4 col-sm-4">
								 						<div class="product-image product_1">
															<div class="product-thumbnail">
																<a href="{{ route("single_product", $product->slug) }}" title="">
																	@if($product->image)
																		<img class="product-featured-image" src="{{ asset("images/dashboard/products/" .$product->image) }}" alt="">
																	@endif
																</a>
															</div>
															<div class="tawcvs-swatches color_selector" data-attribute_name="attribute_pa_color">
					                        					<span class="swatch swatch-color swatch-blue" title="Blue" data-value="blue">Blue</span>
					                        					<span class="swatch swatch-color swatch-orange" title="Orange" data-value="orange">Orange</span>
					                        					<span class="swatch swatch-color swatch-purple" title="Purple" data-value="purple">Purple</span>
					                        				</div>
														</div><!-- /.product-image -->
									 			    </div>
									 				<div class="col-md-8 col-lg-8 col-sm-8">
														<div class="product-meta">
															<span class="product-rating" data-rating="">
																<span class="star-rating">
																	<i class="fa fa-star-o"></i>
																	<i class="fa fa-star-o"></i>
																	<i class="fa fa-star-o"></i>
																	<i class="fa fa-star-o"></i>
																	<i class="fa fa-star-o"></i>
																</span>
															</span>
															<!-- end rating -->
															<h4 class="product-name">
																<a href="product_single.html" title="">
																	{{ $product->name }}
																</a>
															</h4><!-- /.product-product -->

															<div class="product-price">
																<span class="amout">
																	<span class="money" data-currency-usd="$700.0">{{ $product->price }}</span>
																</span>
															</div><!-- /.product-price -->
															<div class="excerpt">
										                        Revolutionary multi-touch interface. iPod touch features the same multi-touch screen
																technology as iPhone. Pinch to zoom in on a photo. Scroll through your songs and videos
																with a flick. Flip throug..              
										                    </div>
										                    <div class="product-footer">
																<a href="cart.html" class="btn btn-primary">Add to Cart<i class="fa fa-shopping-bag" aria-hidden="true"></i></a>
																<div class="product-actions">
																	<a href="#" data-id="" class="btn wishlist product-quick-whistlist" title="Add to whistlist">
																	<i class="fa fa-heart-o"></i>
																	</a>
																	<a href="" data-id="" class="btn product-quick-view btn-quickview" title="Quickview">
																	    <i class="fa fa-eye"></i>
																	</a>
																	<a href="" data-id="" class="btn product-quick-compare btn-compare" title="Compare">
																	    <i class="fa fa-retweet"></i>
																	</a>
																</div>
															</div>
														</div><!-- /.product-meta -->
													</div>
												</div>
											</div>
							 			</div>
							 		@endforeach
						 		</div>
					 		</div>
						</div>
					</div>
					<!-- /tab-content -->

					<!-- pagination -->
					<nav class="pagination clearfix">
						{{ $front_products->render() }}
					</nav>
					<!-- /pagination -->
		 		</div>
	 		</div>
	 	</div>
	</div>
@endsection