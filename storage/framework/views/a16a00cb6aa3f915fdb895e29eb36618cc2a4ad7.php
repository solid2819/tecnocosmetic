<?php $__env->startSection("title"); ?>
	| Crear producto 
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
       <?php if(count($errors)>0): ?>
          <?php echo $__env->make("layouts.messages", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php endif; ?>
      <div class="page-header">
          <h1 class="page-title font_lato">
            Nuevo producto
          </h1>
          <div class="page-header-actions">
            <ol class="breadcrumb">
              <li><a href="<?php echo e(URL::to('/dashboard')); ?>"><?php echo e(trans('app.home')); ?></a></li>
              <li><a href="<?php echo e(URL::to('/dashboard/products')); ?>"><?php echo e(trans('Products')); ?></a></li>
              <li class="active">Create</li>
            </ol>
          </div>
        </div> 
	     <div class="col-sm-12 col-md-12 ol-lg-12 ">
          <!-- Panel Floating Lables -->
         <div class="panel">
            <div class="panel-heading">
              <h2 class="panel-title">Crear un nuevo producto</h2>
            </div>
            <div class="panel-body">
              
            <?php echo Form::open(["route" => "products.store", "id" => "form-products", "enctype" => "multipart/form-data"]); ?>

              <?php echo e(csrf_field()); ?>

             <?php echo $__env->make("dashboard.products.partials.form-create", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo Form::close(); ?>


            </div>
          </div>
          <!-- End Panel Floating Lables -->
        </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>