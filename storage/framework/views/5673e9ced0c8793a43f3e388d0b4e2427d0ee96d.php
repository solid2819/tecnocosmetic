<?php $__env->startSection("title"); ?>
	404
<?php $__env->stopSection(); ?>
<?php $__env->startSection("content"); ?>
	<div class="page-404 section-bg" style="background-image:url(<?php echo e(asset('front/assets/images/bg_404.jpg')); ?>)">
		<!-- container -->
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-8">
					<div class="content-404">
						<h1 class="title_404">404</h1>
						<div class="error-description pt-30 pb-30">
							¡Ooops! La página que buscas no existe.<br/>
						</div>
						<div class="search_page_404">
							<form method="get" class="searchform" action="#">
								<div class="wiget-search input-group">
								 <input name="s" maxlength="40" class="form-control input-search" type="text" size="20" placeholder="Intenta buscar aquí"> 
								 <span class="input-group-addon input-large btn-search"> 
								 	<span class="fa fa-search"></span>
								 	<input type="submit" class="fa" value="">
								 </span>
								</div>
							</form>
						</div>
						<a href="<?php echo e(url("/")); ?>" class="btn btn-primary back-home">Ir al Inicio<i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		</div>
		<!-- /container -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("layouts.frontend.theme", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>