@extends('layouts.template')
@section("title")
	| Crear categoría del producto
@endsection
@section('content')
      <div class="page-header">
          <h1 class="page-title font_lato">
            Nueva Categoría
          </h1>
          <div class="page-header-actions">
            <ol class="breadcrumb">
              <li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
              <li><a href="{{URL::to('/dashboard/product/categories')}}">{{ trans('Categories')}}</a></li>
              <li class="active">Create</li>
            </ol>
          </div>
        </div> 
	     <div class="col-sm-12 col-md-6 ol-lg-6 col-md-offset-2">
          <!-- Panel Floating Lables -->
          @if(count($errors)>0)
            @include("layouts.messages"){{--message--}}
          @endif
         <div class="panel">
            <div class="panel-heading">
              <h2 class="panel-title">Crear nueva categoría</h2>
            </div>
            <div class="panel-body">
              
            {!! Form::open(["route" => "categories.store"]) !!}
             @include("dashboard.products.categories.partials.form-create")
            {!! Form::close() !!}

            </div>
          </div>
          <!-- End Panel Floating Lables -->
        </div>
@endsection